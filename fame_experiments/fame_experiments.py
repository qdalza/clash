'''
| From: "FAME: Fast Attribute-based Message Encryption"
| Published in: 2017
| Available from: https://eprint.iacr.org/2017/807
| Notes: Experiments for Clash and MicroSCOPE
|
| type:           ciphertext-policy attribute-based encryption
|

:Authors:         Alexandr Zalitko, Antonis Michalas
:Date:            30/2020
'''

from charm.toolbox.pairinggroup import PairingGroup, GT
from ac17 import AC17CPABE
from time import clock
from statistics import mean
from tqdm import tqdm
from terminaltables import SingleTable

import argparse
import random
import sys, os

# set the recursion limit to 10000 as it requires for a successful run of experiments with attr more than 200
sys.setrecursionlimit(10000)

banner = '''
+-----FAME Experiments------------------------------------------------------------------------------------------------+
|                                                                                                                     |
| Experiment: 1        Measure Generation/Execution time for cpabe master public/private key pair                     |
| Experiment: 2        Measure Generation/Execution time for cpabe user's key                                         |
| Experiment: 3        Measure Disk Size of a user's key with an increase in the number of attributes                 |
| Experiment: 4        Measure Encryption Time while the number of attributes in the policy grow                      |
|                      Measure Decryption Time while the number of attributes associated to the secret key increasing |
|                      Measure Encryption Time while the number of attributes in the random policy grow               |
|                      Measure Decryption Time required to decrypt a ciphertext attached with a random policy         |
| All Experiments: 5   Measure All Experiments                                                                        |
|                                                                                                                     |
| for more help, type the following command:  python3 fame_experiments.py -h                                          |
|                                                                                                                     |
+---------------------------------------------------------------------------------------------------------------------+
'''

examples = '''

To measure generation/execution time for cpabe master public/private key pair:
* `python3 fame_experiments.py -exp 1 -start 0 -end 200 -interval 1 -rep 100000`

To measure generation/execution time for cpabe user's key:
* `python3 fame_experiments.py -exp 2 -start 0 -end 1000 -interval 100 -rep 100000`

To measure disk size of a user's key with an increase in the number of attributes:
* `python3 fame_experiments.py -exp 3 -start 0 -end 1000 -interval 100 -rep 100000`

To measure encryption time while the number of attributes in the policy grow, and to measure decryption time while the number of attributes associated to the secret key is increasing
* `python3 fame_experiments.py -exp 4 -start 0 -end 1000 -interval 100 -rep 100000` -random n

To measure encryption time while the number of attributes in the random policy grow, and to measure decryption time required to decrypt a ciphertext attached with a random policy
* `python3 fame_experiments.py -exp 4 -start 0 -end 1000 -interval 100 -rep 100000` -random y

To run all the experiments above:
* `python3 fame_experiments.py -exp 5 -start 0 -end 1000 -interval 100 -rep 100000`
'''


def menu():
    print('\n')
    print(banner)
    parser = argparse.ArgumentParser(prog='fame_experiments.py',
                                     usage='%(prog)s [options]',
                                     epilog='Enjoy the program! :)')
    parser.add_argument('-exp', type=int, required=True, help="run an experiment under a chosen number [1 - 5]")
    parser.add_argument('-start', type=int, default=0, help="enter a start value")
    parser.add_argument('-end', type=int, default=50, help='enter an end value')
    parser.add_argument('-interval', type=int, default=1, help='enter an interval value')
    parser.add_argument('-rep', type=int, default=3, help='enter a number of repetitions')
    parser.add_argument('-random', type=str, default='n', help='y/n, generate a random policy ')
    args = parser.parse_args()
    return args


class Experiment:

    title = ""
    container = {}
    total_time_lst = []

    # creates an instant of bilinear pairing map
    pairing_group = PairingGroup('MNT224')

    # AC17 CP-ABE under DLIN (2-linear)
    cpabe = AC17CPABE(pairing_group, 2)

    # generate an attribute list in the form of (1,2,...,n)
    def generate_attr_list(self, n):
        attr_list = []
        for i in range(1, n + 1, 1):
            attr = str(i)
            attr_list.append(attr)
        return attr_list

    # outputs results of an experiment in the table format
    def create_table(self, container, title):
        data = container.items()
        table_instance = SingleTable(data, title)
        table_instance.justify_columns[2] = 'right'
        print("\n")
        print(table_instance.table)


# measures generation/execution time of master public/private key pair
class Exp1(Experiment):

    def __init__(self, start, end, rep, interval):
        self.start = start
        self.end = end
        self.rep = rep
        self.interval = interval

    def generate_master_key_pair(self, cpabe, i):
        t1 = clock()
        for i in range(1, i + 1, 1):
            (pk, msk) = cpabe.setup()
        t2 = clock()
        total_time = t2 - t1
        return total_time

    def run_generation_master_key_pair_experiment(self):
        self.title = "Generation of pk/msk key pairs"
        self.container["# of pk/msk key pairs"] = "time(seconds)"
        for i in tqdm(range(self.start, self.end + self.interval, self.interval)):
            if i == 0:
                continue
            for j in range(1, self.rep + 1, 1):
                total_time = self.generate_master_key_pair(self.cpabe, i)
                self.total_time_lst.append(total_time)
            self.container[str(i)] = "{0:3f}".format(mean(self.total_time_lst))
            self.total_time_lst.clear()

        self.create_table(self.container, self.title)
        self.container.clear()


# measures generation/execution time of user's key
class Exp2(Experiment):

    def __init__(self, start, end, rep, interval):
        self.start = start
        self.end = end
        self.rep = rep
        self.interval = interval

    def generate_user_key(self, cpabe, pk, msk, attr_list):
        t1 = clock()
        cpabe.keygen(pk, msk, attr_list)
        t2 = clock()
        total_time = t2 - t1
        return total_time

    def run_generation_user_key_experiment(self):
        print("\n")
        self.title = "Generation of user's key"
        self.container["# user's key"] = "time(seconds)"
        (pk, msk) = self.cpabe.setup()
        for i in tqdm(range(self.start, self.end + self.interval, self.interval)):
            if i == 0:
                continue
            attr_list = self.generate_attr_list(i)
            for j in range(1, self.rep + 1, 1):
                total_time = self.generate_user_key(self.cpabe, pk, msk, attr_list)
                self.total_time_lst.append(total_time)
            self.container[str(i)] = "{0:3f}".format(mean(self.total_time_lst))
            self.total_time_lst.clear()

        self.create_table(self.container, self.title)
        self.container.clear()


# measures the disk size of a user key with an increase in the number of attributes
class Exp3(Experiment):

    def __init__(self, start, end, rep, interval):
        self.start = start
        self.end = end
        self.rep = rep
        self.interval = interval

    # generate user's key and outputs to a file "./user_key.cpabe". If so, returns the size of the user's key
    def generate_user_key_ret_file_size(self, cpabe, pk, msk, attr_list):
        file_path = "./user_key.cpabe"

        key = cpabe.keygen(pk, msk, attr_list)
        with open(file_path, 'w') as f:
            f.write(str(key))
        try:
            file_size = os.path.getsize(file_path)
        except OSError:
            sys.exit()
        return file_size

    def run_disk_size_user_key_experiment(self):
        print("\n")
        self.title = "Disk size of the user's key"
        self.container["# user's keys"] = "file size(KB)"
        file_size_lst = []
        (pk, msk) = self.cpabe.setup()
        for i in tqdm(range(self.start, self.end + self.interval, self.interval)):
            if i == 0:
                continue
            attr_list = self.generate_attr_list(i)
            for j in range(1, self.rep + 1, 1):
                file_size = self.generate_user_key_ret_file_size(self.cpabe, pk, msk, attr_list)
                file_size_lst.append(file_size / 1024.0)
            self.container[i] = "{0:3f}".format(mean(file_size_lst))
            file_size_lst.clear()

        self.create_table(self.container, self.title)
        self.container.clear()


# measures encryption time while the number of attributes in the policy grow
# measures decryption time while the number of attributes associated to the secret key increases
class Exp4(Experiment):

    def __init__(self, start, end, rep, interval, random):
        self.start = start
        self.end = end
        self.rep = rep
        self.interval = interval
        self.random = random

    # generate a cpabe policy in the form of (1 and 2 ... and n)
    def generate_policy(self, n):
        policy = "1"
        for i in range(2, n + 1, 1):
            policy += " and " + str(i)
        return policy

    # generate a random value in range (-1 million , 1 million). If value is negative returns False, otherwise True
    def get_random(self):
        ret = random.randint(-1000000, 1000000)
        if ret < 0:
            return False
        else:
            return True

    # generate a random cpabe policy in the range (1,2,...,n)
    def generate_random_policy(self, n):
        policy = "1"
        ors = 0
        for i in range(2, n + 1, 1):
            rand = bool(random.getrandbits(1))
            if rand:
                policy += " and " + str(i)
            else:
                policy += " or (" + str(i)
                ors += 1
        for i in range(0, ors):
            policy += ")"
        return policy

    # measures cpabe's encryption time
    def encrypt_message(self, cpabe, pk, policy, msg):
        t1 = clock()
        ctxt = cpabe.encrypt(pk, msg, policy)
        t2 = clock()
        total_time = t2 - t1
        return total_time, ctxt

    # measures cpabe's decryption time
    def decrypt_message(self, cpabe, pk, ctxt, key):
        t1 = clock()
        rec_msg = cpabe.decrypt(pk, ctxt, key)
        t2 = clock()
        total_time = t2 - t1
        return total_time, rec_msg

    def run_encrypt_decrypt_experiment(self):
        print("\n")
        decryption_container = {}
        decryption_time_lst = []

        encrypt_title = "Encryption"
        decrypt_title = "Decryption"

        self.container["# of attrs in the policy"] = "time(seconds)"
        decryption_container["# of attrs bound to secret"] = "time(seconds)"

        msg = self.pairing_group.random(GT)
        (pk, msk) = self.cpabe.setup()
        for i in tqdm(range(self.start, self.end + self.interval, self.interval)):
            if i == 0:
                continue
            if self.random == "y":
                policy = self.generate_random_policy(i)
            elif self.random == "n":
                policy = self.generate_policy(i)
            else:
                sys.exit(0)

            attr_list = self.generate_attr_list(i)
            key = self.cpabe.keygen(pk, msk, attr_list)
            for j in range(1, self.rep + 1, 1):
                total_time, ctxt = self.encrypt_message(self.cpabe, pk, policy, msg)
                decryption_time, rec_msg = self.decrypt_message(self.cpabe, pk, ctxt, key)
                self.total_time_lst.append(total_time)
                decryption_time_lst.append(decryption_time)
            self.container[i] = "{0:3f}".format(mean(self.total_time_lst))
            decryption_container[i] = "{0:3f}".format(mean(decryption_time_lst))
            self.total_time_lst.clear()
            decryption_time_lst.clear()

        self.create_table(self.container, encrypt_title)
        self.create_table(decryption_container, decrypt_title)
        self.container.clear()
        decryption_container.clear()


def main():
    args = menu()
    choice = args.exp
    if choice == 1:
        ex1 = Exp1(args.start, args.end, args.rep, args.interval)
        ex1.run_generation_master_key_pair_experiment()
    elif choice == 2:
        ex2 = Exp2(args.start, args.end, args.rep, args.interval)
        ex2.run_generation_user_key_experiment()
    elif choice == 3:
        ex3 = Exp3(args.start, args.end, args.rep, args.interval)
        ex3.run_disk_size_user_key_experiment()
    elif choice == 4:
        ex4 = Exp4(args.start, args.end, args.rep, args.interval, args.random)
        ex4.run_encrypt_decrypt_experiment()
    elif choice == 5:
        ex1 = Exp1(args.start, args.end, args.rep, args.interval)
        ex2 = Exp2(args.start, args.end, args.rep, args.interval)
        ex3 = Exp3(args.start, args.end, args.rep, args.interval)
        ex4 = Exp4(args.start, args.end, args.rep, args.interval, args.random)

        ex1.run_generation_master_key_pair_experiment()
        ex2.run_generation_user_key_experiment()
        ex3.run_disk_size_user_key_experiment()
        ex4.run_encrypt_decrypt_experiment()
    else:
        sys.exit()


if __name__ == "__main__":
    debug = False
    main()