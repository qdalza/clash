/*
 * Copyright (C) 2011-2019 Intel Corporation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   * Neither the name of Intel Corporation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef UTILITY_E1_H__
#define UTILITY_E1_H__

#include "stdint.h"

#include <openssl/rsa.h>

typedef struct _internal_param_struct_t
{
	uint32_t ivar1;
	uint32_t ivar2;
}internal_param_struct_t;

typedef struct _external_param_struct_t
{
	uint32_t var1;
	uint32_t var2;
	internal_param_struct_t *p_internal_struct;
}external_param_struct_t;

typedef struct encryption_key_pair {
	RSA *pk;
	RSA *sk;
} enc_key_pair;


#ifdef __cplusplus
extern "C" {
#endif

// ADDITIONAL FUNCTIONS

void handleErrors(void);

int hmac_sign(uint8_t *data, uint8_t *key, uint8_t *hmac, uint32_t *hmac_ln);
int hmac_verify(uint8_t *hmac, uint8_t *not_hmac, uint32_t hmac_ln);

int gcm_encrypt(uint8_t *plaintext, int plaintext_len,
				uint8_t *aad, int aad_len,
				uint8_t *key,
				uint8_t *iv, int iv_len,
				uint8_t *ciphertext,
				uint8_t *tag);

int gcm_decrypt(uint8_t *ciphertext, int ciphertext_len,
				uint8_t *aad, int aad_len,
				uint8_t *tag,
				uint8_t *key,
				uint8_t *iv, int iv_len,
				uint8_t *plaintext);

enc_key_pair PKE_KeyGen();
void clear_key_pair(enc_key_pair *key_pair);

uint8_t *base64_decode(uint8_t *bbuf, uint32_t *len);
int get_base64_length(int ct_ln);

// FILE MANIPULATION
int clash_msg_write_file(char *path, char *data, size_t data_ln);
void get_cpabe_ct(char *abe_ct, size_t len);
int clash_get_local_lvs(char *path, char *data, size_t data_sz);

// MESSAGE DIGEST FUNCTIONS

void clash_md_concat_pass_scope(uint8_t *md, uint8_t *r, uint8_t *s_j);
void clash_md_concat_token(uint8_t *md, uint8_t *r, uint8_t *u_j, uint8_t *idx);
void clash_rev_revoke_concat(uint8_t *md, uint8_t *r, uint8_t *u_j, uint8_t *u_l, uint8_t *n, uint8_t *abe_ct, uint8_t *manage);
void clash_rev_assign_concat(uint8_t *md, uint8_t *r, uint8_t *u_l, uint8_t *abe_ct);
void clash_m_idx_concat(uint8_t *md, uint8_t *r, uint8_t *u_i, uint8_t *m_idx);


uint32_t marshal_message_exchange_input_parameters_e1_to_e3(uint32_t target_fn_id, uint32_t msg_type, char *p, size_t p_size, char** marshalled_buff, size_t* marshalled_buff_len);
uint32_t marshal_message_exchange_response(char** resp_buffer, size_t* resp_length, char *p, size_t p_size);
uint32_t marshal_message_exchange_request(uint32_t target_fn_id, uint32_t msg_type, uint32_t secret_data, char** marshalled_buff, size_t* marshalled_buff_len);
uint32_t umarshal_message_exchange_response(char* out_buff, char** secret_response);
uint32_t generate_aeskey(sgx_aes_gcm_128bit_key_t *key, size_t key_size, uint8_t *iv, size_t iv_size);
uint32_t generate_plain(char *p, size_t p_size);
uint32_t get_cipher(sgx_aes_gcm_128bit_key_t *key, char *p, size_t p_size, uint8_t *iv, size_t iv_size, char *c, sgx_aes_gcm_128bit_tag_t *mac);
uint32_t marshal_input_parameters_e1_to_e3(uint32_t target_fn_id, uint32_t msg_type, sgx_aes_gcm_128bit_key_t *key, size_t key_size, uint8_t *iv, size_t iv_size, char** marshalled_buff, size_t* marshalled_buff_len);
uint32_t unmarshal_retval_and_output_parameters_from_e3(char* out_buff, char** retval);
uint32_t umarshal_message_exchange_request(char** p, size_t* p_size, ms_in_msg_exchange_t* ms);


#ifdef __cplusplus
 }
#endif
#endif
