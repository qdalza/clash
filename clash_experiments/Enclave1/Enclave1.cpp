#include <stdio.h>
#include <string>
#include <stdlib.h>
#include <mbusafecrt.h>

#include "sgx_tprotected_fs.h"

#include "/opt/intel/Linux/package/include/tSgxSSL_api.h"

#include "sgx_eid.h"
#include "Enclave1_t.h"
#include "EnclaveMessageExchange.h"
#include "error_codes.h"
#include "Utility_E1.h"
#include "sgx_dh.h"
#include <map>

#include <openssl/rsa.h>
#include <openssl/bn.h>
#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/evp.h>
#include <openssl/sha.h>
#include <openssl/obj_mac.h>
#include <openssl/rand.h>
#include <openssl/err.h>
#include <openssl/hmac.h>

using namespace std;

#define UNUSED(val) (void)(val)

#define ABE_SIZE 512

char cpabe_key[] = "-----BEGIN USER PRIVATE KEY BLOCK-----\n"\
		"AAAAGapvyWDU53PRoeJWg1Wwbw3zR5NkZWNLZXkAAAEcoQFLoUSzoUECC54Drc0c7Bc1fekLmit6x2cFjw0gg7DnAh7N2acdAbIV93ijDaQWYrTmHGVLYHjFiQOawEQXQtIbcAQyFIQYd6EJS1hfRmVtYWxloSSyoSEDCn/\n"\
		"cnxL6ic4r31DWj8stgoEcONqNB5yIVi8zfvro82OhEUtYX1BIRF9SZXNlYXJjaGVyoSSyoSECH0NVsxWtcWzzPk4zFS6ZnSRcQJ8QaROlc7Ufzg8IZmChAUyhRLOhQQMQ9f/s0x1wobTzH3fispgBY/\n"\
		"IE80jQyTzIUIQ1k02c5RZcrDH5RK8co3HijHwTxfAdTvdgrAg+EQUl8vLDViUfoQVpbnB1dKEXfEZlbWFsZXxQSERfUmVzZWFyY2hlcnw=\n"\
		"-----END USER PRIVATE KEY BLOCK-----\n";


// ****************************************************************************
// *              Begin of Section: ENCRYPTION KEYS (pk, sk)                  *
// ****************************************************************************

char e_pk[] = "-----BEGIN PUBLIC KEY-----\n"\
		"MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAthNaVTSoU5yagQai1RkV\n"\
		"rNNccxhyYnxD8C+EoOjZvTLEalDH0aOgY3Z0Aw0rKwz6G9iZl0GfGttaSqAJVeH9\n"\
		"+ECLkWGFKpP2nqfl0XYuDmzng30JZ9QzyldFa0b/7v5ngBT5gl51qGKg1Ie7Yj8N\n"\
		"X+Psi3IGFSNDtN9BdDW4oOyoavVrtMplZBv321OCBRPBCTqmHIfn5Wj8BdsJzGbk\n"\
		"OHIhe77La+LlikvHMw+U7kcU4b9TUDM3uAEtxYFYzfpzsoBhWATTeMM+y8lDZTcm\n"\
		"OlZ8w0UYU7lKbH7t5ZwiPL90eCXZrUJTJIC4L79HMeaLbJPNgIMmVHzLAZG0drSf\n"\
		"MZeVUs1UfTNiFkwSFHLfTzM3Ha9NpRUKzgXjU047qaUp6PRs+ZQzgjXJ40dkHvpy\n"\
		"Iq8Dg0uMGmqWJn+MFvgiLVtdZTYXatWPSY7lN43+GGJgxKn1JGhSRS4vhNPVJ+og\n"\
		"NUvfmSv/tYeZhMguHfihnANCDaiHk9DQjCt4JUGgBMN7jhYszRg23l7Pqld0R84q\n"\
		"e05tpfRuFx4GQkRw/Vfp8BQ/jOpfDj812GQpXl1djhp8C42FlZhTKVMdmITgwyld\n"\
		"9qeTN+5vQBn3WzWwqi3Ui7O98x+6mJ+hg/wvNL1grfDxWp3UUtbvGROO6cqi4Our\n"\
		"xpx/0dCIQQdRnRFJs5JDUAMCAwEAAQ==\n"\
		"-----END PUBLIC KEY-----\n";


char e_sk[] = "-----BEGIN RSA PRIVATE KEY-----\n"\
		"MIIJJwIBAAKCAgEAthNaVTSoU5yagQai1RkVrNNccxhyYnxD8C+EoOjZvTLEalDH\n"\
		"0aOgY3Z0Aw0rKwz6G9iZl0GfGttaSqAJVeH9+ECLkWGFKpP2nqfl0XYuDmzng30J\n"\
		"Z9QzyldFa0b/7v5ngBT5gl51qGKg1Ie7Yj8NX+Psi3IGFSNDtN9BdDW4oOyoavVr\n"\
		"tMplZBv321OCBRPBCTqmHIfn5Wj8BdsJzGbkOHIhe77La+LlikvHMw+U7kcU4b9T\n"\
		"UDM3uAEtxYFYzfpzsoBhWATTeMM+y8lDZTcmOlZ8w0UYU7lKbH7t5ZwiPL90eCXZ\n"\
		"rUJTJIC4L79HMeaLbJPNgIMmVHzLAZG0drSfMZeVUs1UfTNiFkwSFHLfTzM3Ha9N\n"\
		"pRUKzgXjU047qaUp6PRs+ZQzgjXJ40dkHvpyIq8Dg0uMGmqWJn+MFvgiLVtdZTYX\n"\
		"atWPSY7lN43+GGJgxKn1JGhSRS4vhNPVJ+ogNUvfmSv/tYeZhMguHfihnANCDaiH\n"\
		"k9DQjCt4JUGgBMN7jhYszRg23l7Pqld0R84qe05tpfRuFx4GQkRw/Vfp8BQ/jOpf\n"\
		"Dj812GQpXl1djhp8C42FlZhTKVMdmITgwyld9qeTN+5vQBn3WzWwqi3Ui7O98x+6\n"\
		"mJ+hg/wvNL1grfDxWp3UUtbvGROO6cqi4Ourxpx/0dCIQQdRnRFJs5JDUAMCAwEA\n"\
		"AQKCAgAz8bd+wYQYPoBRuRvxYbOuV6pzZ5ESidM0DcpvwZ6E/JvOnPc01tKmLaAR\n"\
		"Usty/8PG9/dxkJYcqmHLEnc3Wj8DeR6YPlMof4gHYW0OYgHnuage+igZyA9ydY1n\n"\
		"epz/iPLsNo+sYth8To8FsyeBcu2U4siUS9ZyZ1okmuDoj3754ip2QUgWDvv4IIaI\n"\
		"uv1IB4e141aKozdEWrwzy3azKtOHSwBEdn1AbT4BQ3c0GQjrJbpOYTAanFIUG7n8\n"\
		"M9erA/3H+hpLay0Nsd4Tc3rXwHEWiaAUcXzsPdt+soOaUv3tiM1ojZvZcyeXLWXe\n"\
		"sVpVYSl7s3sYY9ImnmeoLfJnP0Z++ovf4Icc8Wl3A08EJN9v3STXHDMVGcUwJHmH\n"\
		"ajW/ib62dsePV/yXnhhkGl+jNP7GCwRM10sID4vvwqY39G+nYyNgrOF7r04osaBt\n"\
		"5mMgDDujhRt/hmBzecyPPXzvdWGddx2yNku4Y2Pbp+vS0+9azwFl5wEYbZmKIkJs\n"\
		"yXK2PlXTyV1UNm1vXGdq1edNBL3m1Xpg98qUsqKdVrnly7Xr+YdZRGxwJYnriWdy\n"\
		"rlkC5cyDvmtdiK0jmGkCtRpqFUpHc8cE3tafpYC9gIoetU96Z9lon4yjc4JdoknN\n"\
		"BOV6lFsLhMt649QFca4IDRFmrLUQXITaAR3x8rn6uDeeUQqMiQKCAQEA8NxJeNGS\n"\
		"itqWV4k833+gt/GI/YlHU0WbwZ+CfxJlI9Dh1l+f2QHoI5T7n3gUDn6s/6sNBNl5\n"\
		"dFUkQ4QvUw/ggXNGw2OuIHyzJc+MFv4iJGtCJjC8aOIRP+q4739sERX2+bPtlDpt\n"\
		"1QZbKsIxG73Enn2s30Wl7kSkBPRXLyI7BfJoRsPx95zCmMXdFNgLjDX+z86h8RcL\n"\
		"yPUvkcOjOVRxUw7HSL1/mfBDm7EVb5KHDfHFNe/MV0HGgBrXEQmyEbDpHo+FgXZ7\n"\
		"lWxTcAs1gAhyjVZL5+TSiGQkDq+lqYztKP6DCarUUmO7Xt2GBkpnWPaB4AB1ELA5\n"\
		"h0B8GIoqIxsDHwKCAQEAwYUmyjVRQl4P2RFi9k3wtzS1cfklGe3q7A02nPNFCFIH\n"\
		"P2DURHiStYZwMjcUrOnD5Y+ymFvbg81gElpEk8+p2bLcRVoWbyLFg5c69buld8Hk\n"\
		"7EA6dbXetUSvtovb8AcmsTRtvvxMiwPjcj54m2mFLoQ0ax/0EHFSD6Q6IatDdkEH\n"\
		"suMzOanJ7L+rekC7icjXhxt4XZqmGqb1yhZZXyxUtsQ17EG4BfNKf9V1QLdcxwUg\n"\
		"QHUvoQMyhmG/2VZmcGG5CndYQHrEUwcC/V1IGv0bHgAiivgyEGxyK5Xw8Eab6SEp\n"\
		"+siuoJnArQvXzj0fKb+eZcFrUXIXWLCKACak7rbanQKCAQBd0QARPlg9G41JxVvf\n"\
		"lj/Mxhjc4HbWuZtEnNPgmRKqE7eTn6WMilCYY3RrFgOB7deGsmKcy/Sh2nQIlFk2\n"\
		"mk2RPoyGmLH/V3MUPDZN0mkMFHcslvkcRUoAwnWMW+6E7TBI7MuSW/vh6Nrj2cX1\n"\
		"D0KhlUQbp25NU5ErmNzIsCWbWGNDhRkfoICvrwpBDzHYSEPbP5oWZSqi+NWxiilu\n"\
		"sDR/7Ja0/5LPLCW5ZA18au+N/+kX7DKEnksqJMo2ibdBas4yqCaRWcwjVJFfaA5L\n"\
		"s36jDMT+K0PR+meNDN+THiBWQmRaBJ81BEG8Xr7QfnSo00OdM7Blcq7FvtlXWaG2\n"\
		"ENRzAoIBAFq8G5vjs93yTGvygvwJPzwnMovlzUeDDgrvhxO/3DYyf8NvmxcfkWob\n"\
		"Rq/Es6T/ViJkZdCaASqjONBiEpVbYL/55N1/h9VD8GfqmHJvMP4do0BCPSbDx1Am\n"\
		"jFyC5vR75eBgPLS3hwwHHrWKJPJ5X0CYLBuuN7kd6OP8lGUE7/SaOQnfDT2UJmEi\n"\
		"3TvM7iGEUcm/T4hT4l3MuqGuKOk+19l51e6YQbhIgj29z+QwFQgblyuhxlswLJhl\n"\
		"bJZytEUtDJ2jp3VUGojzWKuC+TPuSLCgT5HpHVAmCMH246lE8gU0z4IOZhJXBMge\n"\
		"LWk9r0bb2WDMd40QeXHUFGBNvjdO9OECggEAa4Z3MUyeAXuofYa9nXbytgPH3SjF\n"\
		"7F8VQPJxmqyxy+2avn9NB7Xf5qlGpsQr9LB6QXGKmjnKjCyS8zwj9+CtQguPdfp1\n"\
		"JrCyNlBoATBu6ag71FpMOG2WB4RbSKUUHPC69YroQUg/qBIONDnEpRrTl+cfjoxX\n"\
		"3dm808istjB6BiutpuIn7GrAu/nb6a9uePDE+aUNZeyENatImX6yhKIQOA6TGLeA\n"\
		"n+hniDBvQtvC19rGWhp+zManhFKbmN4otiYWL6/d2vl5DVv26THcj91JQspiRDr7\n"\
		"JXG6Mf5DNcX/B2zdumjKGLQj7QwYY32hwvGXi5vNaB6RD3qbiwwhNU523A==\n"\
		"-----END RSA PRIVATE KEY-----\n";

// ****************************************************************************
// *              End of Section: ENCRYPTION KEYS (pk, sk)                    *
// ****************************************************************************

// ****************************************************************************
// *              Begin of Section: SIGNATURE KEYS (pk, sk)                   *
// ****************************************************************************

char s_pk[] = "-----BEGIN PUBLIC KEY-----\n"\
		"MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA3VeN+j6YxE1SUsMbYpPi\n"\
		"S3k5WotlsFDbJKmRGWIZEhk6sUllxOQF1NTzthQd4JFndRNQZImePBM0a/SWc7gC\n"\
		"UCGjEYUCIs7eQfLDNw1oBw9X88yVOwqdaPHvRXjzIxDA6Pl98CMPlQAPtd9VNLt0\n"\
		"e5YqkU1w2zD4XL41e23XUWlxkq/gvFiglZHQrJuaIiCl/fN5JlDyIeaHFglMZp3f\n"\
		"+GVUJeFyTOn/fDHPtQcEPCs9pr4/wFWPN084V+9/Pz6+tEbpG9nfDGwi2jWTe6Is\n"\
		"N7qRiPhLmqRd1SPK5blpara4VIY3XNNQhecfapYZmJhCkg/dWugV6L4iZlFB9N0n\n"\
		"TmeK5zvD+Bd1h/WZToDyMeCs/krfUnKP37guPqO1GEgARj75vErtgL7N5mzE+m2z\n"\
		"AkCLneLwg4W1RtPTH0Hqe5ZhtEY8ShWtG0y/52CVeVCCFVGss6wI2YdprEJiaCfJ\n"\
		"LxnppB0f1FRKV5lFBmFG8LmhM/yOZJwdFM3nSIGbNYDfTFTHMQU+b1rVLgR8EX53\n"\
		"4YjTtkdbQmH/qKcwKbgHzQJf9CcYD6z7ZtFglaG2FZWrMM0kERhkqe/8jj7SMDRk\n"\
		"MFe/V84n2dCfwI1GwekyLjD00a2HWmEf8TiwLszX8MMVPkdnBj7vjmNI2sw4ldnH\n"\
		"3sf7XgUYM/8jv1yTVw81UQcCAwEAAQ==\n"\
		"-----END PUBLIC KEY-----\n";


char s_sk[] = "-----BEGIN RSA PRIVATE KEY-----\n"\
		"MIIJKAIBAAKCAgEA3VeN+j6YxE1SUsMbYpPiS3k5WotlsFDbJKmRGWIZEhk6sUll\n"\
		"xOQF1NTzthQd4JFndRNQZImePBM0a/SWc7gCUCGjEYUCIs7eQfLDNw1oBw9X88yV\n"\
		"OwqdaPHvRXjzIxDA6Pl98CMPlQAPtd9VNLt0e5YqkU1w2zD4XL41e23XUWlxkq/g\n"\
		"vFiglZHQrJuaIiCl/fN5JlDyIeaHFglMZp3f+GVUJeFyTOn/fDHPtQcEPCs9pr4/\n"\
		"wFWPN084V+9/Pz6+tEbpG9nfDGwi2jWTe6IsN7qRiPhLmqRd1SPK5blpara4VIY3\n"\
		"XNNQhecfapYZmJhCkg/dWugV6L4iZlFB9N0nTmeK5zvD+Bd1h/WZToDyMeCs/krf\n"\
		"UnKP37guPqO1GEgARj75vErtgL7N5mzE+m2zAkCLneLwg4W1RtPTH0Hqe5ZhtEY8\n"\
		"ShWtG0y/52CVeVCCFVGss6wI2YdprEJiaCfJLxnppB0f1FRKV5lFBmFG8LmhM/yO\n"\
		"ZJwdFM3nSIGbNYDfTFTHMQU+b1rVLgR8EX534YjTtkdbQmH/qKcwKbgHzQJf9CcY\n"\
		"D6z7ZtFglaG2FZWrMM0kERhkqe/8jj7SMDRkMFe/V84n2dCfwI1GwekyLjD00a2H\n"\
		"WmEf8TiwLszX8MMVPkdnBj7vjmNI2sw4ldnH3sf7XgUYM/8jv1yTVw81UQcCAwEA\n"\
		"AQKCAgB6u7XqcRNcplNjuPRScRYmK431+x3j6rHZXn0qyg6EzqCQ9dYMmzPwlDSf\n"\
		"XgBDKd1oOdF2LikjvrJuui1C9WGy9TPq3woUKwlrICXHPRPV9lgaw/JrzrMCIkU/\n"\
		"DJYld4DArrd6lLZrNKGBg9lHaDpq3RW1hG9z22+cXYxiaMHgTsu/Pu8rASnqRfSk\n"\
		"AZQyIFBqxLsE2BWrXpzBR16p+BjdL7K9Xol2xLn6L9Sw2Lcsdf21nADVsZu8W50u\n"\
		"hYrVkwFEDZ3s556a0MtynAqvn3lNXZKdlLC3izTMnxA+2dTua2tC0zmXq0UXOtVc\n"\
		"0OZYivU+8LUZV1/usp94hM5B7O+BrkMXuF8+2Y1UuXB46OesE8SOZLNV41hB/AD5\n"\
		"izq/P0NKuLtnfb8F8mzNuk7jtnvofWaSCIncYiXaldhubwZ4Pd8/AWpWtr4isrp8\n"\
		"MvliBAQBrHVxzgFYEHtWQuuGdP57kKgfqfD+lHDSbMPufbGO3rKTigaJwrZL9oDb\n"\
		"JAH6y/P3JZfIb6wg5qOOxNV6ySkrxRgMLL9mFr/7AFQ+/8js/9NRA5wQGhYe3MvZ\n"\
		"HhF+jUuRWETrbW4t57HfWibMErJqqW1Hw9S9iQNfdCGpHic9jukfRdhU+jQIGCJS\n"\
		"Jxx3m0RFXaqjvinKu3RG0aBZJZnXTvLNS3C5UODx/qwHHMhn4QKCAQEA7mMZfkZ6\n"\
		"RovBv3draSwAVR+4kTEGUwHFm7LiH7M+JpsppmHAQ41EOgb8BsPn5njnPrNVQ5zH\n"\
		"pBRF06B/+Ozm4uXoTUM7q+488/uDYZpBWcRqFgYYcnBDWX0D4pv0r3lIbZTJitmp\n"\
		"OleSoCliHTDDHCJRkXHLkPjhcwE3wKOSTqFs379aQCNWldAQ3pfWxsacKoKEIy+N\n"\
		"FX2tJaapolYK7d9K2ENSnx2iPC80KbIjW84zSXHAtny2oJQVJkRJfTNmFK3HCRbw\n"\
		"VXEcugK34xvZKHVPeIOYnf17b6RjCQ7qfm2TzIgaeAQQXuq5eWjE0tDT/nlPpptT\n"\
		"oiKx+l3OCDRxrQKCAQEA7bIPilNe0HYHpnFVWKL2tnaEach8Jf4oZVSFUQCfeiq9\n"\
		"jOmzNTuFsALn+ztxkGM5kkskedKO2jPHk1LbMjAcGHM5cM5H3GzjPmJIATI+b1un\n"\
		"HZ5yZgLVHexigRHDEe8FI9Z//eHn/6mDiCcWWThTipshrKsDNepqjP50zxOnuCdd\n"\
		"YARjC6HXZ5PTvaFHxhBPU91C5tuTx5qut8khtyDjTAoxnS/0J+Mr5GIPM+sPdJmg\n"\
		"IfVLxdfdg+CwPZ8v4lecOD+g77JAXum2ZfRJJP8PFFee9MyQWQlAo8410WB98YBo\n"\
		"ue8Op/gjVJktCEIA8d3YJShACBtm/O2lNgoZk49sAwKCAQBvFIpqhtivSqwrVbAd\n"\
		"P72f7LoKHbf+sdKsrHwlnVYWkzKjLuVyFL938o05ccv7XtcSJmpSqRl14WRwKle3\n"\
		"XFxx2gOXDP8fLFIGtmoP7tsIwzdTr99wY9NePZyz2Uv5ACC5vxzysF3kq63NJ7tq\n"\
		"MrCXyX4MsePTV8Vl22lpQE8KiLm8wOODA3RzNpLFxib0VNOsE0kTRDMPkpI9+x0M\n"\
		"Yd+R2/x6LVSwhBR2yuAZlcJYIwtO68yKvC41QQth22wLBQpKFHesSp4Okjh6jI23\n"\
		"K+DSs2cmlttksxrzeLwVGng2FUVLhxgeWHZwZBus8R7VxS0jZM+yqKod4ODh8xfw\n"\
		"Rs+5AoIBACggv27i1EbtJi17RKVKHcyABaq2Jf9kAP3aXwdly4acYJPBgHekLNWi\n"\
		"J/fJ+Uoe9kE0XlwSp/s+cpr0ifzXEmcuTKw2pvsXhuWpQe6xxMiX+IaPkVCXedMr\n"\
		"bRXNIdmNOJjsRX9e/AO1YndB4gv97lygA/dah5g6kvJqot2yu8XjH2huVSpJkEv0\n"\
		"MPL6n1tYtYu69uhyeWhhPM3aLs7zxmu37NhUmCuDto0/4MErFdjhdp9FAh3Ma6Ev\n"\
		"9ZDxpuvXTpBQbaMJvMRJQHnF2/Na+/i7MiKxxkzBux/sWDmYyKpUu1loeljxE0Xc\n"\
		"9jVGr1Il2a+sM+MqfSCGHjYZTmgS9nECggEBAJLaskS14CtbRYXjzKUjnpPHStKN\n"\
		"yH82eZ60ZiPUxp4Safn1JagVh9CW0D3Cfj0WWkq+yxe/4jw1VL2D9xBL3178YTH1\n"\
		"oRCBdh4MhBn+PZDBBxOF6Bzjv9neqFVntL+94ncBd5L5dMg80C09imdwrUc2nW8U\n"\
		"8fZjIKXRnKsg9HPl74sY+aXCs/Nc+dtKmMb7Cc2dsGuPzBCtXkcUmwKc/kCayTq+\n"\
		"NJ4XfY0NrapRbmpvF0IRoStQ9MpvlaKraRVTK7arRu3qnCJZwP8RPc0388inxGAi\n"\
		"pOABpTTKAt2Hbv0gGODB7/b2q7WcllvnJ1ZYQi/QQ81w4rNHqmh56bWiMD8=\n"\
		"-----END RSA PRIVATE KEY-----\n";

// ****************************************************************************
// *              End of Section: SIGNATURE KEYS (pk, sk)                     *
// ****************************************************************************

// ****************************************************************************
// *              Begin of Section: ENCRYPTION KEY (Ks)                       *
// ****************************************************************************

sgx_aes_gcm_128bit_key_t *aeskey;

uint8_t *ivector;

size_t aeskey_size = 128 / 8;

size_t iv_size = 12;

/* A 256 bit key */
uint8_t *key = (uint8_t*)"01234567890123456789012345678901";

/* A 128 bit IV */
uint8_t *iv = (uint8_t*)"0123456789012345";

size_t iv_len = 16;

// ****************************************************************************
// *              End of Section: ENCRYPTION KEY (Ks)                         *
// ****************************************************************************

void printf(const char *fmt, ...)
{
	char buf[BUFSIZ] = {'\0'};
	va_list ap;
	va_start(ap, fmt);
	vsnprintf(buf, BUFSIZ, fmt, ap);
	va_end(ap);
	ocall_print(buf);
}


int clash_msg_write_file(char *path, char *data, size_t data_ln) {


	SGX_FILE *fd = sgx_fopen_auto_key(path, "w+");

	sgx_fwrite(data, sizeof(char), data_ln, fd);
	sgx_fclose(fd);

	return 1;
}


void printBytes(uint8_t *digest, size_t ln) {

	for(int i = 0; i < ln; i++) {
		printf("%02x", digest[i]);
	}
	printf("\n");
}


void clash_msg_init_generation()
{

	enc_key_pair key_pair;
	enc_key_pair sign_pair;
	printf("Encryption key pair generation...\n");
	printf("Signature key pair generation...\n\n");

	key_pair = PKE_KeyGen();
	sign_pair = PKE_KeyGen();

	clear_key_pair(&key_pair);
	clear_key_pair(&sign_pair);

	printf("Encryption key pair generated successfully\n");
	printf("Signature key pair generated successfully\n\n");
}

std::map<sgx_enclave_id_t, dh_session_t>g_src_session_info_map;

//Function pointer table containing the list of functions that the enclave exposes
const struct {
	size_t num_funcs;
	const void* table[0];
} func_table = {
	0,
	{
//			(const void*)e1_foo1_wrapper,
	}
};

//Makes use of the sample code function to establish a secure channel with the destination enclave (Test Vector)
uint32_t clash_create_session_e1_to_e3(sgx_enclave_id_t src_enclave_id,
										sgx_enclave_id_t dest_enclave_id)
{
	ATTESTATION_STATUS ke_status = SUCCESS;
	dh_session_t dest_session_info;

	//Core reference code function for creating a session
	ke_status = create_session(src_enclave_id, dest_enclave_id, &dest_session_info);

	//Insert the session information into the map under the corresponding destination enclave id
	if(ke_status == SUCCESS)
	{
		g_src_session_info_map.insert(std::pair<sgx_enclave_id_t, dh_session_t>(dest_enclave_id, dest_session_info));
	}
	memset(&dest_session_info, 0, sizeof(dh_session_t));
	return ke_status;
}

uint32_t clash_exchange_secret_e1_to_e3(sgx_enclave_id_t src_enclave_id,
										sgx_enclave_id_t dest_enclave_id)
{
	ATTESTATION_STATUS ke_status = SUCCESS;
	uint32_t target_fn_id, msg_type;
	char* marshalled_inp_buff;
	size_t marshalled_inp_buff_len;
	char* out_buff;
	size_t out_buff_len;
	dh_session_t *dest_session_info;
	size_t max_out_buff_size;
	char* retval;

	target_fn_id = 0;
	msg_type = ENCLAVE_TO_ENCLAVE_CALL;
	max_out_buff_size = 50;

	// generate a random aeskey and iv
	aeskey = (sgx_aes_gcm_128bit_key_t* )malloc(aeskey_size);
	ivector = (uint8_t *)malloc(iv_size);
	ke_status = generate_aeskey(aeskey, aeskey_size, ivector, iv_size);
	if(ke_status != SUCCESS)
	{
		return ke_status;
	}

	//Marshals the input parameters for calling function exchange_key_e1_e3 in Enclave3 into a input buffer
	ke_status = marshal_input_parameters_e1_to_e3(target_fn_id, msg_type, aeskey, aeskey_size, iv, iv_size, &marshalled_inp_buff, &marshalled_inp_buff_len);
	if(ke_status != SUCCESS)
	{
		return ke_status;
	}


	//Search the map for the session information associated with the destination enclave id of Enclave2 passed in
	std::map<sgx_enclave_id_t, dh_session_t>::iterator it = g_src_session_info_map.find(dest_enclave_id);
	if(it != g_src_session_info_map.end())
	{
		  dest_session_info = &it->second;
	}
	else
	{
		SAFE_FREE(marshalled_inp_buff);
		return INVALID_SESSION;
	}

	//Core Reference Code function
	ke_status = send_request_receive_response(src_enclave_id, dest_enclave_id, dest_session_info, marshalled_inp_buff,
											marshalled_inp_buff_len, max_out_buff_size, &out_buff, &out_buff_len);

	if(ke_status != SUCCESS)
	{
		SAFE_FREE(marshalled_inp_buff);
		SAFE_FREE(out_buff);
		return ke_status;
	}

	//Un-marshal the return value and output parameters from Enclave 3
	ke_status = unmarshal_retval_and_output_parameters_from_e3(out_buff, &retval);
	if(ke_status != SUCCESS)
	{
		SAFE_FREE(marshalled_inp_buff);
		SAFE_FREE(out_buff);
		return ke_status;
	}
	if ( *retval != SUCCESS)
	{
		SAFE_FREE(marshalled_inp_buff);
		SAFE_FREE(out_buff);
		return *retval;
	}

	SAFE_FREE(marshalled_inp_buff);
	SAFE_FREE(out_buff);
	SAFE_FREE(retval);
	return SUCCESS;
}


void clash_msg_pass_scope_generation(char *ptr, size_t len)
{

	char *data = (char*)malloc(1 * len);

	string r = "random";
	string s_j = "10110";
	string sep = ":";

	uint8_t hmac[256] = {0};
	uint32_t hmac_ln = 0;
	int hmac_status = 0;

	uint8_t tag[TAG_SIZE] = {0};

	uint8_t md[SHA512_DIGEST_LENGTH] = {0};

	/* Additional Data */
	uint8_t *additional =
			(uint8_t*)"secret tag";

	char pt[12] = {0};
	int ct_ln = 0, pt_ln = 0;
	int l = 0, ln = 0;

	uint8_t ct[12] = {0};

	/* Generate hash */
	clash_md_concat_pass_scope(md, (uint8_t*)r.c_str(), (uint8_t*)s_j.c_str());

	/* Generates HMAC */
	hmac_status = hmac_sign(md, key, hmac, &hmac_ln);

//	printf("[REV]Enclave 1:\t [clash_msg_pass_scope_generation] %d\n\t\t [hmac_status generation]\n", hmac_status);

	pt_ln = s_j.length();

	for(int i = 0; i < pt_ln; i++)
		pt[i] = s_j[i];

	/* Encrypt the data */
	ct_ln = gcm_encrypt((uint8_t*)pt, strlen((char*)pt),
			additional, strlen((char*)additional),
			key,
			iv, iv_len,
			ct, tag);

	int r_ln = strlen((char*)r.c_str());
	int tag_ln = strlen((char*)tag);

	int b64_r = get_base64_length(r_ln);
	int b64_ct = get_base64_length(ct_ln);
	int b64_hmac = get_base64_length(hmac_ln);
	int b64_tag = get_base64_length(tag_ln);

	uint8_t base64_r[b64_r] = {0};
	uint8_t base64_ct[b64_ct] = {0};
	uint8_t base64_hmac[b64_hmac] = {0};
	uint8_t base64_tag[b64_tag] = {0};

	/* Encode with Base64 */
	EVP_EncodeBlock(base64_r, (uint8_t*)r.c_str(), r_ln);
	EVP_EncodeBlock(base64_ct, ct, ct_ln);
	EVP_EncodeBlock(base64_tag, tag, tag_ln);
	EVP_EncodeBlock(base64_hmac, hmac, hmac_ln);

	/* Concatenate encoded data prior sending it */
	ln += snprintf(data+ln, len-ln, (char*)base64_r);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_ct);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_tag);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_hmac);
	ln += snprintf(data+ln, len-ln, sep.c_str());

	memcpy(ptr, data, strlen(data));
	free(data);

}

uint32_t clash_keyshare_msg_exchange_e1_to_e3(sgx_enclave_id_t src_enclave_id,
							sgx_enclave_id_t dest_enclave_id, char *ptr, size_t len)
{
	ATTESTATION_STATUS ke_status = SUCCESS;
	uint32_t target_fn_id, msg_type;
	char* marshalled_inp_buff;
	size_t marshalled_inp_buff_len;
	char* out_buff;
	size_t out_buff_len;
	dh_session_t *dest_session_info;
	size_t max_out_buff_size;
	char* secret_response;
	uint32_t secret_data;

	clash_msg_pass_scope_generation(ptr, len);

	target_fn_id = 0;
	msg_type = MESSAGE_EXCHANGE;
	max_out_buff_size = 5000;

	//Marshals the secret data into a buffer
	ke_status = marshal_message_exchange_input_parameters_e1_to_e3(target_fn_id, msg_type, ptr, len, &marshalled_inp_buff, &marshalled_inp_buff_len);
	if(ke_status != SUCCESS)
	{
		return ke_status;
	}
	//Search the map for the session information associated with the destination enclave id passed in
	std::map<sgx_enclave_id_t, dh_session_t>::iterator it = g_src_session_info_map.find(dest_enclave_id);
	if(it != g_src_session_info_map.end())
	{
		 dest_session_info = &it->second;
	}
	else
	{
		SAFE_FREE(marshalled_inp_buff);
		return INVALID_SESSION;
	}

	//Core Reference Code function
	ke_status = send_request_receive_response(src_enclave_id, dest_enclave_id, dest_session_info, marshalled_inp_buff,
												marshalled_inp_buff_len, max_out_buff_size, &out_buff, &out_buff_len);
	if(ke_status != SUCCESS)
	{
		SAFE_FREE(marshalled_inp_buff);
		SAFE_FREE(out_buff);
		return ke_status;
	}

	//Un-marshal the secret response data
	ke_status = unmarshal_retval_and_output_parameters_from_e3(out_buff, &secret_response);
	if(ke_status != SUCCESS)
	{
		SAFE_FREE(marshalled_inp_buff);
		SAFE_FREE(out_buff);
		return ke_status;
	}
	memcpy(ptr, secret_response, 1);

	SAFE_FREE(marshalled_inp_buff);
	SAFE_FREE(out_buff);
	SAFE_FREE(secret_response);
	return SUCCESS;
}

uint32_t clash_revocation_msg_exchange_e1_to_e3(sgx_enclave_id_t src_enclave_id,
							sgx_enclave_id_t dest_enclave_id, char *ptr, size_t len)
{
	ATTESTATION_STATUS ke_status = SUCCESS;
	uint32_t target_fn_id, msg_type;
	char* marshalled_inp_buff;
	size_t marshalled_inp_buff_len;
	char* out_buff;
	size_t out_buff_len;
	dh_session_t *dest_session_info;
	size_t max_out_buff_size;
	char* secret_response;
	uint32_t secret_data;

	target_fn_id = 1;
	msg_type = MESSAGE_EXCHANGE;
	max_out_buff_size = 5000;

	clash_msg_idx_req_generation(ptr, len);

	//Marshals the secret data into a buffer
	ke_status = marshal_message_exchange_input_parameters_e1_to_e3(target_fn_id, msg_type, ptr, len, &marshalled_inp_buff, &marshalled_inp_buff_len);
	if(ke_status != SUCCESS)
	{
		return ke_status;
	}
	//Search the map for the session information associated with the destination enclave id passed in
	std::map<sgx_enclave_id_t, dh_session_t>::iterator it = g_src_session_info_map.find(dest_enclave_id);
	if(it != g_src_session_info_map.end())
	{
		 dest_session_info = &it->second;
	}
	else
	{
		SAFE_FREE(marshalled_inp_buff);
		return INVALID_SESSION;
	}

	//Core Reference Code function
	ke_status = send_request_receive_response(src_enclave_id, dest_enclave_id, dest_session_info, marshalled_inp_buff,
												marshalled_inp_buff_len, max_out_buff_size, &out_buff, &out_buff_len);
	if(ke_status != SUCCESS)
	{
		SAFE_FREE(marshalled_inp_buff);
		SAFE_FREE(out_buff);
		return ke_status;
	}

	//Un-marshal the secret response data
	ke_status = unmarshal_retval_and_output_parameters_from_e3(out_buff, &secret_response);
	if(ke_status != SUCCESS)
	{
		SAFE_FREE(marshalled_inp_buff);
		SAFE_FREE(out_buff);
		return ke_status;
	}
	memcpy(ptr, secret_response, sizeof(1));

	SAFE_FREE(marshalled_inp_buff);
	SAFE_FREE(out_buff);
	SAFE_FREE(secret_response);
	return SUCCESS;
}


//Makes use of the sample code function to close a current session
uint32_t clash_close_session_e1_to_e3(sgx_enclave_id_t src_enclave_id,
										sgx_enclave_id_t dest_enclave_id)
{
	dh_session_t dest_session_info;
	ATTESTATION_STATUS ke_status = SUCCESS;
	//Search the map for the session information associated with the destination enclave id passed in
	std::map<sgx_enclave_id_t, dh_session_t>::iterator it = g_src_session_info_map.find(dest_enclave_id);
	if(it != g_src_session_info_map.end())
	{
		dest_session_info = it->second;
	}
	else
	{
		return NULL;
	}

	//Core reference code function for closing a session
	ke_status = close_session(src_enclave_id, dest_enclave_id);

	//Erase the session information associated with the destination enclave id
	g_src_session_info_map.erase(dest_enclave_id);
	return ke_status;
}

//Function that is used to verify the trust of the other enclave
//Each enclave can have its own way verifying the peer enclave identity
extern "C" uint32_t verify_peer_enclave_trust(sgx_dh_session_enclave_identity_t* peer_enclave_identity)
{
	if(!peer_enclave_identity)
	{
		return INVALID_PARAMETER_ERROR;
	}
	if(peer_enclave_identity->isv_prod_id != 0 || !(peer_enclave_identity->attributes.flags & SGX_FLAGS_INITTED))
		// || peer_enclave_identity->attributes.xfrm !=3)// || peer_enclave_identity->mr_signer != xx //TODO: To be hardcoded with values to check
	{
		return ENCLAVE_TRUST_ERROR;
	}
	else
	{
		return SUCCESS;
	}
}


//Dispatcher function that calls the approriate enclave function based on the function id
//Each enclave can have its own way of dispatching the calls from other enclave
extern "C" uint32_t enclave_to_enclave_call_dispatcher(char* decrypted_data,
														size_t decrypted_data_length,
														char** resp_buffer,
														size_t* resp_length)
{
	ms_in_msg_exchange_t *ms;
	uint32_t (*fn1)(ms_in_msg_exchange_t *ms, size_t, char**, size_t*);
	if(!decrypted_data || !resp_length)
	{
		return INVALID_PARAMETER_ERROR;
	}
	ms = (ms_in_msg_exchange_t *)decrypted_data;
	if(ms->target_fn_id >= func_table.num_funcs)
	{
		return INVALID_PARAMETER_ERROR;
	}
	fn1 = (uint32_t (*)(ms_in_msg_exchange_t*, size_t, char**, size_t*))func_table.table[ms->target_fn_id];
	return fn1(ms, decrypted_data_length, resp_buffer, resp_length);
}


//Generates the response from the request message
extern "C" uint32_t message_exchange_response_generator(char* decrypted_data,
														char** resp_buffer,
														size_t* resp_length)
{
	char *temp_buffer;
	char *p;
	size_t p_size;
	uint32_t ret;

	ms_in_msg_exchange_t *ms;

	if(!decrypted_data || !resp_length)
	{
		return INVALID_PARAMETER_ERROR;
	}

	ms = (ms_in_msg_exchange_t *)decrypted_data;

	if(umarshal_message_exchange_request(&p, &p_size, ms) != SUCCESS)
		return ATTESTATION_ERROR;

	clash_msg_idx_verification(p, p_size);

	temp_buffer = (char*)malloc(p_size);
	memcpy(temp_buffer, p, p_size);


	if(marshal_message_exchange_response(resp_buffer, resp_length, temp_buffer, p_size) != SUCCESS)
		return MALLOC_ERROR;

	return SUCCESS;

}


int clash_msg_rev_store(char *ptr, size_t len)
{
	int status = 0;

	char *path = "./Enclave1/lvs_store.sgx";

	status = clash_msg_write_file(path, ptr, len);

	if(status) {
		memset_s(ptr, len, 0, len);
	} else {
		memset_s(ptr, len, 0, len);
	}

	return status;
}


void clash_msg_token_verification(char *ptr, size_t len)
{
	uint8_t *unbase64_r = NULL;
	uint8_t *unbase64_ct_ptr = NULL;
	uint8_t *unbase64_tag_ptr = NULL;
	uint8_t *unbase64_hmac_ptr = NULL;
	uint8_t not_hmac[256] = {0};

	uint32_t hmac_ln = 0;

	uint8_t *additional =
			(uint8_t*)"secret tag";

	uint8_t pt[128] = {0};

	uint8_t md[SHA512_DIGEST_LENGTH] = {0};

	char *u_j = NULL;
	char *idx = NULL;

	const char delim[2] = ":";

	int i = 0;
	int pt_ln = 0, ct_ln = 0;
	int hmac_status = 0;
	char *next = NULL;
	char *tokens = strtok_r(ptr, delim, &next);
	/* Unpacking encoded data and decoding */
	do
	{
		if(i == 0)
		{
			int ln = strlen(tokens);
			unbase64_r = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			unbase64_r[ln] = '\0';
			i++;
			continue;
		}

		if(i == 1)
		{
			int ln = strlen(tokens);
			unbase64_ct_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			unbase64_ct_ptr[ln] = '\0';
			i++;
			continue;
		}

		if(i == 2)
		{
			int ln = strlen(tokens);
			unbase64_tag_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			unbase64_tag_ptr[ln] = '\0';
			i++;
			continue;
		}

		if(i == 3)
		{
			int ln = strlen(tokens);
			unbase64_hmac_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			unbase64_hmac_ptr[ln] = '\0';
			i++;
			continue;
		}
	}
	while((tokens = strtok_r(NULL, delim, &next)) != NULL);

	ct_ln = strlen((char*)unbase64_ct_ptr);


	pt_ln = gcm_decrypt(unbase64_ct_ptr, ct_ln,
						additional, strlen((char*)additional),
						unbase64_tag_ptr,
						key, iv, iv_len,
						pt);

	pt[pt_ln] = '\0';

	/* Unpacking concatenated data prior verification */
	char *next_pt = NULL;
	char *tokens_pt = strtok_r((char*)pt, delim, &next_pt);

	int j = 0;

	do
	{
		if(j == 0)
		{
			u_j = tokens_pt;
			j++;
			continue;
		}

		if(j == 1)
		{
			idx = tokens_pt;
			continue;
		}
	}
	while((tokens_pt = strtok_r(NULL, delim, &next_pt)) != NULL);

	clash_md_concat_token(md, unbase64_r, (uint8_t*)u_j,(uint8_t*)idx);

	/* Generates HMAC */
	hmac_sign(md, key, not_hmac, &hmac_ln);

	/* Verifying HMAC */
	hmac_status = hmac_verify(unbase64_hmac_ptr, not_hmac, hmac_ln);

//	printf("[REV]Enclave 1:\t [clash_msg_token_verification] %d\n\t\t [hmac_status verification]\n", hmac_status);

	if(hmac_status) {
		memcpy(ptr, "1", 1);
		goto erase;
	} else {
		memcpy(ptr, "0", 1);
		goto erase;
	}

	erase:
		free(unbase64_r);
		free(unbase64_ct_ptr);
		free(unbase64_hmac_ptr);
		free(unbase64_tag_ptr);
		free(tokens);
		free(tokens_pt);
}


void clash_msg_revoke_verification(char *ptr, size_t len)
{

	uint8_t *additional =
		(uint8_t*)"secret tag";

	uint8_t *unbase64_r = NULL;
	uint8_t *unbase64_ct_ptr = NULL;
	uint8_t *unbase64_hmac_ptr = NULL;
	uint8_t *unbase64_abe_ct_ptr = NULL;
	uint8_t *unbase64_tag_ptr = NULL;

	uint8_t md[SHA512_DIGEST_LENGTH] = {0};
	uint8_t not_hmac[SHA512_DIGEST_LENGTH] = {0};
	uint32_t hmac_ln = 0;

	char *u_j_ptr = NULL;
	char *u_l_ptr = NULL;
	char *n_ptr = NULL;
	uint8_t *assign_ptr = NULL;

	char *u_j = NULL;
	char *u_l = NULL;
	char *n = NULL;
	char *assign = NULL;

	u_j_ptr = u_j;
	u_l_ptr = u_l;
	n_ptr = n;
	assign_ptr = (uint8_t*)assign;

	const char sep[2] = ":";

	int i = 0;
	int pt_ln = 0, ct_ln = 0;
	int hmac_status = 0;

	char *next = NULL;
	char *tokens = strtok_r(ptr, sep, &next);

	if(tokens == NULL) {
		printf("Memory Allocation Failure\n");
	}

	do
	{
		if(i == 0)
		{
			int ln = strlen(tokens);
			unbase64_r = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 1)
		{
			int ln = strlen(tokens);
			unbase64_ct_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 2)
		{
			int ln = strlen(tokens);
			unbase64_hmac_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 3)
		{
			int ln = strlen(tokens);
			unbase64_tag_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 4)
		{
			int ln = strlen(tokens);
			unbase64_abe_ct_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			continue;
		}
	}
	while((tokens = strtok_r(NULL, sep, &next)) != NULL);

	ct_ln = strlen((char*)unbase64_ct_ptr);

	int tag_len = strlen((char*)unbase64_tag_ptr);

	char *pt = (char*)malloc(ct_ln * 1);

	if(pt == NULL){
		printf("Memory Allocation Failure\n");
	} else {
		memset_s(pt, ct_ln * 1, 0, ct_ln * 1);
	}

	/* Decrypt the cipher */
	pt_ln = gcm_decrypt(unbase64_ct_ptr, ct_ln,
						additional, strlen((char*)additional),
						unbase64_tag_ptr,
						key, iv, iv_len,
						(uint8_t*)pt);


	char *next_pt = NULL;
	char *tokens_pt = strtok_r(pt, sep, &next_pt);

	if(tokens_pt == NULL) {
		printf("Memory Allocation Failure\n");
	}

	int j = 0;

	do
	{
		if(j == 0)
		{
			u_j = tokens_pt;
			j++;
			continue;
		}
		if(j == 1)
		{
			u_l = tokens_pt;
			j++;
			continue;
		}

		if(j == 2)
		{
			n = tokens_pt;
			j++;
			continue;
		}
		if(j == 3)
		{
			assign = tokens_pt;
			continue;
		}
	}
	while((tokens_pt = strtok_r(NULL, sep, &next_pt)) != NULL);

	/* Generate hash from concatenated data */
	clash_rev_revoke_concat(md, unbase64_r, (uint8_t*)u_j, (uint8_t*)u_l, (uint8_t*)n, (uint8_t*)unbase64_abe_ct_ptr, (uint8_t*)assign);

	/* Generating HMAC */
	hmac_sign(md, key, not_hmac, &hmac_ln);

	/* Verifying HMAC */
	hmac_status = hmac_verify(unbase64_hmac_ptr, not_hmac, hmac_ln);

//	printf("[REV]Enclave 1:\t [clash_msg_revoke_verification] %d\n\t\t [hmac_status verification]\n", hmac_status);

	char *path = "./Enclave1/user_to_be_revoked.sgx";

	string user_to_be_revoked(u_l);
	string scope_to_be_revoked(n);
	string user_who_wants_to_revoke((char*)u_j);
	string data = user_to_be_revoked + ":" + scope_to_be_revoked + ":" + user_who_wants_to_revoke + ":";
	size_t data_len = data.length();

	if(hmac_status)
	{
		clash_msg_write_file(path, (char*)data.c_str(), data_len);
		memcpy(ptr, "1", 1);
		goto erase;
	} else {
		memcpy(ptr, "0", 1);
		goto erase;
	}

	erase:
		free(pt);
		free(unbase64_r);
		free(unbase64_ct_ptr);
		free(unbase64_hmac_ptr);
		free(unbase64_tag_ptr);
		free(unbase64_abe_ct_ptr);
		free(tokens);
		free(tokens_pt);

}


void clash_msg_idx_req_generation(char *ptr, size_t len)
{

	char *data = (char*)malloc(len * 1);
	if(data == NULL)
	{
		printf("Memory Allocation Failure\n");
	} else{
		memset_s(data, len * 1, 0, len * 1);
	}

	string r = "rand";
	string u_l = "10.Alexandros";
	string sep = ":";

	char abe_key[ABE_SIZE] = {0};

	uint8_t md[SHA512_DIGEST_LENGTH] = {0};

	uint8_t hmac[SHA512_DIGEST_LENGTH] = {0};
	uint32_t hmac_ln = 0;
	int hmac_status = 0;

	uint8_t tag[TAG_SIZE] = {0};

	int b64_r_ln = 0,
		b64_ct_ln = 0,
		b64_tag_ln = 0,
		b64_abe_ln = 0,
		b64_hmac_ln = 0;

	uint8_t *additional =
			(uint8_t*)"secret tag";

	int ct_ln = 0, pt_ln = 0;
	int l = 0, ln = 0;

	/* Load ABE key from a file to a buffer */
	memcpy_s(abe_key, ABE_SIZE, cpabe_key, strlen(cpabe_key));

	/* Generate hash from concatenated data */
	clash_rev_assign_concat(md, (uint8_t*)r.c_str(), (uint8_t*)u_l.c_str(), (uint8_t*)abe_key);

	/* Generates HMAC */
	hmac_status = hmac_sign(md, key, hmac, &hmac_ln);

//	printf("[REV]Enclave 1:\t [clash_msg_idx_req_generation] %d\n\t\t [hmac_status generation]\n", hmac_status);

	pt_ln = strlen(u_l.c_str()) + 1;

	char pt[pt_ln] = {0};

	ct_ln = pt_ln;

	l += snprintf(pt+l, pt_ln-l, u_l.c_str());

	uint8_t ct[ct_ln] = {0};

	/* Encrypt the data */
	ct_ln = gcm_encrypt((uint8_t*)pt, pt_ln,
			additional, strlen((char*)additional),
			key,
			iv, iv_len,
			ct, tag);

	int r_ln = strlen(r.c_str());
	int tag_ln = strlen((char*)tag);

	b64_r_ln = get_base64_length(r_ln);
	b64_ct_ln = get_base64_length(ct_ln);
	b64_hmac_ln = get_base64_length(SHA512_DIGEST_LENGTH);
	b64_tag_ln = get_base64_length(tag_ln);
	b64_abe_ln = get_base64_length(ABE_SIZE);

	uint8_t base64_r[b64_r_ln] = {0};
	uint8_t base64_ct[b64_ct_ln] = {0};
	uint8_t base64_hmac[b64_hmac_ln] = {0};
	uint8_t base64_tag[b64_tag_ln] = {0};
	uint8_t base64_abe_key[b64_abe_ln] = {0};

	/* Encode with Base64 */
	EVP_EncodeBlock(base64_r, (uint8_t*)r.c_str(), r_ln);
	EVP_EncodeBlock(base64_ct, ct, ct_ln);
	EVP_EncodeBlock(base64_tag, tag, tag_ln);
	EVP_EncodeBlock(base64_hmac, hmac, SHA512_DIGEST_LENGTH);
	EVP_EncodeBlock(base64_abe_key, (uint8_t*)abe_key, ABE_SIZE);

	/* Concatenate encoded data prior sending it */
	ln += snprintf(data+ln, len-ln, (char*)base64_r);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_ct);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_tag);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_hmac);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_abe_key);
	ln += snprintf(data+ln, len-ln, sep.c_str());

	memcpy(ptr, data, strlen(data));
	free(data);
}

int clash_get_local_lvs(char *path, char *data, size_t data_sz)
{

	SGX_FILE *fd = sgx_fopen_auto_key(path, "r");
	sgx_fseek(fd, 0L, SEEK_END);

	int pt_ln = sgx_ftell(fd);
	char *pt = (char*)malloc(pt_ln);

	sgx_fseek(fd, 0L, SEEK_SET);

	while(!sgx_feof(fd)) {
		sgx_fread(pt, pt_ln, 1, fd);
	}

	sgx_fclose(fd);

	memcpy_s(data, data_sz , pt, strlen(pt));
	free(pt);

	return pt_ln;

}


void clash_msg_idx_verification(char *ptr, size_t len)
{
	uint8_t *additional =
		(uint8_t*)"secret tag";

	uint8_t *unbase64_r = NULL;
	uint8_t *unbase64_ct_ptr = NULL;
	uint8_t *unbase64_hmac_ptr = NULL;
	uint8_t *unbase64_tag_ptr = NULL;
	uint8_t not_hmac[256] = {0};

	uint32_t hmac_ln = 0;

	uint8_t pt[512] = {0};
	uint8_t md[SHA512_DIGEST_LENGTH] = {0};

	char *u_i = NULL;
	char *idx = NULL;

	const char delim[2] = ":";

	int i = 0;
	int pt_ln = 0, ct_ln = 0;
	int hmac_status = 0;

	char *next = NULL;
	char *tokens = strtok_r(ptr, delim, &next);

	if(tokens == NULL) {
		printf("NULL is returned\n");
	}
	else
	{
		do
		{
			if(i == 0)
			{
				int ln = strlen(tokens);
				unbase64_r = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
				i++;
				continue;
			}

			if(i == 1)
			{
				int ln = strlen(tokens);
				unbase64_ct_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
				i++;
				continue;
			}

			if(i == 2)
			{
				int ln = strlen(tokens);
				unbase64_tag_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
				i++;
				continue;
			}

			if(i == 3)
			{
				int ln = strlen(tokens);
				unbase64_hmac_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
				i++;
				continue;
			}
		}
		while((tokens = strtok_r(NULL, delim, &next)) != NULL);
	}

	ct_ln = strlen((char*)unbase64_ct_ptr);

	int tag_len = strlen((char*)unbase64_tag_ptr);

	unbase64_ct_ptr[ct_ln] = '\0';
	unbase64_tag_ptr[tag_len] = '\0';

	pt_ln = gcm_decrypt(unbase64_ct_ptr, ct_ln,
						additional, strlen((char*)additional),
						unbase64_tag_ptr,
						key, iv, iv_len,
						pt);

	pt[pt_ln] = '\0';

	char *next_pt = NULL;
	char *tokens_pt = strtok_r((char*)pt, delim, &next_pt);
	int j = 0;

	if(tokens_pt == NULL)
	{
		printf("NULL is returned \n");
	} else {
		do
		{
			if(j == 0)
			{
				u_i = tokens_pt;
				j++;
				continue;
			}

			if(j == 1)
			{
				idx = tokens_pt;
				j++;
				continue;
			}
		}
		while((tokens_pt = strtok_r(NULL, delim, &next_pt)) != NULL);
	}

	//CONCATENATION
	clash_m_idx_concat(md, unbase64_r, (uint8_t*)u_i, (uint8_t*)idx);

	/* Generates HMAC */
	hmac_sign(md, key, not_hmac, &hmac_ln);

	/* Verifying HMAC */
	hmac_status = hmac_verify(unbase64_hmac_ptr, not_hmac, hmac_ln);

//	printf("[REV]Enclave 1:\t [clash_msg_idx_verification] %d\n\t\t [hmac_status verification]\n", hmac_status);

	if(hmac_status) {

		char buffer[256] = {0};

		size_t buffer_sz = sizeof(buffer) / sizeof(buffer[0]);

		clash_get_local_lvs("./Enclave1/lvs_store.sgx", buffer, buffer_sz);

		string idx = "";
		string keys = "";
		string values = "";

		const char delim[2] = "&";

		char *nex = NULL;
		char *t = strtok_r(buffer, delim, &nex);

		int counter = 0;
		do
		{
			if(counter == 0)
			{
				idx.assign(t);
				counter++;
				continue;
			}

			if(counter == 1)
			{
				keys.assign(t);
				counter++;
				continue;
			}
			if(counter == 2)
			{
				values.assign(t);
				continue;
			}

		}
		while((t = strtok_r(NULL, delim, &nex)) != NULL);

		string kk = idx;
		string k = keys;
		string v = values;

		map<string, map<string, string>> rev_lvs;
		map<string, string> rev_scopes;

		string k_sep = ":";
		string v_sep = ":";

		size_t k_pos = 0;
		size_t v_pos = 0;

		string k_tk;
		string v_tk;

		while((k_pos = k.find(k_sep))!=string::npos && (v_pos = v.find(v_sep))!=string::npos) {
			k_tk = k.substr(0, k_pos);
			v_tk = v.substr(0, v_pos);
			rev_scopes.insert(make_pair(k_tk,v_tk));
			k.erase(0, k_pos + k_sep.length());
			v.erase(0, v_pos + v_sep.length());
		}

		rev_lvs.insert(make_pair(kk, rev_scopes));


		char *path_to_u_l = "./Enclave1/user_to_be_revoked.sgx";
		char buf[512] = {0};

		clash_get_local_lvs(path_to_u_l, buf, sizeof(buf));

		string n_p = "";
		int n_pos = 0;

		string u_l = "";
		string u_j = "";

		char *n = NULL;
		char *tk = strtok_r(buf, ":", &n);

		int x = 0;

		do
		{
			if(x == 0)
			{
				u_l.assign(tk);
				x++;
				continue;
			}

			if(x == 1)
			{
				n_pos=stoi(tk);
				x++;
				continue;
			}

			if(x == 2)
			{
				u_j.assign(tk);
				x++;
				continue;
			}

		}
		while((tk = strtok_r(NULL, ":", &n)) != NULL);

		string u_j_scope = "";
		string u_l_scope = "";
		string new_lvs = "";


		map<string,string> :: iterator eptr;
		map<string,string> :: iterator cptr;
		for( map<string,map<string,string> >::iterator ptr=rev_lvs.begin();ptr!=rev_lvs.end(); ptr++) {
//			printf("old list: idx %s\n\n", ptr->first);

			for(map<string,string> :: iterator eptr=ptr->second.begin();eptr!=ptr->second.end(); eptr++){
//				printf("KEY=%s\nVALUE=%s\n", eptr->first, eptr->second);
			}
			eptr = ptr->second.find(u_j);

//			printf("\nu_j=%s,scope=%s\n", u_j, eptr->second);

			u_j_scope = eptr->second;

			if(u_j_scope[n_pos] == '1') {
				cptr = ptr->second.find(u_l);
//				printf("\ncurrent u_l scope: %s %s\n", u_l, cptr->second);
				u_l_scope = cptr->second;
				u_l_scope[n_pos] = '0';
				cptr->second =u_l_scope;
//				printf("revoked u_l scope: %s %s\n", u_l, cptr->second);
			}

//			printf("\nnew_list: idx %s\n\n", ptr->first);
			keys = "";
			values = "";
			for(map<string,string> :: iterator cptr=ptr->second.begin();cptr!=ptr->second.end(); cptr++){
//				printf("KEY=%s\nVALUE=%s\n", cptr->first, cptr->second);
				keys+=cptr->first + ":";
				values+= cptr->second + ":";

			}

		}

		new_lvs = idx + "&" + keys + "&" + values + "&";

		size_t new_lvs_ln = new_lvs.length();
		char *path = "./Enclave1/lvs_store.sgx";
		clash_msg_write_file(path,(char*)new_lvs.c_str(), new_lvs_ln);

	}

	if(hmac_status)
	{
		memcpy(ptr, "1", 1);
		goto erase;
	} else {
		memcpy(ptr, "0", 1);
		goto erase;
	}
	erase:
		free(unbase64_r);
		free(unbase64_ct_ptr);
		free(unbase64_hmac_ptr);
		free(unbase64_tag_ptr);
		free(tokens);
		free(tokens_pt);
}
