#ifndef UTILITY_E2_H__
#define UTILITY_E2_H__

#include "stdint.h"

#ifdef __cplusplus
extern "C" {
#endif

void handleErrors(void);

int aes_encrypt(uint8_t *pt, int pt_ln, uint8_t* key, uint8_t *iv, uint8_t *ct);
int aes_decrypt(uint8_t *ct, int ct_ln, uint8_t *key, uint8_t *iv, uint8_t* pt);

int gcm_encrypt(uint8_t *plaintext, int plaintext_len,
				uint8_t *aad, int aad_len,
				uint8_t *key,
				uint8_t *iv, int iv_len,
				uint8_t *ciphertext,
				uint8_t *tag);

int gcm_decrypt(uint8_t *ciphertext, int ciphertext_len,
				uint8_t *aad, int aad_len,
				uint8_t *tag,
				uint8_t *key,
				uint8_t *iv, int iv_len,
				uint8_t *plaintext);

int hmac_sign(uint8_t *data, uint8_t *key, uint8_t *hmac, uint32_t *hmac_ln);
int hmac_verify(uint8_t *hmac, uint8_t *not_hmac, uint32_t hmac_ln);
uint8_t *base64_decode(uint8_t *base64, uint32_t *len);
int get_base64_length(int ct_ln);



void clash_store_req_concat(uint8_t *md, uint8_t *r, uint8_t *credentials, uint8_t *store_req);
void clash_msg_ver_concat(uint8_t *md, uint8_t *r, uint8_t *u_i, uint8_t *auth);
void clash_msg_search_concat(uint8_t *md, uint8_t *token, uint8_t *ct, uint8_t *ct_abe, uint8_t *hmac);
void clash_msg_key_generation_concat(uint8_t *md, uint8_t *u_j, uint8_t *t, uint8_t *idx, uint8_t *s_j, uint8_t *abe_ct);


#ifdef __cplusplus
 }
#endif
#endif
