#include "sgx_eid.h"
#include "EnclaveMessageExchange.h"
#include "error_codes.h"
#include "Utility_E2.h"
#include "stdlib.h"
#include "string.h"
#include <mbusafecrt.h>

#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <openssl/err.h>
#include <openssl/sha.h>
#include <openssl/pem.h>
#include <openssl/bio.h>
#include <openssl/rsa.h>

void handleErrors(void) {
	ERR_get_error();
	abort();
}


uint8_t *base64_decode(uint8_t *base64, uint32_t *len)
{
	uint8_t *ret = NULL;
	uint32_t bin_len;

	bin_len = (((strlen((char*)base64) + 3)/4) * 3);
	ret = (uint8_t*)malloc(bin_len);
	*len = EVP_DecodeBlock(ret, base64, strlen((char*)base64));
	return ret;

}

int get_base64_length(int ct_ln)
{
	int b64_length = 0;

	b64_length = ct_ln / 3 * 4;

	return b64_length + 65;
}

int gcm_encrypt(uint8_t *plaintext, int plaintext_len,
				uint8_t *aad, int aad_len,
				uint8_t *key,
				uint8_t *iv, int iv_len,
				uint8_t *ciphertext,
				uint8_t *tag)
{
	EVP_CIPHER_CTX *ctx;

	int len;

	int ciphertext_len;


	/* Create and initialise the context */
	if(!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	/* Initialise the encryption operation. */
	if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL))
		handleErrors();

	/*
	 * Set IV length if default 12 bytes (96 bits) is not appropriate
	 */
	if(1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, iv_len, NULL))
		handleErrors();

	/* Initialise key and IV */
	if(1 != EVP_EncryptInit_ex(ctx, NULL, NULL, key, iv))
		handleErrors();

	/*
	 * Provide any AAD data. This can be called zero or more times as
	 * required
	 */
	if(1 != EVP_EncryptUpdate(ctx, NULL, &len, aad, aad_len))
		handleErrors();

	/*
	 * Provide the message to be encrypted, and obtain the encrypted output.
	 * EVP_EncryptUpdate can be called multiple times if necessary
	 */
	if(1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
		handleErrors();
	ciphertext_len = len;

	/*
	 * Finalise the encryption. Normally ciphertext bytes may be written at
	 * this stage, but this does not occur in GCM mode
	 */
	if(1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))
		handleErrors();
	ciphertext_len += len;

	/* Get the tag */
	if(1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_GET_TAG, 16, tag))
		handleErrors();

	/* Clean up */
	EVP_CIPHER_CTX_free(ctx);

	return ciphertext_len;
}


int gcm_decrypt(uint8_t *ciphertext, int ciphertext_len,
				uint8_t *aad, int aad_len,
				uint8_t *tag,
				uint8_t *key,
				uint8_t *iv, int iv_len,
				uint8_t *plaintext)
{
	EVP_CIPHER_CTX *ctx;
	int len;
	int plaintext_len;
	int ret;

	/* Create and initialise the context */
	if(!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	/* Initialise the decryption operation. */
	if(!EVP_DecryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL))
		handleErrors();

	/* Set IV length. Not necessary if this is 12 bytes (96 bits) */
	if(!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, iv_len, NULL))
		handleErrors();

	/* Initialise key and IV */
	if(!EVP_DecryptInit_ex(ctx, NULL, NULL, key, iv))
		handleErrors();

	/*
	 * Provide any AAD data. This can be called zero or more times as
	 * required
	 */
	if(!EVP_DecryptUpdate(ctx, NULL, &len, aad, aad_len))
		handleErrors();

	/*
	 * Provide the message to be decrypted, and obtain the plaintext output.
	 * EVP_DecryptUpdate can be called multiple times if necessary
	 */
	if(!EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
		handleErrors();
	plaintext_len = len;

	/* Set expected tag value. Works in OpenSSL 1.0.1d and later */
	if(!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_TAG, 16, tag))
		handleErrors();

	/*
	 * Finalise the decryption. A positive return value indicates success,
	 * anything else is a failure - the plaintext is not trustworthy.
	 */
	ret = EVP_DecryptFinal_ex(ctx, plaintext + len, &len);

	/* Clean up */
	EVP_CIPHER_CTX_free(ctx);

	if(ret > 0) {
		/* Success */
		plaintext_len += len;
		return plaintext_len;
	} else {
		/* Verify failed */
		return -1;
	}
}

int hmac_sign(uint8_t *data, uint8_t *key, uint8_t *hmac, uint32_t *hmac_ln) {

	HMAC_CTX *ctx;

	if(!(ctx = HMAC_CTX_new()))
		handleErrors();

	if(1 != HMAC_Init_ex(ctx, key, strlen((char*)key), EVP_sha256(), NULL))
		handleErrors();

	if(1 != HMAC_Update(ctx, data, strlen((char*)data)))
		handleErrors();

	if(1 != HMAC_Final(ctx, hmac, hmac_ln)) {
		handleErrors();
		goto erase;
		return 0;
	}

	goto erase;

	erase:
		HMAC_CTX_free(ctx);

	return 1;
}

int hmac_verify(uint8_t *hmac, uint8_t *not_hmac, uint32_t hmac_ln) {

	for (int i=0; i!=hmac_ln; i++) {
		if (not_hmac[i] != hmac[i]) {
			 return 0;
		}
	 }
	return 1;
}


int aes_encrypt(uint8_t *pt, int pt_ln, uint8_t* key, uint8_t *iv, uint8_t *ct) {

	EVP_CIPHER_CTX *ctx;

	int ln, ct_ln;

	if(!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv))
		handleErrors();

	if(1 != EVP_EncryptUpdate(ctx, ct, &ln, pt, pt_ln))
		handleErrors();

	ct_ln = ln;

	if (1 != EVP_EncryptFinal_ex(ctx, ct + ln, &ln))
		handleErrors();

	ct_ln = ln;

	EVP_CIPHER_CTX_free(ctx);

	return ct_ln;
}


int aes_decrypt(uint8_t *ct, int ct_ln, uint8_t *key, uint8_t *iv, uint8_t* pt) {

	EVP_CIPHER_CTX *ctx;

	int ln, pt_ln;

	if(!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	EVP_CIPHER_CTX_set_padding(ctx, 0);

	if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv))
		handleErrors();

	if(1 != EVP_DecryptUpdate(ctx, pt, &ln, ct, ct_ln))
		handleErrors();

	pt_ln = ln;

	if (1 != EVP_DecryptFinal_ex(ctx, pt + ln, &ln))
		handleErrors();
	pt_ln += ln;

	EVP_CIPHER_CTX_free(ctx);

	return pt_ln;
}

void clash_store_req_concat(uint8_t *md, uint8_t *r, uint8_t *credentials, uint8_t *store_req)
{
	SHA512_CTX c;
	SHA512_Init(&c);

	SHA512_Update(&c, r, strlen((char*)r));
	SHA512_Update(&c, credentials, strlen((char*)credentials));
	SHA512_Update(&c, store_req, strlen((char*)store_req));

	SHA512_Final(md, &c);

}

void clash_msg_ver_concat(uint8_t *md, uint8_t *r, uint8_t *u_i, uint8_t *auth)
{
	SHA512_CTX c;
	SHA512_Init(&c);

	SHA512_Update(&c, r, strlen((char*)r));
	SHA512_Update(&c, auth, strlen((char*)auth));
	SHA512_Update(&c, u_i, strlen((char*)u_i));

	SHA512_Final(md, &c);

}

void clash_msg_search_concat(uint8_t *md, uint8_t *token, uint8_t *ct, uint8_t *ct_abe, uint8_t *hmac)
{
	SHA512_CTX c;
	SHA512_Init(&c);

	SHA512_Update(&c, token, strlen((char*)token));
	SHA512_Update(&c, ct, strlen((char*)ct));
	SHA512_Update(&c, ct_abe, strlen((char*)ct_abe));
	SHA512_Update(&c, hmac, strlen((char*)hmac));

	SHA512_Final(md, &c);

}

void clash_msg_key_generation_concat(uint8_t *md, uint8_t *u_j, uint8_t *t, uint8_t *idx, uint8_t *s_j, uint8_t *abe_ct)
{
	SHA512_CTX c;
	SHA512_Init(&c);

	SHA512_Update(&c, u_j, strlen((char*)u_j));
	SHA512_Update(&c, t, strlen((char*)t));
	SHA512_Update(&c, idx, strlen((char*)idx));
	SHA512_Update(&c, s_j, strlen((char*)s_j));
	SHA512_Update(&c, abe_ct, strlen((char*)abe_ct));

	SHA512_Final(md, &c);

}
