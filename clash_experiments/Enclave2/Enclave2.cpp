
#include "Enclave2_t.h"
#include "EnclaveMessageExchange.h"
#include "error_codes.h"
#include "Utility_E2.h"

#include <stdarg.h>
#include <stdio.h>
#include <string>
#include <stdlib.h>

#include "/opt/intel/Linux/package/include/tSgxSSL_api.h"

#include "sgx_eid.h"

#include <openssl/rsa.h>
#include <openssl/bn.h>
#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/evp.h>
#include <openssl/sha.h>
#include <openssl/obj_mac.h>
#include <openssl/rand.h>
#include <openssl/err.h>

#include <openssl/hmac.h>
#include <openssl/aes.h>
#include <openssl/conf.h>

#include <mbusafecrt.h>

#define __STDC_WANT_LIB_EXT1__ 1

using namespace std;

#define RSA_KEY_SIZE 4096
#define PADDING RSA_PKCS1_OAEP_PADDING

char e_pk[] = "-----BEGIN PUBLIC KEY-----\n"\
		"MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAthNaVTSoU5yagQai1RkV\n"\
		"rNNccxhyYnxD8C+EoOjZvTLEalDH0aOgY3Z0Aw0rKwz6G9iZl0GfGttaSqAJVeH9\n"\
		"+ECLkWGFKpP2nqfl0XYuDmzng30JZ9QzyldFa0b/7v5ngBT5gl51qGKg1Ie7Yj8N\n"\
		"X+Psi3IGFSNDtN9BdDW4oOyoavVrtMplZBv321OCBRPBCTqmHIfn5Wj8BdsJzGbk\n"\
		"OHIhe77La+LlikvHMw+U7kcU4b9TUDM3uAEtxYFYzfpzsoBhWATTeMM+y8lDZTcm\n"\
		"OlZ8w0UYU7lKbH7t5ZwiPL90eCXZrUJTJIC4L79HMeaLbJPNgIMmVHzLAZG0drSf\n"\
		"MZeVUs1UfTNiFkwSFHLfTzM3Ha9NpRUKzgXjU047qaUp6PRs+ZQzgjXJ40dkHvpy\n"\
		"Iq8Dg0uMGmqWJn+MFvgiLVtdZTYXatWPSY7lN43+GGJgxKn1JGhSRS4vhNPVJ+og\n"\
		"NUvfmSv/tYeZhMguHfihnANCDaiHk9DQjCt4JUGgBMN7jhYszRg23l7Pqld0R84q\n"\
		"e05tpfRuFx4GQkRw/Vfp8BQ/jOpfDj812GQpXl1djhp8C42FlZhTKVMdmITgwyld\n"\
		"9qeTN+5vQBn3WzWwqi3Ui7O98x+6mJ+hg/wvNL1grfDxWp3UUtbvGROO6cqi4Our\n"\
		"xpx/0dCIQQdRnRFJs5JDUAMCAwEAAQ==\n"\
		"-----END PUBLIC KEY-----\n";

char e_sk[] = "-----BEGIN RSA PRIVATE KEY-----\n"\
		"MIIJJwIBAAKCAgEAthNaVTSoU5yagQai1RkVrNNccxhyYnxD8C+EoOjZvTLEalDH\n"\
		"0aOgY3Z0Aw0rKwz6G9iZl0GfGttaSqAJVeH9+ECLkWGFKpP2nqfl0XYuDmzng30J\n"\
		"Z9QzyldFa0b/7v5ngBT5gl51qGKg1Ie7Yj8NX+Psi3IGFSNDtN9BdDW4oOyoavVr\n"\
		"tMplZBv321OCBRPBCTqmHIfn5Wj8BdsJzGbkOHIhe77La+LlikvHMw+U7kcU4b9T\n"\
		"UDM3uAEtxYFYzfpzsoBhWATTeMM+y8lDZTcmOlZ8w0UYU7lKbH7t5ZwiPL90eCXZ\n"\
		"rUJTJIC4L79HMeaLbJPNgIMmVHzLAZG0drSfMZeVUs1UfTNiFkwSFHLfTzM3Ha9N\n"\
		"pRUKzgXjU047qaUp6PRs+ZQzgjXJ40dkHvpyIq8Dg0uMGmqWJn+MFvgiLVtdZTYX\n"\
		"atWPSY7lN43+GGJgxKn1JGhSRS4vhNPVJ+ogNUvfmSv/tYeZhMguHfihnANCDaiH\n"\
		"k9DQjCt4JUGgBMN7jhYszRg23l7Pqld0R84qe05tpfRuFx4GQkRw/Vfp8BQ/jOpf\n"\
		"Dj812GQpXl1djhp8C42FlZhTKVMdmITgwyld9qeTN+5vQBn3WzWwqi3Ui7O98x+6\n"\
		"mJ+hg/wvNL1grfDxWp3UUtbvGROO6cqi4Ourxpx/0dCIQQdRnRFJs5JDUAMCAwEA\n"\
		"AQKCAgAz8bd+wYQYPoBRuRvxYbOuV6pzZ5ESidM0DcpvwZ6E/JvOnPc01tKmLaAR\n"\
		"Usty/8PG9/dxkJYcqmHLEnc3Wj8DeR6YPlMof4gHYW0OYgHnuage+igZyA9ydY1n\n"\
		"epz/iPLsNo+sYth8To8FsyeBcu2U4siUS9ZyZ1okmuDoj3754ip2QUgWDvv4IIaI\n"\
		"uv1IB4e141aKozdEWrwzy3azKtOHSwBEdn1AbT4BQ3c0GQjrJbpOYTAanFIUG7n8\n"\
		"M9erA/3H+hpLay0Nsd4Tc3rXwHEWiaAUcXzsPdt+soOaUv3tiM1ojZvZcyeXLWXe\n"\
		"sVpVYSl7s3sYY9ImnmeoLfJnP0Z++ovf4Icc8Wl3A08EJN9v3STXHDMVGcUwJHmH\n"\
		"ajW/ib62dsePV/yXnhhkGl+jNP7GCwRM10sID4vvwqY39G+nYyNgrOF7r04osaBt\n"\
		"5mMgDDujhRt/hmBzecyPPXzvdWGddx2yNku4Y2Pbp+vS0+9azwFl5wEYbZmKIkJs\n"\
		"yXK2PlXTyV1UNm1vXGdq1edNBL3m1Xpg98qUsqKdVrnly7Xr+YdZRGxwJYnriWdy\n"\
		"rlkC5cyDvmtdiK0jmGkCtRpqFUpHc8cE3tafpYC9gIoetU96Z9lon4yjc4JdoknN\n"\
		"BOV6lFsLhMt649QFca4IDRFmrLUQXITaAR3x8rn6uDeeUQqMiQKCAQEA8NxJeNGS\n"\
		"itqWV4k833+gt/GI/YlHU0WbwZ+CfxJlI9Dh1l+f2QHoI5T7n3gUDn6s/6sNBNl5\n"\
		"dFUkQ4QvUw/ggXNGw2OuIHyzJc+MFv4iJGtCJjC8aOIRP+q4739sERX2+bPtlDpt\n"\
		"1QZbKsIxG73Enn2s30Wl7kSkBPRXLyI7BfJoRsPx95zCmMXdFNgLjDX+z86h8RcL\n"\
		"yPUvkcOjOVRxUw7HSL1/mfBDm7EVb5KHDfHFNe/MV0HGgBrXEQmyEbDpHo+FgXZ7\n"\
		"lWxTcAs1gAhyjVZL5+TSiGQkDq+lqYztKP6DCarUUmO7Xt2GBkpnWPaB4AB1ELA5\n"\
		"h0B8GIoqIxsDHwKCAQEAwYUmyjVRQl4P2RFi9k3wtzS1cfklGe3q7A02nPNFCFIH\n"\
		"P2DURHiStYZwMjcUrOnD5Y+ymFvbg81gElpEk8+p2bLcRVoWbyLFg5c69buld8Hk\n"\
		"7EA6dbXetUSvtovb8AcmsTRtvvxMiwPjcj54m2mFLoQ0ax/0EHFSD6Q6IatDdkEH\n"\
		"suMzOanJ7L+rekC7icjXhxt4XZqmGqb1yhZZXyxUtsQ17EG4BfNKf9V1QLdcxwUg\n"\
		"QHUvoQMyhmG/2VZmcGG5CndYQHrEUwcC/V1IGv0bHgAiivgyEGxyK5Xw8Eab6SEp\n"\
		"+siuoJnArQvXzj0fKb+eZcFrUXIXWLCKACak7rbanQKCAQBd0QARPlg9G41JxVvf\n"\
		"lj/Mxhjc4HbWuZtEnNPgmRKqE7eTn6WMilCYY3RrFgOB7deGsmKcy/Sh2nQIlFk2\n"\
		"mk2RPoyGmLH/V3MUPDZN0mkMFHcslvkcRUoAwnWMW+6E7TBI7MuSW/vh6Nrj2cX1\n"\
		"D0KhlUQbp25NU5ErmNzIsCWbWGNDhRkfoICvrwpBDzHYSEPbP5oWZSqi+NWxiilu\n"\
		"sDR/7Ja0/5LPLCW5ZA18au+N/+kX7DKEnksqJMo2ibdBas4yqCaRWcwjVJFfaA5L\n"\
		"s36jDMT+K0PR+meNDN+THiBWQmRaBJ81BEG8Xr7QfnSo00OdM7Blcq7FvtlXWaG2\n"\
		"ENRzAoIBAFq8G5vjs93yTGvygvwJPzwnMovlzUeDDgrvhxO/3DYyf8NvmxcfkWob\n"\
		"Rq/Es6T/ViJkZdCaASqjONBiEpVbYL/55N1/h9VD8GfqmHJvMP4do0BCPSbDx1Am\n"\
		"jFyC5vR75eBgPLS3hwwHHrWKJPJ5X0CYLBuuN7kd6OP8lGUE7/SaOQnfDT2UJmEi\n"\
		"3TvM7iGEUcm/T4hT4l3MuqGuKOk+19l51e6YQbhIgj29z+QwFQgblyuhxlswLJhl\n"\
		"bJZytEUtDJ2jp3VUGojzWKuC+TPuSLCgT5HpHVAmCMH246lE8gU0z4IOZhJXBMge\n"\
		"LWk9r0bb2WDMd40QeXHUFGBNvjdO9OECggEAa4Z3MUyeAXuofYa9nXbytgPH3SjF\n"\
		"7F8VQPJxmqyxy+2avn9NB7Xf5qlGpsQr9LB6QXGKmjnKjCyS8zwj9+CtQguPdfp1\n"\
		"JrCyNlBoATBu6ag71FpMOG2WB4RbSKUUHPC69YroQUg/qBIONDnEpRrTl+cfjoxX\n"\
		"3dm808istjB6BiutpuIn7GrAu/nb6a9uePDE+aUNZeyENatImX6yhKIQOA6TGLeA\n"\
		"n+hniDBvQtvC19rGWhp+zManhFKbmN4otiYWL6/d2vl5DVv26THcj91JQspiRDr7\n"\
		"JXG6Mf5DNcX/B2zdumjKGLQj7QwYY32hwvGXi5vNaB6RD3qbiwwhNU523A==\n"\
		"-----END RSA PRIVATE KEY-----\n";

char s_pk[] = "-----BEGIN PUBLIC KEY-----\n"\
		"MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA3VeN+j6YxE1SUsMbYpPi\n"\
		"S3k5WotlsFDbJKmRGWIZEhk6sUllxOQF1NTzthQd4JFndRNQZImePBM0a/SWc7gC\n"\
		"UCGjEYUCIs7eQfLDNw1oBw9X88yVOwqdaPHvRXjzIxDA6Pl98CMPlQAPtd9VNLt0\n"\
		"e5YqkU1w2zD4XL41e23XUWlxkq/gvFiglZHQrJuaIiCl/fN5JlDyIeaHFglMZp3f\n"\
		"+GVUJeFyTOn/fDHPtQcEPCs9pr4/wFWPN084V+9/Pz6+tEbpG9nfDGwi2jWTe6Is\n"\
		"N7qRiPhLmqRd1SPK5blpara4VIY3XNNQhecfapYZmJhCkg/dWugV6L4iZlFB9N0n\n"\
		"TmeK5zvD+Bd1h/WZToDyMeCs/krfUnKP37guPqO1GEgARj75vErtgL7N5mzE+m2z\n"\
		"AkCLneLwg4W1RtPTH0Hqe5ZhtEY8ShWtG0y/52CVeVCCFVGss6wI2YdprEJiaCfJ\n"\
		"LxnppB0f1FRKV5lFBmFG8LmhM/yOZJwdFM3nSIGbNYDfTFTHMQU+b1rVLgR8EX53\n"\
		"4YjTtkdbQmH/qKcwKbgHzQJf9CcYD6z7ZtFglaG2FZWrMM0kERhkqe/8jj7SMDRk\n"\
		"MFe/V84n2dCfwI1GwekyLjD00a2HWmEf8TiwLszX8MMVPkdnBj7vjmNI2sw4ldnH\n"\
		"3sf7XgUYM/8jv1yTVw81UQcCAwEAAQ==\n"\
		"-----END PUBLIC KEY-----\n";

char s_sk[] = "-----BEGIN RSA PRIVATE KEY-----\n"\
		"MIIJKAIBAAKCAgEA3VeN+j6YxE1SUsMbYpPiS3k5WotlsFDbJKmRGWIZEhk6sUll\n"\
		"xOQF1NTzthQd4JFndRNQZImePBM0a/SWc7gCUCGjEYUCIs7eQfLDNw1oBw9X88yV\n"\
		"OwqdaPHvRXjzIxDA6Pl98CMPlQAPtd9VNLt0e5YqkU1w2zD4XL41e23XUWlxkq/g\n"\
		"vFiglZHQrJuaIiCl/fN5JlDyIeaHFglMZp3f+GVUJeFyTOn/fDHPtQcEPCs9pr4/\n"\
		"wFWPN084V+9/Pz6+tEbpG9nfDGwi2jWTe6IsN7qRiPhLmqRd1SPK5blpara4VIY3\n"\
		"XNNQhecfapYZmJhCkg/dWugV6L4iZlFB9N0nTmeK5zvD+Bd1h/WZToDyMeCs/krf\n"\
		"UnKP37guPqO1GEgARj75vErtgL7N5mzE+m2zAkCLneLwg4W1RtPTH0Hqe5ZhtEY8\n"\
		"ShWtG0y/52CVeVCCFVGss6wI2YdprEJiaCfJLxnppB0f1FRKV5lFBmFG8LmhM/yO\n"\
		"ZJwdFM3nSIGbNYDfTFTHMQU+b1rVLgR8EX534YjTtkdbQmH/qKcwKbgHzQJf9CcY\n"\
		"D6z7ZtFglaG2FZWrMM0kERhkqe/8jj7SMDRkMFe/V84n2dCfwI1GwekyLjD00a2H\n"\
		"WmEf8TiwLszX8MMVPkdnBj7vjmNI2sw4ldnH3sf7XgUYM/8jv1yTVw81UQcCAwEA\n"\
		"AQKCAgB6u7XqcRNcplNjuPRScRYmK431+x3j6rHZXn0qyg6EzqCQ9dYMmzPwlDSf\n"\
		"XgBDKd1oOdF2LikjvrJuui1C9WGy9TPq3woUKwlrICXHPRPV9lgaw/JrzrMCIkU/\n"\
		"DJYld4DArrd6lLZrNKGBg9lHaDpq3RW1hG9z22+cXYxiaMHgTsu/Pu8rASnqRfSk\n"\
		"AZQyIFBqxLsE2BWrXpzBR16p+BjdL7K9Xol2xLn6L9Sw2Lcsdf21nADVsZu8W50u\n"\
		"hYrVkwFEDZ3s556a0MtynAqvn3lNXZKdlLC3izTMnxA+2dTua2tC0zmXq0UXOtVc\n"\
		"0OZYivU+8LUZV1/usp94hM5B7O+BrkMXuF8+2Y1UuXB46OesE8SOZLNV41hB/AD5\n"\
		"izq/P0NKuLtnfb8F8mzNuk7jtnvofWaSCIncYiXaldhubwZ4Pd8/AWpWtr4isrp8\n"\
		"MvliBAQBrHVxzgFYEHtWQuuGdP57kKgfqfD+lHDSbMPufbGO3rKTigaJwrZL9oDb\n"\
		"JAH6y/P3JZfIb6wg5qOOxNV6ySkrxRgMLL9mFr/7AFQ+/8js/9NRA5wQGhYe3MvZ\n"\
		"HhF+jUuRWETrbW4t57HfWibMErJqqW1Hw9S9iQNfdCGpHic9jukfRdhU+jQIGCJS\n"\
		"Jxx3m0RFXaqjvinKu3RG0aBZJZnXTvLNS3C5UODx/qwHHMhn4QKCAQEA7mMZfkZ6\n"\
		"RovBv3draSwAVR+4kTEGUwHFm7LiH7M+JpsppmHAQ41EOgb8BsPn5njnPrNVQ5zH\n"\
		"pBRF06B/+Ozm4uXoTUM7q+488/uDYZpBWcRqFgYYcnBDWX0D4pv0r3lIbZTJitmp\n"\
		"OleSoCliHTDDHCJRkXHLkPjhcwE3wKOSTqFs379aQCNWldAQ3pfWxsacKoKEIy+N\n"\
		"FX2tJaapolYK7d9K2ENSnx2iPC80KbIjW84zSXHAtny2oJQVJkRJfTNmFK3HCRbw\n"\
		"VXEcugK34xvZKHVPeIOYnf17b6RjCQ7qfm2TzIgaeAQQXuq5eWjE0tDT/nlPpptT\n"\
		"oiKx+l3OCDRxrQKCAQEA7bIPilNe0HYHpnFVWKL2tnaEach8Jf4oZVSFUQCfeiq9\n"\
		"jOmzNTuFsALn+ztxkGM5kkskedKO2jPHk1LbMjAcGHM5cM5H3GzjPmJIATI+b1un\n"\
		"HZ5yZgLVHexigRHDEe8FI9Z//eHn/6mDiCcWWThTipshrKsDNepqjP50zxOnuCdd\n"\
		"YARjC6HXZ5PTvaFHxhBPU91C5tuTx5qut8khtyDjTAoxnS/0J+Mr5GIPM+sPdJmg\n"\
		"IfVLxdfdg+CwPZ8v4lecOD+g77JAXum2ZfRJJP8PFFee9MyQWQlAo8410WB98YBo\n"\
		"ue8Op/gjVJktCEIA8d3YJShACBtm/O2lNgoZk49sAwKCAQBvFIpqhtivSqwrVbAd\n"\
		"P72f7LoKHbf+sdKsrHwlnVYWkzKjLuVyFL938o05ccv7XtcSJmpSqRl14WRwKle3\n"\
		"XFxx2gOXDP8fLFIGtmoP7tsIwzdTr99wY9NePZyz2Uv5ACC5vxzysF3kq63NJ7tq\n"\
		"MrCXyX4MsePTV8Vl22lpQE8KiLm8wOODA3RzNpLFxib0VNOsE0kTRDMPkpI9+x0M\n"\
		"Yd+R2/x6LVSwhBR2yuAZlcJYIwtO68yKvC41QQth22wLBQpKFHesSp4Okjh6jI23\n"\
		"K+DSs2cmlttksxrzeLwVGng2FUVLhxgeWHZwZBus8R7VxS0jZM+yqKod4ODh8xfw\n"\
		"Rs+5AoIBACggv27i1EbtJi17RKVKHcyABaq2Jf9kAP3aXwdly4acYJPBgHekLNWi\n"\
		"J/fJ+Uoe9kE0XlwSp/s+cpr0ifzXEmcuTKw2pvsXhuWpQe6xxMiX+IaPkVCXedMr\n"\
		"bRXNIdmNOJjsRX9e/AO1YndB4gv97lygA/dah5g6kvJqot2yu8XjH2huVSpJkEv0\n"\
		"MPL6n1tYtYu69uhyeWhhPM3aLs7zxmu37NhUmCuDto0/4MErFdjhdp9FAh3Ma6Ev\n"\
		"9ZDxpuvXTpBQbaMJvMRJQHnF2/Na+/i7MiKxxkzBux/sWDmYyKpUu1loeljxE0Xc\n"\
		"9jVGr1Il2a+sM+MqfSCGHjYZTmgS9nECggEBAJLaskS14CtbRYXjzKUjnpPHStKN\n"\
		"yH82eZ60ZiPUxp4Safn1JagVh9CW0D3Cfj0WWkq+yxe/4jw1VL2D9xBL3178YTH1\n"\
		"oRCBdh4MhBn+PZDBBxOF6Bzjv9neqFVntL+94ncBd5L5dMg80C09imdwrUc2nW8U\n"\
		"8fZjIKXRnKsg9HPl74sY+aXCs/Nc+dtKmMb7Cc2dsGuPzBCtXkcUmwKc/kCayTq+\n"\
		"NJ4XfY0NrapRbmpvF0IRoStQ9MpvlaKraRVTK7arRu3qnCJZwP8RPc0388inxGAi\n"\
		"pOABpTTKAt2Hbv0gGODB7/b2q7WcllvnJ1ZYQi/QQ81w4rNHqmh56bWiMD8=\n"\
		"-----END RSA PRIVATE KEY-----\n";

RSA *get_key(char *str_key, int type)
{
	RSA *key = NULL;

	BIO *key_bio = BIO_new_mem_buf(str_key, -1);

	if(type)
	{
		key = PEM_read_bio_RSA_PUBKEY(key_bio, &key, NULL, NULL);
	} else {
		key = PEM_read_bio_RSAPrivateKey(key_bio, &key, NULL, NULL);
	}

	BIO_free(key_bio);
	return key;
}


void printf(const char *fmt, ...)
{
	char buf[BUFSIZ] = {'\0'};
	va_list ap;
	va_start(ap, fmt);
	vsnprintf(buf, BUFSIZ, fmt, ap);
	va_end(ap);
	ocall_print(buf);
}

uint8_t *aes_key = (uint8_t *)"01234567890123456789012345678901";

 /* A 128 bit IV */
uint8_t *iv = (uint8_t *)"0123456789012345";
size_t iv_ln = 16;


void printBytes(uint8_t *digest, size_t ln) {

	for(int i = 0; i < ln; i++) {
		printf("%02x", digest[i]);
	}
	printf("\n");
}

void clash_msg_store_req_verification(char *ptr, size_t len)
{

	uint8_t md[SHA512_DIGEST_LENGTH] = {0};

	uint8_t *additional = (uint8_t*)"secret tag";

	uint8_t *unbase64_r = NULL;
	uint8_t *unbase64_ct_ptr = NULL;
	uint8_t *unbase64_store_req_ptr = NULL;
	uint8_t *unbase64_hmac_ptr = NULL;
	uint8_t *unbase64_tag_ptr = NULL;
	uint8_t not_hmac[SHA512_DIGEST_LENGTH] = {0};

	const char sep[2] = ":";

	int i = 0;
	int pt_ln = 0, ct_ln = 0;
	uint32_t hmac_ln = 0;
	int hmac_status = 0;

	char *next = NULL;
	char *tokens = strtok_r(ptr, sep, &next);

	/* Unpacking encoded data and decoding */
	if(tokens == NULL) {
		printf("NULL is returned \n");
	} else {
		do
			{
				if(i == 0)
				{
					int ln = strlen(tokens);
					unbase64_r = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
					i++;
					continue;
				}

				if(i == 1)
				{
					int ln = strlen(tokens);
					unbase64_ct_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
					i++;
					continue;
				}

				if(i == 2)
				{
					int ln = strlen(tokens);
					unbase64_store_req_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
					i++;
					continue;
				}

				if(i == 3)
				{
					int ln = strlen(tokens);
					unbase64_hmac_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
					i++;
					continue;
				}

				if(i == 4)
				{
					int ln = strlen(tokens);
					unbase64_tag_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
					continue;
				}
			}
			while((tokens = strtok_r(NULL, sep, &next)) != NULL);
	}

	ct_ln = strlen((char*)unbase64_ct_ptr);

	char pt[ct_ln] = {0};

	pt_ln = gcm_decrypt(unbase64_ct_ptr, ct_ln,
					additional, strlen((char*)additional),
					unbase64_tag_ptr,
					aes_key, iv, iv_ln,
					(uint8_t*)pt);

	pt[pt_ln] = '\0';

	clash_store_req_concat(md, unbase64_r, (uint8_t*)pt, unbase64_store_req_ptr);

	/* Generates HMAC */
	hmac_sign(md, aes_key, not_hmac, &hmac_ln);

	/* Verifying HMAC */
	hmac_status = hmac_verify(unbase64_hmac_ptr, not_hmac, hmac_ln);
//	printf("Enclave:\t [ecall_clash_msg_store_req_verification] %d\n\t\t [hmac_status verification]\n", hmac_status);

	if(hmac_status) {
		memcpy(ptr, "1", 1);
		goto erase;
	} else {
		memcpy(ptr, "0", 1);
		goto erase;
	}

	goto erase;

	erase:
		free(unbase64_r);
		free(unbase64_ct_ptr);
		free(unbase64_hmac_ptr);
		free(unbase64_store_req_ptr);
		free(unbase64_tag_ptr);
		free(tokens);

}


void clash_msg_store_ver_generation(char *ptr, size_t len)
{

	char *data = (char*)malloc(len * 1);

	if(data == NULL) {
		printf("Memory Allocation Failure\n");
	} else {
		memset(data, 0, len * 1);
	}

	string r = "rand";
	string auth = "authentication";
	string u_i =  "1.Antonis";
	string sep = ":";

	uint8_t hmac[256] = {0};
	uint32_t hmac_ln = 0;
	int hmac_status = 0;

	uint8_t md[SHA512_DIGEST_LENGTH] = {0};
	uint8_t tag[TAG_SIZE] = {0};

	int r_ln = 0, tag_ln = 0;
	int ct_ln = 0, pt_ln = 0;
	int l = 0;
	int ln = 0;

	int b64_r_ln = 0,
		b64_ct_ln = 0,
		b64_hmac_ln = 0,
		b64_tag_ln = 0;

	/* Additional Data */
	uint8_t *additional = (uint8_t*)"secret tag";

	/* Generate hash*/
	clash_msg_ver_concat(md, (uint8_t*)r.c_str(), (uint8_t*)u_i.c_str(), (uint8_t*)auth.c_str());

	/* Generates HMAC */
	hmac_status = hmac_sign(md, aes_key, hmac, &hmac_ln);

//	printf("Enclave:\t [ecall_clash_msg_store_ver_generation] %d\n\t\t [hmac_status generation]\n", hmac_status);

	string concat = auth + sep + u_i + sep;

	pt_ln = concat.length() + 1;
	char pt[pt_ln] = {0};

	l += snprintf(pt+l, pt_ln-l, auth.c_str());
	l += snprintf(pt+l, pt_ln-l, sep.c_str());
	l += snprintf(pt+l, pt_ln-l, u_i.c_str());
	l += snprintf(pt+l, pt_ln-l, sep.c_str());

	ct_ln = pt_ln;

	uint8_t ct[ct_ln] = {0};

	/* Encrypt the data */
	ct_ln = gcm_encrypt((uint8_t*)pt, pt_ln,
			additional, strlen((char*)additional),
			aes_key,
			iv, iv_ln,
			ct, tag);

	r_ln = strlen(r.c_str());
	tag_ln = strlen((char*)tag);

	b64_r_ln = get_base64_length(r_ln);
	b64_ct_ln = get_base64_length(ct_ln);
	b64_hmac_ln = get_base64_length(hmac_ln);
	b64_tag_ln = get_base64_length(tag_ln);

	uint8_t base64_r[b64_r_ln] = {0};
	uint8_t base64_ct[b64_ct_ln] = {0};
	uint8_t base64_hmac[b64_hmac_ln] = {0};
	uint8_t base64_tag[b64_tag_ln] = {0};

	/* Encode with Base64 */
	EVP_EncodeBlock(base64_r, (uint8_t*)r.c_str(), r_ln);
	EVP_EncodeBlock(base64_ct, ct, ct_ln);
	EVP_EncodeBlock(base64_hmac, hmac, hmac_ln);
	EVP_EncodeBlock(base64_tag, tag, tag_ln);

	/* Concatenate encoded data prior sending it */
	ln += snprintf(data+ln, len-ln, (char*)base64_r);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_ct);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_hmac);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_tag);
	ln += snprintf(data+ln, len-ln, sep.c_str());

	memcpy(ptr, data, strlen(data));
	free(data);
}

void clash_msg_search_verification(char *ptr, size_t len)
{

	uint8_t pt[256] = {0};
	uint8_t md[SHA512_DIGEST_LENGTH] = {0};

	uint8_t not_hmac[256] = {0};
	uint8_t not_hmac_msg_key[256] = {0};

	uint32_t not_hmac_ln = 0;
	uint32_t not_hmac_msg_key_ln = 0;

	int hmac_status = 0;
	int i = 0;
	int j = 0;
	int pt_ln = 0, ct_ln = 0;

	char *u_j = NULL;
	char *t = NULL;
	char *idx = NULL;
	char *s_j = NULL;

	const char delim[2] = ":";

	char *next = NULL;
	char *tokens = NULL;
	char *tokens_pt = NULL;

	tokens = strtok_r(ptr, delim, &next);

	uint8_t *unbase64_token = NULL;
	uint8_t *unbase64_ct_ptr = NULL;
	uint8_t *unbase64_ct_abe_ptr = NULL;
	uint8_t *unbase64_hmac_ptr = NULL;
	uint8_t *unbase64_hmac_msg_search_ptr = NULL;

	do
	{
		if(i == 0)
		{
			int ln = strlen(tokens);
			unbase64_token = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 1)
		{
			int ln = strlen(tokens);
			unbase64_ct_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 2)
		{
			int ln = strlen(tokens);
			unbase64_ct_abe_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 3)
		{
			int ln = strlen(tokens);
			unbase64_hmac_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 4)
		{
			int ln = strlen(tokens);
			unbase64_hmac_msg_search_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}
	}
	while((tokens = strtok_r(NULL, delim, &next)) != NULL);

	clash_msg_search_concat(md, unbase64_token, unbase64_ct_ptr, unbase64_ct_abe_ptr, unbase64_hmac_ptr);

	hmac_sign(md, aes_key, not_hmac, &not_hmac_ln);

	hmac_status = hmac_verify(unbase64_hmac_msg_search_ptr, not_hmac, not_hmac_ln);

//	printf("Enclave:\t [ecall_clash_msg_search_verification] %d\n\t\t [hmac_status verification]\n", hmac_status);

	if(hmac_status) {
		ct_ln = strlen((char*)unbase64_ct_ptr);

		pt_ln = aes_decrypt(unbase64_ct_ptr, ct_ln, aes_key, iv, pt);

		pt[pt_ln] = '\0';

		char *next_pt = NULL;
		tokens_pt = strtok_r((char*)pt, delim, &next_pt);

		do
		{
			if(j == 0)
			{
				u_j = tokens_pt;
				j++;
				continue;
			}

			if(j == 1)
			{
				t = tokens_pt;
				j++;
				continue;
			}

			if(j == 2)
			{
				idx = tokens_pt;
				j++;
				continue;
			}

			if(j == 3)
			{
				s_j = tokens_pt;
				j++;
				continue;
			}
		}
		while((tokens_pt = strtok_r(NULL, delim, &next_pt)) != NULL);

		clash_msg_key_generation_concat(md, (uint8_t*)u_j, (uint8_t*)t, (uint8_t*)idx, (uint8_t*)s_j, unbase64_ct_abe_ptr);

		hmac_sign(md, aes_key, not_hmac_msg_key, &not_hmac_msg_key_ln);

		hmac_status = hmac_verify(unbase64_hmac_ptr, not_hmac_msg_key, not_hmac_msg_key_ln);

//		printf("Enclave:\t [clash_msg_search_verification] %d\n\t\t [hmac_status verification]\n", hmac_status);

		if(hmac_status) {
			memcpy(ptr, "1", 1);
			goto erase;
		} else {
			memcpy(ptr, "0", 1);
			goto erase;
		}
	}

	erase:
		free(unbase64_token);
		free(unbase64_ct_ptr);
		free(unbase64_ct_abe_ptr);
		free(unbase64_hmac_ptr);
		free(unbase64_hmac_msg_search_ptr);
		free(tokens);
		free(tokens_pt);

}


void clash_msg_update_verification(char *ptr, size_t len)
{

	uint8_t pt[256] = {0};
	uint8_t md[SHA512_DIGEST_LENGTH] = {0};

	uint8_t not_hmac[256] = {0};
	uint8_t not_hmac_msg_key[256] = {0};

	uint32_t not_hmac_ln = 0;
	uint32_t not_hmac_msg_key_ln = 0;

	int hmac_status = 0;
	int i = 0;
	int j = 0;
	int pt_ln = 0, ct_ln = 0;

	char *u_j = NULL;
	char *t = NULL;
	char *idx = NULL;
	char *s_j = NULL;

	const char delim[2] = ":";

	char *next = NULL;
	char *tokens = NULL;
	char *tokens_pt = NULL;

	tokens = strtok_r(ptr, delim, &next);

	uint8_t *unbase64_token = NULL;
	uint8_t *unbase64_ct_ptr = NULL;
	uint8_t *unbase64_ct_abe_ptr = NULL;
	uint8_t *unbase64_hmac_ptr = NULL;
	uint8_t *unbase64_hmac_msg_search_ptr = NULL;

	do
	{
		if(i == 0)
		{
			int ln = strlen(tokens);
			unbase64_token = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 1)
		{
			int ln = strlen(tokens);
			unbase64_ct_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 2)
		{
			int ln = strlen(tokens);
			unbase64_ct_abe_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 3)
		{
			int ln = strlen(tokens);
			unbase64_hmac_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 4)
		{
			int ln = strlen(tokens);
			unbase64_hmac_msg_search_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}
	}
	while((tokens = strtok_r(NULL, delim, &next)) != NULL);

	clash_msg_search_concat(md, unbase64_token, unbase64_ct_ptr, unbase64_ct_abe_ptr, unbase64_hmac_ptr);

	hmac_sign(md, aes_key, not_hmac, &not_hmac_ln);

	hmac_status = hmac_verify(unbase64_hmac_msg_search_ptr, not_hmac, not_hmac_ln);

//	printf("Enclave:\t [ecall_clash_msg_update_verification] %d\n\t\t [hmac_status verification]\n", hmac_status);

	if(hmac_status) {
		ct_ln = strlen((char*)unbase64_ct_ptr);

		pt_ln = aes_decrypt(unbase64_ct_ptr, ct_ln, aes_key, iv, pt);

		pt[pt_ln] = '\0';

		char *next_pt = NULL;
		tokens_pt = strtok_r((char*)pt, delim, &next_pt);

		do
		{
			if(j == 0)
			{
				u_j = tokens_pt;
				j++;
				continue;
			}

			if(j == 1)
			{
				t = tokens_pt;
				j++;
				continue;
			}

			if(j == 2)
			{
				idx = tokens_pt;
				j++;
				continue;
			}

			if(j == 3)
			{
				s_j = tokens_pt;
				j++;
				continue;
			}
		}
		while((tokens_pt = strtok_r(NULL, delim, &next_pt)) != NULL);

		clash_msg_key_generation_concat(md, (uint8_t*)u_j, (uint8_t*)t, (uint8_t*)idx, (uint8_t*)s_j, unbase64_ct_abe_ptr);

		hmac_sign(md, aes_key, not_hmac_msg_key, &not_hmac_msg_key_ln);

		hmac_status = hmac_verify(unbase64_hmac_ptr, not_hmac_msg_key, not_hmac_msg_key_ln);

//		printf("Enclave:\t [clash_msg_update_verification] %d\n\t\t [hmac_status verification]\n", hmac_status);

		if(hmac_status) {
			memcpy(ptr, "1", 1);
			goto erase;
		} else {
			memcpy(ptr, "0", 1);
			goto erase;
		}
	}

	erase:
		free(unbase64_token);
		free(unbase64_ct_ptr);
		free(unbase64_ct_abe_ptr);
		free(unbase64_hmac_ptr);
		free(unbase64_hmac_msg_search_ptr);
		free(tokens);
		free(tokens_pt);

}


void clash_msg_delete_verification(char *ptr, size_t len)
{

	uint8_t pt[256] = {0};
	uint8_t md[SHA512_DIGEST_LENGTH] = {0};

	uint8_t not_hmac[256] = {0};
	uint8_t not_hmac_msg_key[256] = {0};

	uint32_t not_hmac_ln = 0;
	uint32_t not_hmac_msg_key_ln = 0;

	int hmac_status = 0;
	int i = 0;
	int j = 0;
	int pt_ln = 0;
	int ct_ln = 0;

	char *u_j = NULL;
	char *t = NULL;
	char *idx = NULL;
	char *s_j = NULL;

	const char delim[2] = ":";

	char *next = NULL;
	char *tokens = NULL;
	char *tokens_pt = NULL;

	tokens = strtok_r(ptr, delim, &next);

	uint8_t *unbase64_token = NULL;
	uint8_t *unbase64_ct_ptr = NULL;
	uint8_t *unbase64_ct_abe_ptr = NULL;
	uint8_t *unbase64_hmac_ptr = NULL;
	uint8_t *unbase64_hmac_msg_search_ptr = NULL;

	do
	{
		if(i == 0)
		{
			int ln = strlen(tokens);
			unbase64_token = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 1)
		{
			int ln = strlen(tokens);
			unbase64_ct_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 2)
		{
			int ln = strlen(tokens);
			unbase64_ct_abe_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 3)
		{
			int ln = strlen(tokens);
			unbase64_hmac_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 4)
		{
			int ln = strlen(tokens);
			unbase64_hmac_msg_search_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}
	}
	while((tokens = strtok_r(NULL, delim, &next)) != NULL);

	clash_msg_search_concat(md, unbase64_token, unbase64_ct_ptr, unbase64_ct_abe_ptr, unbase64_hmac_ptr);

	hmac_sign(md, aes_key, not_hmac, &not_hmac_ln);

	hmac_status = hmac_verify(unbase64_hmac_msg_search_ptr, not_hmac, not_hmac_ln);

//	printf("Enclave:\t [ecall_clash_msg_delete_verification] %d\n\t\t [hmac_status verification]\n", hmac_status);

	if(hmac_status) {
		ct_ln = strlen((char*)unbase64_ct_ptr);

		pt_ln = aes_decrypt(unbase64_ct_ptr, ct_ln, aes_key, iv, pt);

		pt[pt_ln] = '\0';

		char *next_pt = NULL;
		tokens_pt = strtok_r((char*)pt, delim, &next_pt);

		do
		{
			if(j == 0)
			{
				u_j = tokens_pt;
				j++;
				continue;
			}

			if(j == 1)
			{
				t = tokens_pt;
				j++;
				continue;
			}

			if(j == 2)
			{
				idx = tokens_pt;
				j++;
				continue;
			}

			if(j == 3)
			{
				s_j = tokens_pt;
				j++;
				continue;
			}
		}
		while((tokens_pt = strtok_r(NULL, delim, &next_pt)) != NULL);

		clash_msg_key_generation_concat(md, (uint8_t*)u_j, (uint8_t*)t, (uint8_t*)idx, (uint8_t*)s_j, unbase64_ct_abe_ptr);

		hmac_sign(md, aes_key, not_hmac_msg_key, &not_hmac_msg_key_ln);

		hmac_status = hmac_verify(unbase64_hmac_ptr, not_hmac_msg_key, not_hmac_msg_key_ln);

//		printf("Enclave:\t [clash_msg_delete_verification] %d\n\t\t [hmac_status verification]\n", hmac_status);

		if(hmac_status) {
			memcpy(ptr, "1", 1);
			goto erase;
		} else {
			memcpy(ptr, "0", 1);
			goto erase;
		}
	}

	erase:
		free(unbase64_token);
		free(unbase64_ct_ptr);
		free(unbase64_ct_abe_ptr);
		free(unbase64_hmac_ptr);
		free(unbase64_hmac_msg_search_ptr);
		free(tokens);
		free(tokens_pt);

}
