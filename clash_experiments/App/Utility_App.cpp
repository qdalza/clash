#include "Utility_App.h"

#include <string.h>
#include <stdio.h>

#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <openssl/err.h>
#include <openssl/sha.h>


void waitForKeyPress()
{
	char ch;
	int temp;
	printf("\n\nHit a key....\n");
	temp = scanf_s("%c", &ch);
	(void) temp;
}

void handleErrors(void) {
	ERR_get_error();
	abort();
}

void printBytes(uint8_t *digest, size_t ln)
{
	for(int i = 0; i < ln; i++) {
		printf("%02x", digest[i]);
	}
	printf("\n");
}

int get_base64_length(int ct_ln)
{
	int b64_length = 0;

	b64_length = ct_ln / 3 * 4;

	return b64_length + 65;
}

uint8_t *base64_decode(uint8_t *base64, uint32_t *len)
{
	uint8_t *ret = NULL;
	uint32_t bin_len;

	bin_len = (((strlen((char*)base64) + 3)/4) * 3);
	ret = (uint8_t*)malloc(bin_len);
	*len = EVP_DecodeBlock(ret, base64, strlen((char*)base64));
	return ret;

}

void get_cpabe_ct(char *abe_ct, int len)
{
	FILE *fd = fopen("./App/abe.key", "r");

	long int file_size = 0;

	fseek(fd, 0L, SEEK_END);

	file_size = ftell(fd);

	fseek(fd, 0, SEEK_SET);

	if(file_size > len){
		exit(EXIT_FAILURE);
	}

	char *data = (char*)malloc(file_size * 1);

	if(data == NULL) exit(EXIT_FAILURE);
	memset(data, 0, file_size * 1);

	if(fd != 0) {
		fread(data, 1, file_size, fd);
	}

	fclose(fd);

	memcpy(abe_ct, data, strlen(data));
	free(data);
}


int hmac_sign(uint8_t *data, uint8_t *key, uint8_t *hmac, uint32_t *hmac_ln) {

	HMAC_CTX *ctx;

	if(!(ctx = HMAC_CTX_new()))
		handleErrors();

	if(1 != HMAC_Init_ex(ctx, key, strlen((char*)key), EVP_sha256(), NULL))
		handleErrors();

	if(1 != HMAC_Update(ctx, data, strlen((char*)data)))
		handleErrors();

	if(1 != HMAC_Final(ctx, hmac, hmac_ln)) {
		handleErrors();
		return 0;
	}

	HMAC_CTX_free(ctx);

	return 1;
}


int hmac_verify(uint8_t *hmac, uint8_t *not_hmac, uint32_t hmac_ln) {

	for (int i=0; i!=hmac_ln; i++) {
		if (not_hmac[i] != hmac[i]) {
			 return 0;
		}
	 }
	return 1;
}


int gcm_encrypt(uint8_t *plaintext, int plaintext_len,
				uint8_t *aad, int aad_len,
				uint8_t *key,
				uint8_t *iv, int iv_len,
				uint8_t *ciphertext,
				uint8_t *tag)
{
	EVP_CIPHER_CTX *ctx;

	int len;
	int ciphertext_len;

	if(!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL))
		handleErrors();

	if(1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, iv_len, NULL))
		handleErrors();

	if(1 != EVP_EncryptInit_ex(ctx, NULL, NULL, key, iv))
		handleErrors();

	if(1 != EVP_EncryptUpdate(ctx, NULL, &len, aad, aad_len))
		handleErrors();

	if(1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
		handleErrors();
	ciphertext_len = len;

	if(1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))
		handleErrors();
	ciphertext_len += len;

	if(1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_GET_TAG, 16, tag))
		handleErrors();

	EVP_CIPHER_CTX_free(ctx);

	return ciphertext_len;
}

int gcm_decrypt(uint8_t *ciphertext, int ciphertext_len,
				uint8_t *aad, int aad_len,
				uint8_t *tag,
				uint8_t *key,
				uint8_t *iv, int iv_len,
				uint8_t *plaintext)
{
	EVP_CIPHER_CTX *ctx;
	int len;
	int plaintext_len;
	int ret;

	if(!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	if(!EVP_DecryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL))
		handleErrors();

	if(!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, iv_len, NULL))
		handleErrors();

	if(!EVP_DecryptInit_ex(ctx, NULL, NULL, key, iv))
		handleErrors();

	if(!EVP_DecryptUpdate(ctx, NULL, &len, aad, aad_len))
		handleErrors();

	if(!EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
		handleErrors();
	plaintext_len = len;

	if(!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_TAG, 16, tag))
		handleErrors();

	ret = EVP_DecryptFinal_ex(ctx, plaintext + len, &len);

	EVP_CIPHER_CTX_free(ctx);

	if(ret > 0) {
		plaintext_len += len;
		return plaintext_len;
	} else {
		return -1;
	}
}

int aes_encrypt(uint8_t *pt, int pt_ln, uint8_t* key, uint8_t *iv, uint8_t *ct) {

	EVP_CIPHER_CTX *ctx;

	int ln, ct_ln;

	if(!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv))
		handleErrors();

	if(1 != EVP_EncryptUpdate(ctx, ct, &ln, pt, pt_ln))
		handleErrors();

	ct_ln = ln;

	if (1 != EVP_EncryptFinal_ex(ctx, ct + ln, &ln))
		handleErrors();

	ct_ln = ln;

	EVP_CIPHER_CTX_free(ctx);

	return ct_ln;
}

int aes_decrypt(uint8_t *ct, int ct_ln, uint8_t *key, uint8_t *iv, uint8_t *pt) {


	EVP_CIPHER_CTX *ctx;

	int ln, pt_ln;

	if(!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	EVP_CIPHER_CTX_set_padding(ctx, 0);

	if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv))
		handleErrors();

	if(1 != EVP_DecryptUpdate(ctx, pt, &ln, ct, ct_ln))
		handleErrors();

	pt_ln = ln;

	if (1 != EVP_DecryptFinal_ex(ctx, pt + ln, &ln))
		handleErrors();
	pt_ln += ln;

	EVP_CIPHER_CTX_free(ctx);

	return pt_ln;
}



void clash_msg_revoke_concat(uint8_t *md, uint8_t *r, uint8_t *u_j, uint8_t *u_l, uint8_t *n, uint8_t *abe_ct)
{
	SHA512_CTX c;

	SHA512_Init(&c);

	SHA512_Update(&c, r, strlen((const char*)r));
	SHA512_Update(&c, u_j, strlen((const char*)u_j));
	SHA512_Update(&c, u_l, strlen((const char*)u_l));
	SHA512_Update(&c, n, strlen((const char*)n));
	SHA512_Update(&c, abe_ct, strlen((const char*)abe_ct));

	SHA512_Final(md, &c);
}


void clash_msg_ver_req_concat(uint8_t *md, uint8_t *r, uint8_t *u_i, uint8_t *u_j)
{
	SHA512_CTX c;

	SHA512_Init(&c);

	SHA512_Update(&c, r, strlen((char*)r));
	SHA512_Update(&c, u_i, strlen((char*)u_i));
	SHA512_Update(&c, u_j, strlen((char*)u_j));

	SHA512_Final(md, &c);

}


void clash_msg_assign_concat(uint8_t *md, uint8_t *r, uint8_t *u_j, uint8_t *u_l, uint8_t *n, uint8_t *abe_ct, uint8_t *manage)
{
	SHA512_CTX c;

	SHA512_Init(&c);

	SHA512_Update(&c, r, strlen((char*)r));
	SHA512_Update(&c, u_j, strlen((char*)u_j));
	SHA512_Update(&c, u_l, strlen((char*)u_l));
	SHA512_Update(&c, n, strlen((char*)n));
	SHA512_Update(&c, abe_ct, strlen((char*)abe_ct));
	SHA512_Update(&c, manage, strlen((char*)manage));

	SHA512_Final(md, &c);
}


void clash_kt_store_concat(uint8_t *md, uint8_t *r, uint8_t *u_i, uint8_t *idx, uint8_t *abe_ct)
{
	SHA512_CTX c;

	SHA512_Init(&c);

	SHA512_Update(&c, r, strlen((char*)r));
	SHA512_Update(&c, u_i, strlen((char*)u_i));
	SHA512_Update(&c, abe_ct, strlen((char*)abe_ct));
	SHA512_Update(&c, idx, strlen((char*)idx));


	SHA512_Final(md, &c);
}

void clash_msg_search_concat(uint8_t *md, uint8_t *token, uint8_t *ct, uint8_t *ct_abe, uint8_t *hmac)
{
	SHA512_CTX c;
	SHA512_Init(&c);

	SHA512_Update(&c, token, strlen((char*)token));
	SHA512_Update(&c, ct, strlen((char*)ct));
	SHA512_Update(&c, ct_abe, strlen((char*)ct_abe));
	SHA512_Update(&c, hmac, strlen((char*)hmac));

	SHA512_Final((unsigned char*)md, &c);

}


void clash_msg_update_concat(uint8_t *md, uint8_t *token, uint8_t *ct, uint8_t *ct_abe, uint8_t *hmac)
{
	SHA512_CTX c;
	SHA512_Init(&c);

	SHA512_Update(&c, token, strlen((char*)token));
	SHA512_Update(&c, ct, strlen((char*)ct));
	SHA512_Update(&c, ct_abe, strlen((char*)ct_abe));
	SHA512_Update(&c, hmac, strlen((char*)hmac));

	SHA512_Final((unsigned char*)md, &c);
}

void clash_msg_delete_concat(uint8_t *md, uint8_t *token, uint8_t *ct, uint8_t *ct_abe, uint8_t *hmac)
{
	SHA512_CTX c;
	SHA512_Init(&c);

	SHA512_Update(&c, token, strlen((char*)token));
	SHA512_Update(&c, ct, strlen((char*)ct));
	SHA512_Update(&c, ct_abe, strlen((char*)ct_abe));
	SHA512_Update(&c, hmac, strlen((char*)hmac));

	SHA512_Final((uint8_t*)md, &c);

}
//
void clash_msg_ver_concat(uint8_t *md, uint8_t *r, uint8_t *u_i, uint8_t *auth)
{
	SHA512_CTX c;
	SHA512_Init(&c);

	SHA512_Update(&c, r, strlen((char*)r));
	SHA512_Update(&c, auth, strlen((char*)auth));
	SHA512_Update(&c, u_i, strlen((char*)u_i));

	SHA512_Final((uint8_t*)md, &c);

}

void clash_store_req_concat(uint8_t *md, uint8_t *r, uint8_t *credentials, uint8_t *store_req)
{
	SHA512_CTX c;
	SHA512_Init(&c);

	SHA512_Update(&c, r, strlen((char*)r));
	SHA512_Update(&c, credentials, strlen((char*)credentials));
	SHA512_Update(&c, store_req, strlen((char*)store_req));

	SHA512_Final((uint8_t*)md, &c);

}

int clash_file_exist(const char *fname)
{
	FILE *fd;
	if((fd = fopen(fname, "r"))) {
		fclose(fd);
		return 1;
	}

	return 0;
}


void clash_get_local_lvs(char *path, char *data)
{

	FILE *fd = fopen(path, "r");
	fseek(fd, 0L, SEEK_END);

	long int pt_ln = ftell(fd);
	char *pt = (char*)malloc(pt_ln * 1);

	fseek(fd, 0L, SEEK_SET);

	while(!feof(fd)) {
		fread(pt, pt_ln, 1, fd);
	}

	fclose(fd);

	memcpy(data, pt, strlen(pt));
	free(pt);
}


int clash_local_store_lvs(char *path, char *ht_keys, char *keys, char *values)
{
	if(!clash_file_exist(path)) {
		FILE *fd = fopen(path, "w");

		fwrite(ht_keys, 1, strlen(ht_keys), fd);
		fwrite(keys, 1, strlen(keys), fd);
		fwrite(values, 1, strlen(values), fd);

		fclose(fd);

		return 0;
	} else {
		return 1;
	}
}

