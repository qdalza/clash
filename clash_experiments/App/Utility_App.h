#ifndef APP_UTILITY_APP_H_
#define APP_UTILITY_APP_H_
#define scanf_s scanf

#include "stdint.h"
#include <cstdlib>

#ifdef __cplusplus
extern "C" {
#endif

/* Additional Functions*/

int hmac_sign(uint8_t *data, uint8_t *key, uint8_t *hmac, uint32_t *hmac_ln);
int hmac_verify(uint8_t *hmac, uint8_t *not_hmac, uint32_t hmac_ln);

int gcm_encrypt(uint8_t *plaintext, int plaintext_len,
				uint8_t *aad, int aad_len,
				uint8_t *key,
				uint8_t *iv, int iv_len,
				uint8_t *ciphertext,
				uint8_t *tag);

int gcm_decrypt(uint8_t *ciphertext, int ciphertext_len,
				uint8_t *aad, int aad_len,
				uint8_t *tag,
				uint8_t *key,
				uint8_t *iv, int iv_len,
				uint8_t *plaintext);

int aes_encrypt(uint8_t *pt, int pt_ln, uint8_t* key, uint8_t *iv, uint8_t *ct);
int aes_decrypt(uint8_t *ct, int ct_ln, uint8_t *key, uint8_t *iv, uint8_t *pt);
int get_base64_length(int ct_ln);
uint8_t *base64_decode(uint8_t *base64, uint32_t *len);
void printBytes(uint8_t *digest, size_t ln);
void waitForKeyPress();
void handleErrors(void);

/* File Manipulation */

int clash_file_exist(const char *fname);
void clash_get_local_lvs(char *path, char *data);
int clash_local_store_lvs(char *path, char *ht_keys, char *keys, char *values);
void get_cpabe_ct(char *abe_ct, int len);

/* Message Digest */

void clash_msg_revoke_concat(uint8_t *md, uint8_t *r, uint8_t *u_j, uint8_t *u_l, uint8_t *n, uint8_t *abe_ct);
void clash_msg_ver_req_concat(uint8_t *md, uint8_t *r, uint8_t *u_i, uint8_t *u_j);
void clash_msg_assign_concat(uint8_t *md, uint8_t *r, uint8_t *u_j, uint8_t *u_l, uint8_t *n, uint8_t *abe_ct, uint8_t *manage);
void clash_kt_store_concat(uint8_t *md, uint8_t *r, uint8_t *u_i, uint8_t *idx, uint8_t *abe_ct);
void clash_msg_search_concat(uint8_t *md, uint8_t *token, uint8_t *ct, uint8_t *ct_abe, uint8_t *hmac);
void clash_msg_update_concat(uint8_t *md, uint8_t *token, uint8_t *ct, uint8_t *ct_abe, uint8_t *hmac);
void clash_msg_delete_concat(uint8_t *md, uint8_t *token, uint8_t *ct, uint8_t *ct_abe, uint8_t *hmac);
void clash_msg_ver_concat(uint8_t *md, uint8_t *r, uint8_t *u_i, uint8_t *auth);
void clash_store_req_concat(uint8_t *md, uint8_t *r, uint8_t *credentials, uint8_t *store_req);

#endif /* APP_UTILITY_APP_H_ */

#ifdef __cplusplus
 }
#endif
