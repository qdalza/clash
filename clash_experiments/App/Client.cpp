#define __STDC_WANT_LIB_EXT1__ 1

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <inttypes.h>
#include <fstream>
#include <cstdlib>

#include <map>
#include <vector>

#include <chrono>

#include "sgx_eid.h"
#include "sgx_urts.h"
#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#include "Utility_App.h"

#include <openssl/rsa.h>
#include <openssl/bn.h>
#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/evp.h>
#include <openssl/sha.h>
#include <openssl/obj_mac.h>
#include <openssl/rand.h>
#include <openssl/err.h>
#include <openssl/hmac.h>

#define UNUSED(val) (void)(val)
#define TCHAR   char
#define _TCHAR  char
#define _T(str) str
#define scanf_s scanf
#define _tmain  main
#define RSA_KEY_SIZE 4096
#define PADDING RSA_PKCS1_OAEP_PADDING

#define ABE_SIZE 512
#define BUFFER_SIZE 3096
#define TAG_SIZE 16
#define B64_SIZE 800

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1

using namespace std;
using namespace std::chrono;

#include "Client.h"

// ****************************************************************************
// *              Begin of Section: ENCRYPTION KEY (Ks)                       *
// ****************************************************************************

/* A 256 bit key */
uint8_t *key = (uint8_t*)"01234567890123456789012345678901";

//
/* A 128 bit IV */
uint8_t *iv = (uint8_t*)"0123456789012345";

size_t iv_len = 16;

// ****************************************************************************
// *              End of Section: ENCRYPTION KEY (Ks)                         *
// ****************************************************************************


void Client::clash_msg_search_generation(char *ptr, size_t len, char *base64_msg_key, size_t msg_key_len)
{
	uint8_t token[] = "token";

	uint8_t md[SHA512_DIGEST_LENGTH] = {0};

	uint8_t hmac[256] = {0};
	uint8_t base64_token[100] = {0};
	uint8_t base64_hmac[32] = {0};

	uint32_t hmac_ln = 0;

	int hmac_status = 0;
	int ln = 0;

	int i = 0;

	char *msg_key = (char*)malloc(1 * msg_key_len);
	memcpy(msg_key, base64_msg_key, msg_key_len);

	const char delim[2] = ":";

	uint8_t *unbase64_ct_ptr = NULL;
	uint8_t *unbase64_ct_abe_ptr = NULL;
	uint8_t *unbase64_hmac_ptr = NULL;

	/* Unpacking concatenated data prior verification */
	char *next = NULL;
	char *tokens = strtok_r(base64_msg_key, delim, &next);

	do
	{
		if(i == 0)
		{
			ln = strlen(tokens);
			unbase64_ct_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 1)
		{
			ln = strlen(tokens);
			unbase64_ct_abe_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 2)
		{
			ln = strlen(tokens);
			unbase64_hmac_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}
	}
	while((tokens = strtok_r(NULL, delim, &next)) != NULL);

	clash_msg_search_concat(md, token, unbase64_ct_ptr, unbase64_ct_abe_ptr, unbase64_hmac_ptr);

	/* Generates HMAC */
	hmac_status  = hmac_sign(md, key, hmac, &hmac_ln);

//	printf("Application:\t [clash_msg_search_generation] %d\n\t\t [hmac_status generation]\n", hmac_status);

	char *data = (char*)malloc(len * msg_key_len * 1);

	/* Encode with Base64 */
	EVP_EncodeBlock(base64_token, token, 10);
	EVP_EncodeBlock(base64_hmac, hmac, hmac_ln);

	ln = 0;
	/* Concatenate encoded data prior sending it */
	ln += snprintf(data+ln, len-ln, (char*)base64_token);
	ln += snprintf(data+ln, len-ln, ":");
	ln += snprintf(data+ln, len-ln, (char*)msg_key);
	ln += snprintf(data+ln, len-ln, (char*)base64_hmac);
	ln += snprintf(data+ln, len-ln, ":");

	memcpy(ptr, data, strlen(data));

	free(data);
	free(msg_key);
	free(unbase64_ct_ptr);
	free(unbase64_ct_abe_ptr);
	free(unbase64_hmac_ptr);
	free(tokens);

}

void Client::clash_msg_update_generation(char *ptr, size_t len, char *base64_msg_key, size_t msg_key_len)
{
	uint8_t token[] = "token";

	uint8_t md[SHA512_DIGEST_LENGTH] = {0};

	uint8_t hmac[256] = {0};
	uint8_t base64_token[100] = {0};
	uint8_t base64_hmac[32] = {0};

	uint32_t hmac_ln = 0;

	int hmac_status = 0;
	int ln = 0;

	int i = 0;

	char *msg_key = (char*)malloc(1 * msg_key_len);
	memcpy(msg_key, base64_msg_key, msg_key_len);

	const char delim[2] = ":";

	uint8_t *unbase64_ct_ptr = NULL;
	uint8_t *unbase64_ct_abe_ptr = NULL;
	uint8_t *unbase64_hmac_ptr = NULL;

	/* Unpacking encoded data and decoding */
	char *next = NULL;
	char *tokens = strtok_r(base64_msg_key, delim, &next);

	do
	{
		if(i == 0)
		{
			ln = strlen(tokens);
			unbase64_ct_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 1)
		{
			ln = strlen(tokens);
			unbase64_ct_abe_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 2)
		{
			ln = strlen(tokens);
			unbase64_hmac_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}
	}
	while((tokens = strtok_r(NULL, delim, &next)) != NULL);

	/* Generate hash */
	clash_msg_update_concat(md, token, unbase64_ct_ptr, unbase64_ct_abe_ptr, unbase64_hmac_ptr);

	/* Generates HMAC */
	hmac_status  = hmac_sign(md, key, hmac, &hmac_ln);

//	printf("Application:\t [clash_msg_update_generation] %d\n\t\t [hmac_status generation]\n", hmac_status);

	char *data = (char*)malloc(len * msg_key_len * 1);

	/* Encode with Base64 */
	EVP_EncodeBlock(base64_token, token, 10);
	EVP_EncodeBlock(base64_hmac, hmac, hmac_ln);

	ln = 0;
	/* Concatenate encoded data prior sending it */
	ln += snprintf(data+ln, len-ln, (char*)base64_token);
	ln += snprintf(data+ln, len-ln, ":");
	ln += snprintf(data+ln, len-ln, (char*)msg_key);
	ln += snprintf(data+ln, len-ln, (char*)base64_hmac);
	ln += snprintf(data+ln, len-ln, ":");

	memcpy(ptr, data, strlen(data));
	free(data);
	free(msg_key);
	free(unbase64_ct_ptr);
	free(unbase64_ct_abe_ptr);
	free(unbase64_hmac_ptr);
	free(tokens);

}

void Client::clash_msg_delete_generation(char *ptr, size_t len, char *base64_msg_key, size_t msg_key_len)
{
	uint8_t token[] = "token";

	uint8_t md[SHA512_DIGEST_LENGTH] = {0};

	uint8_t hmac[256] = {0};
	uint8_t base64_token[100] = {0};
	uint8_t base64_hmac[32] = {0};

	uint32_t hmac_ln = 0;

	int hmac_status = 0;
	int ln = 0;

	int i = 0;

	char *msg_key = (char*)malloc(1 * msg_key_len);
	memcpy(msg_key, base64_msg_key, msg_key_len);

	const char delim[2] = ":";

	uint8_t *unbase64_ct_ptr = NULL;
	uint8_t *unbase64_ct_abe_ptr = NULL;
	uint8_t *unbase64_hmac_ptr = NULL;

	/* Unpacking encoded data and decoding */
	char *next = NULL;
	char *tokens = strtok_r(base64_msg_key, delim, &next);

	do
	{
		if(i == 0)
		{
			ln = strlen(tokens);
			unbase64_ct_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 1)
		{
			ln = strlen(tokens);
			unbase64_ct_abe_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 2)
		{
			ln = strlen(tokens);
			unbase64_hmac_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}
	}
	while((tokens = strtok_r(NULL, delim, &next)) != NULL);

	/* Generate hash */
	clash_msg_delete_concat(md, token, unbase64_ct_ptr, unbase64_ct_abe_ptr, unbase64_hmac_ptr);

	/* Generates HMAC */
	hmac_status  = hmac_sign(md, key, hmac, &hmac_ln);

//	printf("Application:\t [clash_msg_delete_generation] %d\n\t\t [hmac_status generation]\n", hmac_status);

	char *data = (char*)malloc(len * msg_key_len * 1);

	/* Encode with Base64 */
	EVP_EncodeBlock(base64_token, token, 10);
	EVP_EncodeBlock(base64_hmac, hmac, hmac_ln);

	ln = 0;
	/* Concatenate encoded data prior sending it */
	ln += snprintf(data+ln, len-ln, (char*)base64_token);
	ln += snprintf(data+ln, len-ln, ":");
	ln += snprintf(data+ln, len-ln, (char*)msg_key);
	ln += snprintf(data+ln, len-ln, (char*)base64_hmac);
	ln += snprintf(data+ln, len-ln, ":");

	memcpy(ptr, data, strlen(data));
	free(data);
	free(msg_key);
	free(unbase64_ct_ptr);
	free(unbase64_ct_abe_ptr);
	free(unbase64_hmac_ptr);
	free(tokens);

}

void Client::clash_msg_kt_tray_store_generation(char *ptr, size_t len)
{
	char *data = (char*)malloc(len * 1);

	if(data == NULL) {
		printf("Memory Allocation Failure\n");
		exit(EXIT_FAILURE);
	} else {
		memset(data, 0, len * 1);
	}

	string r = "rand";
	string u_i = "1.Antonis";
	string idx = "idx";
	string sep = ":";

	char abe_key[ABE_SIZE] = {0};

	uint8_t md[SHA512_DIGEST_LENGTH] = {0};
	uint8_t hmac[256] = {0};

	uint32_t hmac_ln = 0;
	int hmac_status = 0;

	int b64_r_ln = 0,
		b64_ct_ln = 0,
		b64_tag_ln = 0,
		b64_abe_ln = 0,
		b64_hmac_ln = 0;

	int r_ln = 0, tag_ln = 0;
	int ct_ln = 0, pt_ln = 0;
	int ln = 0;

	/* Additional data */
	uint8_t *additional =
		(uint8_t*)"secret tag";

	uint8_t tag[TAG_SIZE] = {0};

	/* Load ABE key from a file to a buffer */
	get_cpabe_ct(abe_key, ABE_SIZE);

	string concat = u_i + sep + idx + sep;

	char pt[concat.length()] = {0};

	for (int i = 0; i < sizeof(pt); i++) {
		pt[i] = concat[i];
	}

	pt_ln = strlen(pt);
	ct_ln = pt_ln;

	uint8_t ct[ct_ln] = {0};

	/* Concatenate data into a hash */
	clash_kt_store_concat(md, (uint8_t*)r.c_str(),
							(uint8_t*)u_i.c_str(),
							(uint8_t*)idx.c_str(),
							(uint8_t*)abe_key);

	/* Generates HMAC */
	hmac_status = hmac_sign(md, key, hmac, &hmac_ln);

	if(hmac_status != EXIT_FAILURE) exit(EXIT_FAILURE);

//	printf("\n\nApplication:\t [clash_msg_kt_store_generation] %d\n\t\t [hmac_status generation]\n", hmac_status);

	/* Encrypt the data */
	ct_ln = gcm_encrypt((uint8_t*)pt, pt_ln,
				additional, strlen((char*)additional),
				key,
				iv, iv_len,
				ct, tag);

	tag_ln = strlen((char*)tag);
	r_ln = strlen(r.c_str());

	b64_r_ln = get_base64_length(r_ln);
	b64_ct_ln = get_base64_length(ct_ln);
	b64_tag_ln = get_base64_length(tag_ln);
	b64_abe_ln = get_base64_length(ABE_SIZE);
	b64_hmac_ln = get_base64_length(hmac_ln);

	uint8_t base64_r[b64_r_ln] = {0};
	uint8_t base64_ct[b64_ct_ln] = {0};
	uint8_t base64_tag[b64_tag_ln] = {0};
	uint8_t base64_abe[b64_abe_ln] = {0};
	uint8_t base64_hmac[b64_hmac_ln] = {0};

	/* Encode with Base64 */
	EVP_EncodeBlock(base64_r, (uint8_t*)r.c_str(), r_ln);
	EVP_EncodeBlock(base64_ct, ct, ct_ln);
	EVP_EncodeBlock(base64_tag, tag, tag_ln);
	EVP_EncodeBlock(base64_hmac, hmac, hmac_ln);
	EVP_EncodeBlock(base64_abe, (uint8_t*)abe_key, ABE_SIZE);

	/* Concatenate encoded data prior sending it */
	ln += snprintf(data+ln, len-ln, (char*)base64_r);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_ct);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_tag);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_hmac);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_abe);
	ln += snprintf(data+ln, len-ln, sep.c_str());

	memcpy(ptr, data, strlen(data));
	free(data);

}

void Client::clash_msg_store_ver_verification(char *ptr, size_t len)
{

	uint8_t *additional =
			(uint8_t*)"secret tag";

	uint8_t md[SHA512_DIGEST_LENGTH] = {0};

	uint8_t *unbase64_r = NULL;
	uint8_t *unbase64_ct_ptr = NULL;
	uint8_t *unbase64_hmac_ptr = NULL;
	uint8_t *unbase64_tag_ptr = NULL;
	uint8_t not_hmac[256] = {0};

	uint32_t hmac_ln = 0;

	char *auth = NULL;
	char *u_i = NULL;

	const char sep[2] = ":";

	int i = 0, j = 0;
	int pt_ln = 0, ct_ln = 0;
	int hmac_status = 0;

	char *next = NULL;
	char *tokens = strtok_r(ptr, sep, &next);

	if(tokens == NULL) {
		printf("NULL is returned\n");
	}
	/* Unpacking encoded data and decoding */
	do
	{
		if(i == 0)
		{
			int ln = strlen(tokens);
			unbase64_r = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 1)
		{
			int ln = strlen(tokens);
			unbase64_ct_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 2)
		{
			int ln = strlen(tokens);
			unbase64_hmac_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			i++;
			continue;
		}

		if(i == 3)
		{
			int ln = strlen(tokens);
			unbase64_tag_ptr = base64_decode((uint8_t*)tokens, (uint32_t*)&ln);
			continue;
		}
	}
	while((tokens = strtok_r(NULL, sep, &next)) != NULL);

	ct_ln = strlen((char*)unbase64_ct_ptr);

	char *pt = (char*)malloc(ct_ln * 1);

	memset(pt, 0, ct_ln * 1);

	int tag_len = strlen((char*)unbase64_tag_ptr);

	unbase64_ct_ptr[ct_ln] = '\0';
	unbase64_tag_ptr[tag_len] = '\0';

	pt_ln = gcm_decrypt(unbase64_ct_ptr, ct_ln,
						additional, strlen((char*)additional),
						unbase64_tag_ptr,
						key, iv, iv_len,
						(uint8_t*)pt);

	char *next_pt = NULL;
	char *tokens_pt = strtok_r(pt, sep, &next_pt);

	/* Unpacking concatenated data prior verification */
	if(tokens_pt == NULL) {
			printf("Memory Allocation Failure\n");
		}

	do
	{
		if(j == 0)
		{
			auth = tokens_pt;
			j++;
			continue;
		}

		if(j == 1)
		{
			u_i = tokens_pt;
			j++;
			continue;
		}
	}
	while((tokens_pt = strtok_r(NULL, sep, &next_pt)) != NULL);

	/* Generate hash */
	clash_msg_ver_concat(md, unbase64_r, (uint8_t*)u_i, (uint8_t*)auth);

	/* Generates HMAC */
	hmac_sign(md, key, not_hmac, &hmac_ln);
	/* Verifying HMAC */
	hmac_status = hmac_verify(unbase64_hmac_ptr, not_hmac, hmac_ln);

//	printf("Application:\t [clash_msg_store_ver_verification] %d\n\t\t [hmac_status verification]\n", hmac_status);

	if(hmac_status) {
		memcpy(ptr, "1", 1);
		goto erase;
	} else {
		memcpy(ptr, "0", 1);
		goto erase;
	}
	erase:
		free(unbase64_r);
		free(unbase64_ct_ptr);
		free(unbase64_hmac_ptr);
		free(unbase64_tag_ptr);
		free(tokens);
		free(tokens_pt);
		free(pt);
}

void Client::clash_msg_ver_req_generation(char *ptr, size_t len)
{
	char *data = (char*)malloc(len * 1);

	if(data == NULL) {
		printf("Memory Allocation Failure\n");
		exit(EXIT_FAILURE);
	} else {
		memset(data, 0, len * 1);
	}

	string r = "random";
	string u_i = "1.Antonis";
	string u_j = "9.Alexandr";
	string sep = ":";

	/* Additional Data */
	uint8_t *additional =
			(uint8_t*)"secret tag";

	uint8_t tag[TAG_SIZE] = {0};

	uint8_t hmac[SHA512_DIGEST_LENGTH] = {0};
	uint32_t hmac_ln = 0;
	int hmac_status = 0;

	uint8_t md[SHA512_DIGEST_LENGTH] = {0};

	int r_ln = 0;
	int tag_ln = 0;

	int ct_ln = 0, pt_ln = 0;
	int ln = 0;

	string concat  = u_i + sep + u_j + sep;

	uint8_t ct[128] = {0};

	/* Generate hash */
	clash_msg_ver_req_concat(md, (uint8_t*)r.c_str() , (uint8_t*)u_i.c_str(), (uint8_t*)u_j.c_str());

	/* Generates HMAC */
	hmac_status = hmac_sign(md, key, hmac, &hmac_ln);

//	printf("Application:\t [clash_msg_ver_req_generation] %d\n\t\t [hmac_status generation]\n", hmac_status);
	pt_ln = concat.length();

	char pt[pt_ln] = {0};

	for(int i = 0; i < pt_ln - 1; i++) {
		pt[i] = concat[i];
	}

	/* Encrypt the data */
	ct_ln = gcm_encrypt((uint8_t*)pt, strlen((char*)pt),
			additional, strlen((char*)additional),
			key,
			iv, iv_len,
			ct, tag);


	r_ln = strlen((char*)r.c_str());
	tag_ln = strlen((char*)tag);

	int b64_r_ln = get_base64_length(r_ln);
	int b64_ct_ln = get_base64_length(tag_ln);
	int b64_hmac_ln = get_base64_length(hmac_ln);
	int b64_tag_ln = get_base64_length(tag_ln);

	uint8_t base64_r[b64_r_ln] = {0};
	uint8_t base64_ct[b64_ct_ln] = {0};
	uint8_t base64_hmac[b64_hmac_ln] = {0};
	uint8_t base64_tag[b64_tag_ln] = {0};

	/* Encode with Base64 */
	EVP_EncodeBlock(base64_r, (uint8_t*)r.c_str(), r_ln);
	EVP_EncodeBlock(base64_ct, ct, ct_ln);
	EVP_EncodeBlock(base64_tag, tag, tag_ln);
	EVP_EncodeBlock(base64_hmac, hmac, hmac_ln);

	/* Concatenate encoded data prior sending it */
	ln += snprintf(data+ln, len-ln, (char*)base64_r);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_ct);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_tag);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_hmac);
	ln += snprintf(data+ln, len-ln, sep.c_str());

	memcpy(ptr, data, strlen(data));
	free(data);
}

void Client::clash_msg_revoke_generation(char *ptr, size_t len)
{
	char *data = (char*)malloc(len * 1);

	if(data == NULL) {
		printf("Memory Allocation Failure\n");
		exit(EXIT_FAILURE);
	} else {
		memset(data, 0, len * 1);
	}

	string r = "rand";
	string u_j = "9.Alexandr";
	string u_l = "10.Alexandros";
	string n = "3";
	string assign = "assign";
	string sep = ":";

	char abe_key[ABE_SIZE] = {0};

	uint8_t md[SHA512_DIGEST_LENGTH] = {0};
	uint8_t hmac[SHA512_DIGEST_LENGTH] = {0};

	uint32_t hmac_ln = 0;
	int hmac_status = 0;

	int b64_r_ln = 0, b64_ct_ln = 0,
			b64_hmac_ln = 0,
			b64_tag_ln = 0,
			b64_abe_ln = 0;

	uint8_t tag[TAG_SIZE] = {0};

	int pt_ln = 0, ct_ln = 0;
	int ln = 0;
	int r_ln = 0, tag_ln = 0;

	/* Load ABE key from a file to a buffer */
	get_cpabe_ct(abe_key, ABE_SIZE);

	string concat = u_j + sep + u_l + sep + n + sep + assign + sep;

	pt_ln = concat.length();
	char pt[pt_ln] = {0};

	strcpy(pt, concat.c_str());

	ct_ln = pt_ln;
	uint8_t ct[ct_ln] = {0};

	uint8_t *additional =
			(uint8_t*)"secret tag";

	/* Generate hash from concatenated data */
	clash_msg_assign_concat(md, (uint8_t*)r.c_str(), (uint8_t*)u_j.c_str(),
								(uint8_t*)u_l.c_str(), (uint8_t*)n.c_str(),
								(uint8_t*)abe_key, (uint8_t*)assign.c_str());

	/* Generates HMAC */
	hmac_status = hmac_sign(md, key, hmac, &hmac_ln);

	if(hmac_status != EXIT_FAILURE) exit(EXIT_FAILURE);

//	printf("Application:\t [clash_msg_assign_generation] %d\n\t\t [hmac_status generation]\n", hmac_status);

	/* Encrypt the data */
	ct_ln = gcm_encrypt((uint8_t*)pt, pt_ln,
			additional, strlen((char*)additional),
			key,
			iv, iv_len,
			ct, tag);

	r_ln = strlen(r.c_str());
	tag_ln = strlen((char*)tag);

	b64_r_ln = get_base64_length(r_ln);
	b64_ct_ln = get_base64_length(ct_ln + 3);
	b64_hmac_ln = get_base64_length(SHA512_DIGEST_LENGTH);
	b64_tag_ln = get_base64_length(tag_ln);
	b64_abe_ln = get_base64_length(ABE_SIZE);

	uint8_t base64_r[b64_r_ln] = {0};
	uint8_t base64_ct[b64_ct_ln] = {0};
	uint8_t base64_hmac[b64_hmac_ln] = {0};
	uint8_t base64_tag[b64_tag_ln] = {0};
	uint8_t base64_abe[b64_abe_ln] = {0};

	/* Encode with Base64 */
	EVP_EncodeBlock(base64_r, (uint8_t*)r.c_str(), r_ln);
	EVP_EncodeBlock(base64_ct, ct, ct_ln);
	EVP_EncodeBlock(base64_hmac, hmac, SHA512_DIGEST_LENGTH);
	EVP_EncodeBlock(base64_tag, tag, tag_ln);
	EVP_EncodeBlock(base64_abe, (uint8_t*)abe_key, ABE_SIZE);

	/* Concatenate encoded data prior sending it */
	ln += snprintf(data+ln, len-ln, (char*)base64_r);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_ct);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_hmac);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_tag);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_abe);
	ln += snprintf(data+ln, len-ln, sep.c_str());

	memcpy(ptr, data, strlen(data));
	free(data);
}

void Client::clash_msg_store_req_generation(char *ptr, size_t len)
{

	char *data = (char*)malloc(len * 1);
	if(data == NULL) {
		printf("Memory Allocation Failure\n");
	} else {
		memset(data, 0, len * 1);
	}

	int b64_r_ln = 0,
		b64_ct_ln = 0,
		b64_store_req_ln = 0,
		b64_hmac_ln = 0,
		b64_tag_ln = 0;

	string r = "rand";
	string store_req = "storereq";
	string credentials = "credentials";
	string sep = ":";

	uint8_t md[SHA512_DIGEST_LENGTH] = {0};
	uint8_t hmac[SHA512_DIGEST_LENGTH] = {0};

	uint8_t *additional = (uint8_t*)"secret tag";

	uint8_t tag[TAG_SIZE] = {0};

	uint32_t hmac_ln = 0;
	int hmac_status = 0;

	int ct_ln = 0, pt_ln = 0;
	int ln = 0;

	pt_ln = strlen(credentials.c_str());

	char pt[pt_ln] = {0};

	strcpy(pt, credentials.c_str());

	/* Generate hash for HMAC */
	clash_store_req_concat(md, (uint8_t*)r.c_str(),
			(uint8_t*)credentials.c_str(),
			(uint8_t*)store_req.c_str());

	/* Generates HMAC */
	hmac_status = hmac_sign(md, key, hmac, &hmac_ln);

	if(hmac_status != EXIT_FAILURE) exit(EXIT_FAILURE);

//	printf("Application:\t [clash_msg_store_req_generation] %d\n\t\t [hmac_status generation]\n", hmac_status);

	uint8_t ct[pt_ln] = {0};

	/* Encrypt the data */
	ct_ln = gcm_encrypt((uint8_t*)pt, pt_ln,
						additional, strlen((char*)additional),
						key,
						iv, iv_len,
						ct, tag);

	int r_ln = strlen(r.c_str());
	int tag_ln = strlen((char*)tag);
	int store_req_ln = strlen(store_req.c_str());

	b64_r_ln = get_base64_length(r_ln);
	b64_ct_ln = get_base64_length(ct_ln);
	b64_store_req_ln = get_base64_length(store_req_ln);
	b64_hmac_ln = get_base64_length(SHA512_DIGEST_LENGTH);
	b64_tag_ln = get_base64_length(tag_ln);

	uint8_t base64_r[b64_r_ln] = {0};
	uint8_t base64_ct[b64_ct_ln] = {0};
	uint8_t base64_store_req[b64_store_req_ln] = {0};
	uint8_t base64_hmac[b64_hmac_ln] = {0};
	uint8_t base64_tag[b64_tag_ln] = {0};

	/* Encode with Base64 */
	EVP_EncodeBlock(base64_r, (uint8_t*)r.c_str(), r_ln);
	EVP_EncodeBlock(base64_store_req, (uint8_t*)store_req.c_str(), store_req_ln);
	EVP_EncodeBlock(base64_ct, ct, ct_ln);
	EVP_EncodeBlock(base64_hmac, hmac, SHA512_DIGEST_LENGTH);
	EVP_EncodeBlock(base64_tag, tag, tag_ln);

	/* Concatenate encoded data prior sending it */
	ln += snprintf(data+ln, len-ln, (char*)base64_r);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_ct);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_store_req);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_hmac);
	ln += snprintf(data+ln, len-ln, sep.c_str());
	ln += snprintf(data+ln, len-ln, (char*)base64_tag);
	ln += snprintf(data+ln, len-ln, sep.c_str());

	memcpy(ptr, data, strlen(data));
	free(data);
}
