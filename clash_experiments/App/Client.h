#ifndef APP_CLIENT_H_
#define APP_CLIENT_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <inttypes.h>
#include <fstream>
#include <cstdlib>

class Client {

	public:

		void clash_msg_search_generation(char *ptr, size_t len, char *base64_msg_key, size_t msg_key_len);
		void clash_msg_update_generation(char *ptr, size_t len, char *base64_msg_key, size_t msg_key_len);
		void clash_msg_delete_generation(char *ptr, size_t len, char *base64_msg_key, size_t msg_key_len);
		void clash_msg_kt_tray_store_generation(char *ptr, size_t len);
		void clash_msg_store_ver_verification(char *ptr, size_t len);
		void clash_msg_ver_req_generation(char *ptr, size_t len);
		void clash_msg_revoke_generation(char *ptr, size_t len);
		void clash_msg_store_req_generation(char *ptr, size_t len);
};

#endif /* APP_CLIENT_H_ */
