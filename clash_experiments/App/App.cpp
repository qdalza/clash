#define __STDC_WANT_LIB_EXT1__ 1

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <inttypes.h>
#include <fstream>
#include <cstdlib>

#include <time.h>
#include <map>
#include <vector>

#include "../Enclave1/Enclave1_u.h"
#include "../Enclave2/Enclave2_u.h"
#include "../Enclave3/Enclave3_u.h"
#include "../Enclave4/Enclave4_u.h"
#include "sgx_eid.h"
#include "sgx_urts.h"
#define __STDC_FORMAT_MACROS

#include "Utility_App.h"

#include "Client.h"

#define UNUSED(val) (void)(val)
#define TCHAR   char
#define _TCHAR  char
#define _T(str) str

#define _tmain  main

#define ABE_SIZE 512
#define BUFFER_SIZE 3096
#define TAG_SIZE 16

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1

using namespace std;

extern std::map<sgx_enclave_id_t, uint32_t>g_enclave_id_map;

sgx_enclave_id_t e1_enclave_id = 0;
sgx_enclave_id_t e2_enclave_id = 0;
sgx_enclave_id_t e3_enclave_id = 0;
sgx_enclave_id_t e4_enclave_id = 0;

#define ENCLAVE1_PATH "libenclave1.so"
#define ENCLAVE2_PATH "libenclave2.so"
#define ENCLAVE3_PATH "libenclave3.so"
#define ENCLAVE4_PATH "libenclave4.so"


class EnclaveManager {

	public:
		uint32_t load_enclaves()
		{
			uint32_t enclave_temp_no;
			sgx_status_t ret = SGX_ERROR_UNEXPECTED;

			enclave_temp_no = 0;

			ret = sgx_create_enclave(ENCLAVE1_PATH, SGX_DEBUG_FLAG, NULL, NULL, &e1_enclave_id, NULL);
			if (ret != SGX_SUCCESS) {
				return ret;
			}

			enclave_temp_no++;
			g_enclave_id_map.insert(std::pair<sgx_enclave_id_t, uint32_t>(e1_enclave_id, enclave_temp_no));

			ret = sgx_create_enclave(ENCLAVE2_PATH, SGX_DEBUG_FLAG, NULL, NULL, &e2_enclave_id, NULL);
			if (ret != SGX_SUCCESS) {
				return ret;
			}

			ret = sgx_create_enclave(ENCLAVE3_PATH, SGX_DEBUG_FLAG, NULL, NULL, &e3_enclave_id, NULL);
			if (ret != SGX_SUCCESS) {
				return ret;
			}

			enclave_temp_no++;

			g_enclave_id_map.insert(std::pair<sgx_enclave_id_t, uint32_t>(e3_enclave_id, enclave_temp_no));

			ret = sgx_create_enclave(ENCLAVE4_PATH, SGX_DEBUG_FLAG, NULL, NULL, &e4_enclave_id, NULL);
			if (ret != SGX_SUCCESS) {
				return ret;
			}

			return SGX_SUCCESS;
		}

		uint32_t local_attestation()
		{
			uint32_t ret_status = 0;
			sgx_status_t status;

			status = Enclave1_clash_create_session_e1_to_e3(e1_enclave_id, &ret_status, e1_enclave_id, e3_enclave_id);
			status = Enclave1_clash_exchange_secret_e1_to_e3(e1_enclave_id, &ret_status, e1_enclave_id, e3_enclave_id);
			status = Enclave1_clash_close_session_e1_to_e3(e1_enclave_id, &ret_status, e1_enclave_id, e3_enclave_id);

			return SGX_SUCCESS;
		}

		uint32_t destroy_enclaves()
		{
			sgx_destroy_enclave(e1_enclave_id);
			sgx_destroy_enclave(e2_enclave_id);
			sgx_destroy_enclave(e3_enclave_id);
			sgx_destroy_enclave(e4_enclave_id);

			return SGX_SUCCESS;
		}
};

void ocall_print(const char *str)
{
	printf("%s", str);
}


void clash_generate_lvs(int n, map<string, map<string, string>> &lvs, map<string, string> &scopes)
{
	srand(time(0));

	int r_0 = 0;
	int r_1 = 0;
	int r_2 = 0;
	int r_3 = 0;

	string concat = "";

	for(int i = 2; i <= n - 2; i++) {
		r_0 = rand() % 2;
		r_1 = rand() % 2;
		r_2 = rand() % 2;
		r_3 = rand() % 2;
		concat = to_string(r_0) + to_string(r_1) + to_string(r_2) + to_string(r_3) + "0";
		scopes.insert(make_pair(to_string(i) + ".user", concat));
	}
	scopes.insert(make_pair(to_string(1) + ".Antonis", "11111"));
	scopes.insert(make_pair(to_string(9) + ".Alexandr", "11111"));
	scopes.insert(make_pair(to_string(10) + ".Alexandros", "11111"));

	lvs.insert(make_pair("idx", scopes));
}

class Experiment : public Client, public EnclaveManager{

	public:

		double get_mean_of_vector(std::vector<double> v)
		{
			double avg_time = 0.0;
			double sum = 0.0;
			int v_size = 0;

			std::vector<double>::iterator it;

			for(it = v.begin(); it!= v.end(); it++)
			{
				sum += *it;
			}

			v_size = v.size();

			avg_time = static_cast<double>(sum/v_size);

			return avg_time;
		}

		std::vector<string>measure_enclave_creation(string ENCLAVE_PATH)
		{
			double cpu_time_used = 0.0;
			string avg_time = "";

			clock_t start, end;

			std::vector<double> v;
			std::vector<double>::iterator it;
			std::vector<string> ret;

			sgx_enclave_id_t enclave_id = 0;

			sgx_status_t status = SGX_ERROR_UNEXPECTED;

			char* c_string = (char*)ENCLAVE_PATH.c_str();
			c_string[ENCLAVE_PATH.size()] = '\0';

			for(int i=0;i < 100;i++)
			{
				start = clock();
				status = sgx_create_enclave((const char*)c_string, SGX_DEBUG_FLAG, NULL, NULL, &enclave_id, NULL);
				end = clock();
				cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
				v.push_back(cpu_time_used);
				sgx_destroy_enclave(enclave_id);
			}

			avg_time = to_string(get_mean_of_vector(v));

			ret.push_back(ENCLAVE_PATH);
			ret.push_back(avg_time);

			return ret;
		}

		std::vector<string>measure_local_attestation()
		{
			clock_t start, end;

			double cpu_time_used = 0.0;
			double avg_time = 0.0;
			std::vector<double> v;
			std::vector<double>::iterator it;
			std::vector<string> ret;

			for(int i=0;i< 100;i++)
			{
				start = clock();
				local_attestation();
				end = clock();
				cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
				v.push_back(cpu_time_used);
			}
			avg_time = get_mean_of_vector(v);

			ret.push_back("LA-------clash_create_session_e1_to_e3\n\t\t\t clash_exchange_secret_e1_to_e3\n\t\t\t clash_close_session_e1_to_e3\n\t\t\t");
			ret.push_back(to_string(avg_time));

			return ret;
		}

		std::vector<string>measure_store_function()
		{
			double cpu_time_used = 0.0;
			double avg_time = 0.0;

			clock_t start, end;

			std::vector<string> ret;
			std::vector<double> v;
			std::vector<double>::iterator it;

			char data[BUFFER_SIZE] = {0};

			for(int i = 0; i < 100; i++)
			{
				clash_msg_store_req_generation(data, BUFFER_SIZE);
				start = clock();
				Enclave2_clash_msg_store_req_verification(e2_enclave_id, data, BUFFER_SIZE);
				end = clock();
				cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
				v.push_back(cpu_time_used);
			}

			avg_time = get_mean_of_vector(v);
			v.clear();

			for(int i = 0; i < 100; i++)
			{
				start = clock();
				Enclave2_clash_msg_store_ver_generation(e2_enclave_id, data, BUFFER_SIZE);
				end = clock();
				cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
				v.push_back(cpu_time_used);
				clash_msg_store_ver_verification(data, BUFFER_SIZE);
			}

			avg_time += get_mean_of_vector(v);

			ret.push_back("Store----clash_msg_store_req_verification\n\t\t\t clash_msg_store_ver_generation\n\t\t\t ");
			ret.push_back(to_string(avg_time));

			return ret;
		}

		std::vector<string>measure_search_function()
		{
			std::vector<string> ret;
			std::vector<double> v;
			std::vector<double>::iterator it;

			clock_t start, end;

			double cpu_time_used = 0.0;
			double avg_time = 0.0;

			char msg_key[BUFFER_SIZE] = {0};
			char data[BUFFER_SIZE] = {0};

			// ****************************************************
			// *        Begin Ui sends to CSP: [CLASH.Search]     *
			// ****************************************************

			for(int i = 0; i < 100; i++)
			{
				Enclave3_clash_msg_key_generation(e3_enclave_id, msg_key, BUFFER_SIZE);
				clash_msg_search_generation(data, BUFFER_SIZE, msg_key, BUFFER_SIZE);
				start = clock();
				Enclave2_clash_msg_search_verification(e2_enclave_id, data, BUFFER_SIZE);
				end = clock();
				cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
				v.push_back(cpu_time_used);
			}

			// ****************************************************
			// *       End Ui sends to CSP: [CLASH.Search]        *
			// ****************************************************

			avg_time = get_mean_of_vector(v);

			ret.push_back("Search---clash_msg_key_generation\n\t\t\t clash_msg_search_verification\n\t\t\t");
			ret.push_back(to_string(avg_time));

			return ret;
		}

		std::vector<string>measure_update_function()
		{
			double cpu_time_used = 0.0;
			double avg_time = 0.0;
			clock_t start, end;

			std::vector<string> ret;
			std::vector<double> v;
			std::vector<double>::iterator it;

			char msg_key[BUFFER_SIZE] = {0};
			char data[BUFFER_SIZE] = {0};

			// ****************************************************
			// *        Begin Ui sends to CSP: [CLASH.Update]     *
			// ****************************************************

			for(int i = 0; i < 100; i++)
			{
				Enclave3_clash_msg_key_generation(e3_enclave_id, msg_key, BUFFER_SIZE);
				clash_msg_update_generation(data, BUFFER_SIZE, msg_key, BUFFER_SIZE);
				start = clock();
				Enclave2_clash_msg_update_verification(e2_enclave_id, data, BUFFER_SIZE);
				end = clock();
				cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
				v.push_back(cpu_time_used);
			}

			// ****************************************************
			// *       End Ui sends to CSP: [CLASH.Update]        *
			// ****************************************************

			avg_time = get_mean_of_vector(v);
			ret.push_back("Update---clash_msg_key_generation\n\t\t\t clash_msg_update_verification\n\t\t\t");
			ret.push_back(to_string(avg_time));

			return ret;
		}

		std::vector<string>measure_delete_function()
		{
			double cpu_time_used = 0.0;
			double avg_time = 0.0;
			clock_t start, end;

			std::vector<string> ret;
			std::vector<double> v;
			std::vector<double>::iterator it;

			char msg_key[BUFFER_SIZE] = {0};
			char data[BUFFER_SIZE] = {0};

			// ****************************************************
			// *        Begin Ui sends to CSP: [CLASH.Delete]     *
			// ****************************************************

			for(int i = 0; i < 100; i++)
			{
				Enclave3_clash_msg_key_generation(e3_enclave_id, msg_key, BUFFER_SIZE);
				clash_msg_delete_generation(data, BUFFER_SIZE, msg_key, BUFFER_SIZE);
				start = clock();
				Enclave2_clash_msg_delete_verification(e2_enclave_id, data, BUFFER_SIZE);
				end = clock();
				cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
				v.push_back(cpu_time_used);
			}

			// ****************************************************
			// *       End Ui sends to CSP: [CLASH.Delete]        *
			// ****************************************************

			avg_time = get_mean_of_vector(v);
			ret.push_back("Delete---clash_msg_key_generation\n\t\t\t clash_msg_delete_verification\n\t\t\t");
			ret.push_back(to_string(avg_time));

			return ret;
		}

		std::vector<string>measure_keyshare_function()
		{
			uint32_t ret_status = 0;
			sgx_status_t status;

			clock_t start, end;
			double cpu_time_used = 0.0;
			double avg_time = 0.0;

			std::vector<string> ret;
			std::vector<double> v;
			std::vector<double>::iterator it;

			char data[BUFFER_SIZE] = {0};

			// ********************************************************************************
			// *           Begin Uj sends MSG 6 to KT [CLASH.KeyShare][1],[2]                 *
			// ********************************************************************************

			for(int i = 0; i < 100; i++)
			{
				clash_msg_ver_req_generation(data, BUFFER_SIZE);
				start = clock();
				Enclave3_clash_msg_kt_ver_req_verification(e3_enclave_id, data, BUFFER_SIZE);
				end = clock();
				cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
				v.push_back(cpu_time_used);
			}

			// ********************************************************************************
			// *           End Uj sends MSG 6 to KT [CLASH.KeyShare][1],[2]                   *
			// ********************************************************************************

			avg_time = get_mean_of_vector(v);
			v.clear();

			// ****************************************************************************************
			// *           Begin KT sends MSG 7 to Uj who forwards to KT [CLASH.KeyShare][1],[2]      *
			// ****************************************************************************************

			for(int i = 0; i < 100; i++)
			{
				start = clock();
				Enclave3_clash_msg_token_generation(e3_enclave_id, data, sizeof(data));
				Enclave1_clash_msg_token_verification(e1_enclave_id, data, sizeof(data));
				Enclave3_clash_msg_key_generation(e3_enclave_id, data, sizeof(data));
				end = clock();
				cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
				v.push_back(cpu_time_used);
			}
			// ****************************************************************************************
			// *           End KT sends MSG 7 to Uj who forwards to KT [CLASH.KeyShare][1],[2]        *
			// ****************************************************************************************

			avg_time += get_mean_of_vector(v);
			v.clear();

			status = Enclave1_clash_create_session_e1_to_e3(e1_enclave_id, &ret_status, e1_enclave_id, e3_enclave_id);

			// ****************************************************************************************
			// *           Begin REV sends MSG 8 to KT  [CLASH.KeyShare][3],[4]                       *
			// ****************************************************************************************

			// Message Exchange between Enclave1(REV) and Enclave3(KT)

			for(int i = 0; i < 100; i++)
			{
				start = clock();
				Enclave1_clash_keyshare_msg_exchange_e1_to_e3(e1_enclave_id, &ret_status, e1_enclave_id, e3_enclave_id, data, sizeof(data));
				end = clock();
				cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
				v.push_back(cpu_time_used);
			}

			//Closing Session between Enclave1(REV) and Enclave3(KT)

			avg_time += get_mean_of_vector(v);

			// ****************************************************************************************
			// *               End REV sends MSG 8 to KT  [CLASH.KeyShare][3],[4]                     *
			// ****************************************************************************************

			status = Enclave1_clash_close_session_e1_to_e3(e1_enclave_id, &ret_status, e1_enclave_id, e3_enclave_id);

			ret.push_back("KeyShare-clash_msg_kt_ver_req_verification\n\t\t\t clash_msg_token_generation\n"
					"\t\t\t clash_msg_token_verification\n\t\t\t clash_msg_key_generation\n\t\t\t clash_keyshare_msg_exchange_e1_to_e3");
			ret.push_back(to_string(avg_time));

			return ret;
		}

		std::vector<string>measure_revoke_function()
		{
			uint32_t ret_status = 0;
			sgx_status_t status;

			double cpu_time_used = 0.0;
			double avg_time = 0.0;

			clock_t start, end;

			std::vector<string> ret;
			std::vector<double> v;
			std::vector<double>::iterator it;

			char data[BUFFER_SIZE] = {0};

			// ****************************************************************************************
			// *                   Begin Uj sends MSG 11 to REV  [CLASH.Revoke][1]                    *
			// ****************************************************************************************

			for(int i = 0; i < 100; i++)
			{
				clash_msg_revoke_generation(data, BUFFER_SIZE);
				start = clock();
				Enclave1_clash_msg_revoke_verification(e1_enclave_id, data, BUFFER_SIZE);
				end = clock();
				cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
				v.push_back(cpu_time_used);
			}

			// ****************************************************************************************
			// *                    End Uj sends MSG 11 to REV  [CLASH.Revoke][1]                     *
			// ****************************************************************************************

			avg_time = get_mean_of_vector(v);

			//Create Session between Enclave1(REV) and Enclave3(KT)
			status = Enclave1_clash_create_session_e1_to_e3(e1_enclave_id, &ret_status, e1_enclave_id, e3_enclave_id);

			// ****************************************************************************************
			// *                    Begin REV sends MSG 12 to KT  [CLASH.Revoke][2]                   *
			// ****************************************************************************************

			// Message Exchange between Enclave1(REV) and Enclave3(KT)
			start = clock();
			status = Enclave1_clash_revocation_msg_exchange_e1_to_e3(e1_enclave_id, &ret_status, e1_enclave_id, e3_enclave_id, data, BUFFER_SIZE);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

			// ****************************************************************************************
			// *                   End REV sends MSG 12 to KT  [CLASH.Revoke][2]                      *
			// ****************************************************************************************

			avg_time += cpu_time_used;

			//Closing Session between Enclave1(REV) and Enclave3(KT)
			status = Enclave1_clash_close_session_e1_to_e3(e1_enclave_id, &ret_status, e1_enclave_id, e3_enclave_id);

			// ****************************************************************************************
			// *           Begin KT sends MSG 13 to REV  [CLASH.Revoke][3],[4]                        *
			// ****************************************************************************************

			/* Create Session between Enclave3(KT) and Enclave1(REV) */
			status = Enclave3_clash_create_session_e3_to_e1(e3_enclave_id, &ret_status, e3_enclave_id, e1_enclave_id);

			start = clock();
			/* Message Exchange between Enclave3(KT) and Enclave1(REV) */
			status = Enclave3_clash_revocation_msg_exchange_e3_to_e1(e3_enclave_id, &ret_status, e3_enclave_id, e1_enclave_id, data, BUFFER_SIZE);
			end = clock();
			// ****************************************************************************************
			// *           End KT sends MSG 13 to REV  [CLASH.Revoke][3],[4]                          *
			// ****************************************************************************************
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

			avg_time += cpu_time_used;
			/* Closing Session between Enclave3(Source) and Enclave1(Destination) */
			status = Enclave3_clash_close_session_e3_to_e1(e3_enclave_id, &ret_status, e3_enclave_id, e1_enclave_id);

			ret.push_back("Revoke---clash_msg_revoke_verification\n\t\t\t clash_revocation_msg_exchange_e1_to_e3\n\t\t\t clash_revocation_msg_exchange_e3_to_e1\n\t\t\t ");
			ret.push_back(to_string(avg_time));

			return ret;
		}

		std::vector<string>measure_keytraystore_function()
		{

			clock_t start, end;
			int status = EXIT_SUCCESS;
			int n = 10;
			int SIZE = 2048;

			double cpu_time_used = 0.0;
			double avg_time = 0.0;

			std::vector<string> ret;
			std::vector<double> v;

			map<std::string, map<std::string, std::string> > lvs;
			map<std::string, std::string> scopes;

			std::string ht_keys = "";
			std::string keys = "";
			std::string delim = ":";
			std::string values = "";

			char data[BUFFER_SIZE] = {0};
			char lvs_buffer[SIZE] = {0};

			// ********************************************************************************
			// *           Begin Ui sends MSG 4 to KT [CLASH.KeyTrayStore][1],[2]             *
			// ********************************************************************************

			for(int i = 0; i < 5; i++)
			{
				clash_msg_kt_tray_store_generation(data, BUFFER_SIZE);
				start = clock();
				Enclave3_clash_msg_kt_tray_store_verification(e3_enclave_id, data, BUFFER_SIZE);
				end = clock();
				cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
				v.push_back(cpu_time_used);
			}

			// ********************************************************************************
			// *           End Ui sends MSG 4 to KT [CLASH.KeyTrayStore][1],[2]               *
			// ********************************************************************************

			avg_time = get_mean_of_vector(v);
			v.clear();

			// ********************************************************************************
			// *           Begin Ui sends MSG 5 to REV [CLASH.KeyTrayStore][3]                *
			// ********************************************************************************

			clash_generate_lvs(n, lvs, scopes);

			for(map<std::string,map<std::string,std::string> >::iterator ptr=lvs.begin();ptr!=lvs.end(); ptr++) {

				ht_keys += ptr->first + delim;
			}

			for(map<std::string,std::string>::iterator it = scopes.begin(); it != scopes.end(); ++it) {
				keys += it->first + delim;
				values += it->second + delim;
			}

			ht_keys += "&";
			char *ht_keys_c = (char*)ht_keys.c_str();

			keys += "&";
			char *keys_c = (char*)keys.c_str();

			values += "&";
			char *values_c = (char*)values.c_str();

			if (n > 0 && 10 >= n) {
				char *s_path = "./App/small_lvs.txt";
				if(!clash_file_exist(s_path)) {
					clash_local_store_lvs(s_path, ht_keys_c, keys_c, values_c);
				}
			} else if(11 <= n && n <= 100) {
				char *m_path = "./App/medium_lvs.txt";
				if(!clash_file_exist(m_path)) {
					clash_local_store_lvs(m_path, ht_keys_c, keys_c, values_c);
				}
			} else if (101 <= n && n <= 1000) {
				char *l_path = "./App/large_lvs.txt";
				if(!clash_file_exist(l_path)) {
					clash_local_store_lvs(l_path, ht_keys_c, keys_c, values_c);
				}
			}

			clash_get_local_lvs("./App/small_lvs.txt", lvs_buffer);

			start = clock();
			Enclave1_clash_msg_rev_store(e1_enclave_id, &status, lvs_buffer, SIZE);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
			v.push_back(cpu_time_used);

			// ********************************************************************************
			// *           End Ui sends MSG 5 to REV [CLASH.KeyTrayStore][3]                  *
			// ********************************************************************************

			avg_time += cpu_time_used;

			ret.push_back("KTStore--clash_msg_kt_tray_store_verification\n\t\t\t clash_msg_rev_store\n\t\t\t ");
			ret.push_back(to_string(avg_time));

			return ret;
		}

		uint32_t run_creation_enclaves_experiment()
		{
			std::vector<std::string> enclaves_paths;

			enclaves_paths.push_back("libenclave1.so");
			enclaves_paths.push_back("libenclave2.so");
			enclaves_paths.push_back("libenclave3.so");
			enclaves_paths.push_back("libenclave4.so");

			std::map<std::string, std::string> m;
			std::vector<std::string> v;

			for(auto const & path:enclaves_paths)
			{
				v = measure_enclave_creation(path);
				m.insert({v.front(), v.back()});
				v.clear();
			}

			std::cout << std::setw(7) << "\nTime" << "\t\t" << "Enclave Creation" << std::endl <<  std::endl;
			for(auto& t : m)
			{
				std::cout << std::setw(7) << t.second << "\t" << t.first<< std::endl;
			}

			return SGX_SUCCESS;
		}

		uint32_t run_functions_performance_experiment()
		{
			std::map<std::string, std::string> m;
			std::vector<std::string> v;

			v = measure_delete_function();
			m.insert({v.front(), v.back()});
			v.clear();

			v = measure_update_function();
			m.insert({v.front(), v.back()});
			v.clear();

			v = measure_search_function();
			m.insert({v.front(), v.back()});
			v.clear();

			v = measure_store_function();
			m.insert({v.front(), v.back()});
			v.clear();

			v = measure_keytraystore_function();
			m.insert({v.front(), v.back()});
			v.clear();

			v = measure_keyshare_function();
			m.insert({v.front(), v.back()});
			v.clear();

			v = measure_local_attestation();
			m.insert({v.front(), v.back()});
			v.clear();

			v = measure_revoke_function();
			m.insert({v.front(), v.back()});
			v.clear();

			std::cout << "\nTime"  << "\t\t" << "CLASH Functions" << std::endl <<  std::endl;

			for(auto& t : m)
			{
				std::cout << std::endl <<t.second << "\t" << std::setw(7) << t.first << std::endl;
			}

			return SGX_SUCCESS;
		}

		uint32_t run_key_generation_experiment()
		{
			clock_t start, end;
			double cpu_time_used = 0.0;
			start = clock();
			Enclave4_clash_msg_init_generation(e4_enclave_id);
			end = clock();
			cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

			std::cout << "\nTime" << "\t\t" << "RSA Keys" << std::endl <<  std::endl;
			std::cout << cpu_time_used << "\t\t"<< "Key Generation"<< std::endl;

			return SGX_SUCCESS;
		}

};


int _tmain(void)
{
	EnclaveManager manager;
	Experiment experiment;

	experiment.run_creation_enclaves_experiment();

	if(manager.load_enclaves() != SGX_SUCCESS)
	{
		printf("\nLoad Enclaves Failure");
		return EXIT_FAILURE;
	}

	experiment.run_key_generation_experiment();

	experiment.run_functions_performance_experiment();

	manager.destroy_enclaves();

	waitForKeyPress();

	return EXIT_SUCCESS;
}
