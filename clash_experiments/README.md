Clash Architecture
===============================

The clash prototype project contain 7 objectives such as (EnclaveManager, Client, Experiment, Enclave1 (REV), Enclave2 (CSP), Enclave3 (KT), and Enclave4 (MS)).

The EnclaveManager class contains SGX core functions:
* `load_enclaves()` - creation of REV, CSP, KT and MS enclaves.
* `local_attestation()` - establishing a secure session and exchanging of a secret between REV and KT enclaves.
* `destroy_enclaves()` - destruction of aforementioned enclaves.

The Client is a class which contains all untrusted functions related to Clash user. (i.e. u_i, u_j, and u_l).

The Experiment class inherites EnclaveManager, and Client class. Experiment contain the following functions:
* `run_creation_enclaves_experiments()` - measures the average execution time required to launch enclaves.
* `run_key_generation_experiments()` - measures the RSA master public/private key pairs for encryption/decryption, signature creation/verification
* `run_functions_performance_experiment()` - measures the execution time for the core CLASH functions: Clash.Revoke(), Clash.KeyShare(), Clash.KeyTrayStore(), Clash.Store(), Clash.Search(), Clash.Update(), Clash.Delete(), and Local Attestion().

Enclave1 (REV) - contains all associated trusted functions to REV.\
Enclave2 (CSP) - contains all associated trusted functions to CSP.\
Enclave3  (KT) - contains all associated trusted functions to KT.\
Enclave4  (MS) - contains all associated trusted functions to MS.

Additionally, each objective linked with utility.cpp file where all utility
methods are stored relevant to the component. (i.e. cryptographic primitives, debug functions, etc.) Apart from that, the execution flow can be observed from the App.cpp (main) file.

Clash Experiments
==================
The project demonstrates:
* Experiments (Enclave Creation)
* Evalution of CLASH functions' performance
* Execution time of RSA master public/private key pairs(encryption/decryption), signature (creation/verification)


Clash Prerequisites (Linux)
===========================

* Perl
* Intel(R) SGX SDK.
* Intel(R) SGX PSW.
* Intel(R) SGX driver.
* OpenSSL SGX 1.1.1.

To build Intel SGX SSL package in Linux OS:
(tar.gz package, e.g. openssl-1.1.1a.tar.gz)
1. Download [OpenSSL 1.1.1](https://github.com/intel/intel-sgx-ssl) package into openssl_source/ directory.
2. Download and install latest SGX SDK from [01.org](https://01.org/intel-software-guard-extensions/downloads). You can find installation guide in the same website.
3. Source SGX SDK's environment variables.
4. `cd` to Linux/ directory and run:
	* `make all test`
	
This will build and test the Intel SGX SSL libraries (libsgx_tsgxssl.a, libsgx_usgxssl.a, libsgx_tsgxssl_crypto.a), which can be found in package/lib64/.

How to Build/Execute the Clash
=======================================

Install Intel(R) Software Guard Extensions (Intel(R) SGX) SDK for Linux* OS\Make sure your environment is set:
* `$ source ${sgx-sdk-install-path}/environment`

Build the project with the prepared Makefile:

* Hardware Mode, Debug build:\
	`$ make`
* Hardware Mode, Pre-release build:\
	`$ make SGX_PRERELEASE=1 SGX_DEBUG=0`
* Hardware Mode, Release build:\
	`$ make SGX_DEBUG=0`
* Simulation Mode, Debug build:\
	`$ make SGX_MODE=SIM`
* Simulation Mode, Pre-release build:\
	`$ make SGX_MODE=SIM SGX_PRERELEASE=1 SGX_DEBUG=0`
* Simulation Mode, Release build:\
	`$ make SGX_MODE=SIM SGX_DEBUG=0`

Execute the binary directly:
* `$ ./app`
Remember to `make clean` before switching build mode