#include "sgx_eid.h"
#include "EnclaveMessageExchange.h"
#include "error_codes.h"
#include "Utility_E3.h"
#include "stdlib.h"
#include "string.h"
#include <mbusafecrt.h>

#include <openssl/hmac.h>
#include <openssl/evp.h>
#include <openssl/sha.h>
#include <openssl/err.h>
#include <openssl/bio.h>
#include <openssl/pem.h>

#include "sgx_tprotected_fs.h"

#define RSA_KEY_SIZE 4096
#define PADDING RSA_PKCS1_OAEP_PADDING

void handleErrors(void) {
	ERR_get_error();
	abort();
}

int hmac_sign(uint8_t *data, uint8_t *key, uint8_t *hmac, uint32_t *hmac_ln) {

	HMAC_CTX *ctx;

	if(!(ctx = HMAC_CTX_new()))
		handleErrors();

	if(1 != HMAC_Init_ex(ctx, key, strlen((char*)key), EVP_sha256(), NULL))
		handleErrors();

	if(1 != HMAC_Update(ctx, data, strlen((char*)data)))
		handleErrors();

	if(1 != HMAC_Final(ctx, hmac, hmac_ln)) {
		handleErrors();
		return 0;
	}

	HMAC_CTX_free(ctx);

	return 1;
}


int hmac_verify(uint8_t *hmac, uint8_t *not_hmac, uint32_t hmac_ln) {

	for (int i=0; i!=hmac_ln; i++) {
		if (not_hmac[i] != hmac[i]) {
			 return 0;
		}
	 }
	return 1;
}

int aes_encrypt(uint8_t *pt, int pt_ln, uint8_t* key, uint8_t *iv, uint8_t *ct) {

	EVP_CIPHER_CTX *ctx;

	int ln, ct_ln;

	if(!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv))
		handleErrors();

	if(1 != EVP_EncryptUpdate(ctx, ct, &ln, pt, pt_ln))
		handleErrors();

	ct_ln = ln;

	if (1 != EVP_EncryptFinal_ex(ctx, ct + ln, &ln))
		handleErrors();

	ct_ln = ln;

	EVP_CIPHER_CTX_free(ctx);

	return ct_ln;
}

int aes_decrypt(uint8_t *ct, int ct_ln, uint8_t *key, uint8_t *iv, uint8_t *pt) {


	EVP_CIPHER_CTX *ctx;

	int ln, pt_ln;

	if(!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	EVP_CIPHER_CTX_set_padding(ctx, 0);

	if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv))
		handleErrors();

	if(1 != EVP_DecryptUpdate(ctx, pt, &ln, ct, ct_ln))
		handleErrors();

	pt_ln = ln;

	if (1 != EVP_DecryptFinal_ex(ctx, pt + ln, &ln))
		handleErrors();
	pt_ln += ln;

	EVP_CIPHER_CTX_free(ctx);

	return pt_ln;
}

int gcm_encrypt(uint8_t *plaintext, int plaintext_len,
				uint8_t *aad, int aad_len,
				uint8_t *key,
				uint8_t *iv, int iv_len,
				uint8_t *ciphertext,
				uint8_t *tag)
	{
	EVP_CIPHER_CTX *ctx;

	int len;

	int ciphertext_len;

	if(!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL))
		handleErrors();

	if(1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, iv_len, NULL))
		handleErrors();

	if(1 != EVP_EncryptInit_ex(ctx, NULL, NULL, key, iv))
		handleErrors();

	if(1 != EVP_EncryptUpdate(ctx, NULL, &len, aad, aad_len))
		handleErrors();

	if(1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
		handleErrors();
	ciphertext_len = len;

	if(1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))
		handleErrors();
	ciphertext_len += len;

	if(1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_GET_TAG, 16, tag))
		handleErrors();

	EVP_CIPHER_CTX_free(ctx);

	return ciphertext_len;
}


int gcm_decrypt(uint8_t *ciphertext, int ciphertext_len,
				uint8_t *aad, int aad_len,
				uint8_t *tag,
				uint8_t *key,
				uint8_t *iv, int iv_len,
				uint8_t *plaintext)
	{
	EVP_CIPHER_CTX *ctx;
	int len;
	int plaintext_len;
	int ret;

	if(!(ctx = EVP_CIPHER_CTX_new()))
		handleErrors();

	if(!EVP_DecryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL))
		handleErrors();

	if(!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, iv_len, NULL))
		handleErrors();

	if(!EVP_DecryptInit_ex(ctx, NULL, NULL, key, iv))
		handleErrors();

	if(!EVP_DecryptUpdate(ctx, NULL, &len, aad, aad_len))
		handleErrors();

	if(!EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
		handleErrors();
	plaintext_len = len;

	if(!EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_TAG, 16, tag))
		handleErrors();

	ret = EVP_DecryptFinal_ex(ctx, plaintext + len, &len);


	EVP_CIPHER_CTX_free(ctx);

	if(ret > 0) {
		plaintext_len += len;
		return plaintext_len;
	} else {
		return -1;
	}
}


uint8_t *base64_decode(uint8_t *bbuf, uint32_t *len)
{
	uint8_t *ret;
	uint32_t bin_len;

	bin_len = (((strlen((char*)bbuf) + 3)/4) * 3);
	ret = (uint8_t*)malloc(bin_len);
	*len = EVP_DecodeBlock(ret, bbuf, strlen((char*)bbuf));

	return ret;
}


enc_key_pair PKE_KeyGen()
{
	enc_key_pair key_pair;
	key_pair.pk = NULL;
	key_pair.sk = NULL;

	RSA *keypair = RSA_new();
	BIGNUM *e = BN_new();

	BN_set_word(e, RSA_F4);

	RSA_generate_key_ex(keypair, RSA_KEY_SIZE, e, NULL);

	BIO *pub_key_bio = BIO_new(BIO_s_mem());
	BIO *priv_key_bio = BIO_new(BIO_s_mem());

	PEM_write_bio_RSAPublicKey(pub_key_bio, keypair);
	PEM_write_bio_RSAPrivateKey(priv_key_bio, keypair, NULL, NULL, 0, NULL, NULL);

	PEM_read_bio_RSAPublicKey(pub_key_bio, &key_pair.pk, NULL, NULL);
	PEM_read_bio_RSAPrivateKey(priv_key_bio, &key_pair.sk, NULL, NULL);

	BN_free(e);
	RSA_free(keypair);
	BIO_free(pub_key_bio);
	BIO_free(priv_key_bio);

	return (key_pair);
}

void clear_key_pair(enc_key_pair *key_pair)
{
	RSA_free(key_pair->pk);
	RSA_free(key_pair->sk);
}

// FILE MANIPULATION

// MESSAGE DIGEST FUNCTIONS

void clash_msg_concat_pass_scope(uint8_t *md, uint8_t *r, uint8_t* s_j)
{
	SHA512_CTX c;

	SHA512_Init(&c);

	SHA512_Update(&c, r, strlen((char*)r));
	SHA512_Update(&c, s_j, strlen((char*)s_j));

	SHA512_Final(md, &c);

}


void clash_msg_kt_store_concat(uint8_t *md, uint8_t *r, uint8_t *u_i, uint8_t *idx, uint8_t *abe_ct)
{
	SHA512_CTX c;

	SHA512_Init(&c);

	SHA512_Update(&c, r, strlen((char*)r));
	SHA512_Update(&c, u_i, strlen((char*)u_i));
	SHA512_Update(&c, abe_ct, strlen((char*)abe_ct));
	SHA512_Update(&c, idx, strlen((char*)idx));

	SHA512_Final(md, &c);
}

void clash_msg_ver_req_concat(uint8_t *md, uint8_t *r, uint8_t *u_i, uint8_t *u_j)
{
	SHA512_CTX c;

	SHA512_Init(&c);

	SHA512_Update(&c, r, strlen((char*)r));
	SHA512_Update(&c, u_i, strlen((char*)u_i));
	SHA512_Update(&c, u_j, strlen((char*)u_j));

	SHA512_Final(md, &c);
}


void clash_md_concat_token(uint8_t *md, uint8_t *r, uint8_t *u_j, uint8_t *idx)
{
	SHA512_CTX c;

	SHA512_Init(&c);

	SHA512_Update(&c, r, strlen((char*)r));
	SHA512_Update(&c, u_j, strlen((char*)u_j));
	SHA512_Update(&c, idx, strlen((char*)idx));

	SHA512_Final(md, &c);
}


void clash_msg_key_generation_concat(uint8_t *md, uint8_t *r, uint8_t *u_j, uint8_t *t, uint8_t *idx, uint8_t *abe_ct)
{
	SHA512_CTX c;

	SHA512_Init(&c);

	SHA512_Update(&c, r, strlen((char*)r));
	SHA512_Update(&c, u_j, strlen((char*)u_j));
	SHA512_Update(&c, t, strlen((char*)t));
	SHA512_Update(&c, idx, strlen((char*)idx));
	SHA512_Update(&c, abe_ct, strlen((char*)abe_ct));

	SHA512_Final(md, &c);

}


void clash_idx_kt_concat(uint8_t *md, uint8_t *r, uint8_t *u_l, uint8_t *abe_ct)
{
	SHA512_CTX c;

	SHA512_Init(&c);

	SHA512_Update(&c, r, strlen((char*)r));
	SHA512_Update(&c, u_l, strlen((char*)u_l));
	SHA512_Update(&c, abe_ct, strlen((char*)abe_ct));

	SHA512_Final(md, &c);
}


void clash_m_idx_concat(uint8_t *md, uint8_t *r, uint8_t *u_i, uint8_t *m_idx)
{
	SHA512_CTX c;

	SHA512_Init(&c);

	SHA512_Update(&c, r, strlen((char*)r));
	SHA512_Update(&c, u_i, strlen((char*)u_i));
	SHA512_Update(&c, m_idx, strlen((char*)m_idx));

	SHA512_Final(md, &c);
}

void get_cpabe_ct(char *abe_ct, size_t len)
{
	SGX_FILE *fd = sgx_fopen_auto_key("./App/abe.key", "r");

	char *data = (char*)malloc(len);

	if(fd != 0) {
		sgx_fread(data, sizeof(char), len, fd);
	}

	sgx_fclose(fd);

	memcpy_s(abe_ct, len, data, len);
	free(data);
}


uint32_t marshal_input_parameters_e3_to_e1(uint32_t target_fn_id, uint32_t msg_type, char *p, size_t p_size, char** marshalled_buff, size_t* marshalled_buff_len)
{
	ms_in_msg_exchange_t *ms;
	size_t param_len, ms_len;
	char *temp_buff;

	param_len = p_size;
	temp_buff = (char*)malloc(param_len);
	if(!temp_buff)
		return MALLOC_ERROR;

	memcpy(temp_buff, p, p_size);
	ms_len = sizeof(ms_in_msg_exchange_t) + param_len;
	ms = (ms_in_msg_exchange_t *)malloc(ms_len);
	if(!ms) {
		SAFE_FREE(temp_buff);
		return MALLOC_ERROR;
	}
	ms->msg_type = msg_type;
	ms->target_fn_id = target_fn_id;
	ms->inparam_buff_len = (uint32_t)param_len;
	memcpy(&ms->inparam_buff, temp_buff, param_len);
	*marshalled_buff = (char*)ms;
	*marshalled_buff_len = ms_len;
	SAFE_FREE(temp_buff);
	return SUCCESS;
}


uint32_t get_cipher(sgx_aes_gcm_128bit_key_t *key, char *p, size_t p_size, uint8_t *iv, size_t iv_size, char *c)
{
	char *temp_buff;
	sgx_status_t status;
	const uint8_t* plaintext;
	uint32_t plaintext_length;
	sgx_aes_gcm_128bit_tag_t temp_mac;

	plaintext = (const uint8_t*)(" ");
	plaintext_length = 0;
	temp_buff = (char*)malloc(p_size);
	if(!temp_buff)
		return MALLOC_ERROR;

	status = sgx_rijndael128GCM_encrypt((sgx_aes_gcm_128bit_key_t *)key, (uint8_t*)p, p_size,
				reinterpret_cast<uint8_t *>(temp_buff),
				reinterpret_cast<uint8_t *>(iv), iv_size, plaintext, plaintext_length,
				&temp_mac);
	if(SGX_SUCCESS != status)
	{
		SAFE_FREE(temp_buff);
		return status;
	}

	memcpy(c, temp_buff, p_size);
	SAFE_FREE(temp_buff);
	return SUCCESS;
}


uint32_t marshal_message_exchange_request(uint32_t target_fn_id, uint32_t msg_type, uint32_t secret_data, char** marshalled_buff, size_t* marshalled_buff_len)
{
	ms_in_msg_exchange_t *ms;
	size_t secret_data_len, ms_len;
	if(!marshalled_buff_len)
		return INVALID_PARAMETER_ERROR;
	secret_data_len = sizeof(secret_data);
	ms_len = sizeof(ms_in_msg_exchange_t) + secret_data_len;
	ms = (ms_in_msg_exchange_t *)malloc(ms_len);
	if(!ms)
		return MALLOC_ERROR;

	ms->msg_type = msg_type;
	ms->target_fn_id = target_fn_id;
	ms->inparam_buff_len = (uint32_t)secret_data_len;
	memcpy(&ms->inparam_buff, &secret_data, secret_data_len);

	*marshalled_buff = (char*)ms;
	*marshalled_buff_len = ms_len;
	return SUCCESS;
}


uint32_t umarshal_message_exchange_input_parameters_e1_to_e3(char** p, size_t* p_size, ms_in_msg_exchange_t* ms)
{
	char* buff;
	size_t len;

	if(!p || !p_size || !ms)
		return INVALID_PARAMETER_ERROR;

	buff = ms->inparam_buff;
	len = ms->inparam_buff_len;

	*p = (char*)malloc(len);

	if(!p)
		return MALLOC_ERROR;
	memcpy(*p, buff, len);
	*p_size = len;

	return SUCCESS;
}


uint32_t marshal_message_exchange_response(char** resp_buffer, size_t* resp_length, char *p, size_t p_size)
{
	ms_out_msg_exchange_t *ms;
	size_t ret_param_len, ms_len;
	size_t retval_len;
	char *temp_buff;
	if(!resp_length)
		return INVALID_PARAMETER_ERROR;
	retval_len = p_size;
	ret_param_len = retval_len;
	temp_buff = (char*)malloc(ret_param_len);
	if(!temp_buff)
			return MALLOC_ERROR;
	memcpy(temp_buff, p, p_size);
	ms_len = sizeof(ms_out_msg_exchange_t) + ret_param_len;
	ms = (ms_out_msg_exchange_t *)malloc(ms_len);
	if(!ms)
	{
		SAFE_FREE(temp_buff);
		return MALLOC_ERROR;
	}
	ms->retval_len = (uint32_t)retval_len;
	ms->ret_outparam_buff_len = (uint32_t)ret_param_len;
	memcpy(&ms->ret_outparam_buff, temp_buff, ret_param_len);
	*resp_buffer = (char*)ms;
	*resp_length = ms_len;
	SAFE_FREE(temp_buff);
	return SUCCESS;
}

uint32_t umarshal_message_exchange_response(char* out_buff, char** secret_response)
{
	size_t retval_len;
	ms_out_msg_exchange_t *ms;
	if(!out_buff)
		return INVALID_PARAMETER_ERROR;
	ms = (ms_out_msg_exchange_t *)out_buff;
	retval_len = ms->retval_len;
	*secret_response = (char*)malloc(retval_len);
	if(!*secret_response)
	{
		return MALLOC_ERROR;
	}
	memcpy(*secret_response, ms->ret_outparam_buff, retval_len);
	return SUCCESS;
}


uint32_t unmarshal_input_parameters_e1_to_e3(sgx_aes_gcm_128bit_key_t* key, uint8_t* iv, ms_in_msg_exchange_t* ms)
{
	char* buff;
	size_t len;

	size_t key_size = 128 / 8;

	if(!key || !iv || !ms)
		return INVALID_PARAMETER_ERROR;

	buff = ms->inparam_buff;
	len = ms->inparam_buff_len;

	if(len < key_size)
		return ATTESTATION_ERROR;

	if(!key)
		return MALLOC_ERROR;
	if (!iv)
		return MALLOC_ERROR;

	memcpy(key, buff, key_size);
	memcpy(iv, buff + key_size, len - key_size);

	return SUCCESS;
}


uint32_t marshal_retval_and_output_parameters_e3_to_e1(char** resp_buffer, size_t* resp_length, uint32_t retval)
{
	ms_out_msg_exchange_t *ms;
	size_t ret_param_len, ms_len;
	char *temp_buff;
	size_t retval_len;
	if(!resp_length)
		return INVALID_PARAMETER_ERROR;
	retval_len = sizeof(retval);
	ret_param_len = retval_len; //no out parameters
	temp_buff = (char*)malloc(ret_param_len);
	if(!temp_buff)
		return MALLOC_ERROR;

	memcpy(temp_buff, &retval, sizeof(retval));
	ms_len = sizeof(ms_out_msg_exchange_t) + ret_param_len;
	ms = (ms_out_msg_exchange_t *)malloc(ms_len);
	if(!ms)
	{
		SAFE_FREE(temp_buff);
		return MALLOC_ERROR;
	}
	ms->retval_len = (uint32_t)retval_len;
	ms->ret_outparam_buff_len = (uint32_t)ret_param_len;
	memcpy(&ms->ret_outparam_buff, temp_buff, ret_param_len);
	*resp_buffer = (char*)ms;
	*resp_length = ms_len;
	SAFE_FREE(temp_buff);
	return SUCCESS;
}
