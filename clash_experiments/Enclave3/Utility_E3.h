#ifndef UTILITY_E3_H__
#define UTILITY_E3_H__

#include "stdint.h"

#include <openssl/rsa.h>


typedef struct _internal_param_struct_t
{
	uint32_t ivar1;
	uint32_t ivar2;
}internal_param_struct_t;

typedef struct _external_param_struct_t
{
	uint32_t var1;
	uint32_t var2;
	internal_param_struct_t *p_internal_struct;
}external_param_struct_t;

typedef struct _param_struct_t
{
	uint32_t var1;
	uint32_t var2;
}param_struct_t;

typedef struct encryption_key_pair {
	RSA *pk;
	RSA *sk;
} enc_key_pair;


#ifdef __cplusplus
extern "C" {
#endif

// ADDITIONAL FUNCTIONS

void handleErrors(void);
int hmac_sign(uint8_t *data, uint8_t *key, uint8_t *hmac, uint32_t *hmac_ln);
int hmac_verify(uint8_t *hmac, uint8_t *not_hmac, uint32_t hmac_ln);

int aes_encrypt(uint8_t *pt, int pt_ln, uint8_t* key, uint8_t *iv, uint8_t *ct);
int aes_decrypt(uint8_t *ct, int ct_ln, uint8_t *key, uint8_t *iv, uint8_t *pt);



int gcm_encrypt(uint8_t *plaintext, int plaintext_len,
                uint8_t *aad, int aad_len,
                uint8_t *key,
                uint8_t *iv, int iv_len,
                uint8_t *ciphertext,
                uint8_t *tag);


int gcm_decrypt(uint8_t *ciphertext, int ciphertext_len,
                uint8_t *aad, int aad_len,
                uint8_t *tag,
                uint8_t *key,
                uint8_t *iv, int iv_len,
                uint8_t *plaintext);

uint8_t *base64_decode(uint8_t *bbuf, uint32_t *len);
enc_key_pair PKE_KeyGen();
void clear_key_pair(enc_key_pair *key_pair);

// FILE MANIPULATION

void get_cpabe_ct(char *abe_ct, size_t len);

// MESSAGE DIGEST FUNCTIONS

void clash_msg_concat_pass_scope(uint8_t *md, uint8_t *r, uint8_t* s_j);
void clash_msg_kt_store_concat(uint8_t *md, uint8_t *r, uint8_t *u_i, uint8_t *idx, uint8_t *abe_ct);
void clash_msg_ver_req_concat(uint8_t *md, uint8_t *r, uint8_t *u_i, uint8_t *u_j);
void clash_md_concat_token(uint8_t *md, uint8_t *r, uint8_t *u_j, uint8_t *idx);
void clash_msg_key_generation_concat(uint8_t *md, uint8_t *r, uint8_t *u_j, uint8_t *t, uint8_t *idx, uint8_t *abe_ct);
void clash_idx_kt_concat(uint8_t *md, uint8_t *r, uint8_t *u_l, uint8_t *abe_ct);
void clash_m_idx_concat(uint8_t *md, uint8_t *r, uint8_t *u_i, uint8_t *m_idx);


uint32_t unmarshal_input_parameters_e1_to_e3(sgx_aes_gcm_128bit_key_t* key, uint8_t* iv, ms_in_msg_exchange_t* ms);
uint32_t marshal_retval_and_output_parameters_e3_to_e1(char** resp_buffer, size_t* resp_length, uint32_t retval);
uint32_t umarshal_message_exchange_input_parameters_e1_to_e3(char** p, size_t* p_size, ms_in_msg_exchange_t* ms);
uint32_t marshal_input_parameters_e3_to_e1(uint32_t target_fn_id, uint32_t msg_type, char *p, size_t p_size, char** marshalled_buff, size_t* marshalled_buff_len);
uint32_t marshal_message_exchange_request(uint32_t target_fn_id, uint32_t msg_type, uint32_t secret_data, char** marshalled_buff, size_t* marshalled_buff_len);
uint32_t marshal_message_exchange_response(char** resp_buffer, size_t* resp_length, char *p, size_t p_size);
uint32_t umarshal_message_exchange_response(char* out_buff, char** secret_response);

#ifdef __cplusplus
 }
#endif
#endif
