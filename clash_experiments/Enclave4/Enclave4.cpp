#include "sgx_eid.h"

#include <stdarg.h>
#include <stdio.h>

#include <sgx_tprotected_fs.h>

#include "/opt/intel/Linux/package/include/tSgxSSL_api.h"

#include <openssl/rsa.h>
#include <openssl/bn.h>
#include <openssl/bio.h>
#include <openssl/pem.h>
#include <openssl/evp.h>
#include <openssl/sha.h>
#include <openssl/obj_mac.h>

#include "Enclave4_t.h"
#include "EnclaveMessageExchange.h"
#include "error_codes.h"
#include "Utility_E4.h"

#include <mbusafecrt.h>

#define RSA_KEY_SIZE 4096
#define PADDING RSA_PKCS1_OAEP_PADDING


typedef struct encryption_key_pair {
	RSA *pk;
	RSA *sk;
} enc_key_pair;


enc_key_pair PKE_KeyGen();

void clear_key_pair(enc_key_pair *key_pair);

void printf(const char *fmt, ...)
{
	char buf[BUFSIZ] = {'\0'};
	va_list ap;
	va_start(ap, fmt);
	vsnprintf(buf, BUFSIZ, fmt, ap);
	va_end(ap);
	ocall_print(buf);
}

enc_key_pair PKE_KeyGen()
{
	enc_key_pair key_pair;
	key_pair.pk = NULL;
	key_pair.sk = NULL;

	RSA *keypair = RSA_new();
	BIGNUM *e = BN_new();

	BN_set_word(e, RSA_F4);

	RSA_generate_key_ex(keypair, RSA_KEY_SIZE, e, NULL);

	BIO *pub_key_bio = BIO_new(BIO_s_mem());
	BIO *priv_key_bio = BIO_new(BIO_s_mem());

	PEM_write_bio_RSAPublicKey(pub_key_bio, keypair);
	PEM_write_bio_RSAPrivateKey(priv_key_bio, keypair, NULL, NULL, 0, NULL, NULL);

	PEM_read_bio_RSAPublicKey(pub_key_bio, &key_pair.pk, NULL, NULL);
	PEM_read_bio_RSAPrivateKey(priv_key_bio, &key_pair.sk, NULL, NULL);

	BN_free(e);
	RSA_free(keypair);
	BIO_free(pub_key_bio);
	BIO_free(priv_key_bio);

	return (key_pair);
}


void clear_key_pair(enc_key_pair *key_pair)
{
	RSA_free(key_pair->pk);
	RSA_free(key_pair->sk);

}

void clash_msg_init_generation()
{
	enc_key_pair key_pair;
	enc_key_pair sign_key_pair;
	key_pair = PKE_KeyGen();
	sign_key_pair = PKE_KeyGen();
	clear_key_pair(&key_pair);
	clear_key_pair(&sign_key_pair);
}
